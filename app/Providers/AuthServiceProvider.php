<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;
use App\Models\Role;


class AuthServiceProvider extends ServiceProvider
{



    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('haveaccess', function(User $user, $perm){
           //dd($perm);
           return $user->havePermission($perm);
          //  return $perm;
        });

        Gate::define('administrador', function(User $user) {
            return $user->haveRole();
         });

         Gate::define('mensajero', function(User $user) {
            return $user->haveRoleM();
         });

         Gate::define('logistica', function(User $user) {
            return $user->haveRoleL();
         });
    }
}
