<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactanosMailable extends Mailable
{
    use Queueable, SerializesModels;
    //variable para declarar asunto
    public $subject ="Has recibido un mensaje desde el formulario de contacto de tu sitio web";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $nombre=Session::get('nombre');
        $correo=Session::get('correo');
        $telefono=Session::get('telefono');
        $mensaje=Session::get('mensaje');
       return $this->view('emails.contactanos', compact('nombre','correo','telefono','mensaje'));

    }
}
