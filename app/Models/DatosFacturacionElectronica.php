<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatosFacturacionElectronica extends Model
{
    use HasFactory;
    protected $table = "datos_facturacion_electronicas";
    protected $primaryKey = 'id';
    protected $fillable = [
        'empresa_id',
        'contribuyente',
        'ubicacionarchivop12',
        'contrasena',
        'ubicacionruta',
        'pruebaproduccion',

    ];
}
