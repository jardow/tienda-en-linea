<?php

namespace App\Models;

//include ('/app/includes.php');



use XMLWriter;
use Illuminate\Support\Str;
use App\Models\Metodo_Envio;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;


//class ComprobantesElectronicos extends Model
//{
trait XmlTrait
{

    use HasFactory;
    public function validar_clave($clave)
    {

        if ($clave == "") {
            $verificado = false;
            return $verificado;
        }

        $x = 2;
        $sumatoria = 0;
        for ($i = strlen($clave) - 1; $i >= 0; $i--) {
            if ($x > 7) {
                $x = 2;
            }
            $sumatoria = $sumatoria + ($clave[$i] * $x);
            $x++;
        }
        $digito = $sumatoria % 11;
        $digito = 11 - $digito;

        switch ($digito) {
            case 10:
                $digito = "1";
                break;
            case 11:
                $digito = "0";
                break;
        }

        /*
          if (strtolower($digito_v)==$digito){
          $verificado=true;
          } else {
          $verificado=false;
          }

         */

        return $digito;
    }

    public function xmlFactura()
    {

        $infoTributaria = DB::table('empresas')
        ->where('facturas.id',Session::get('factura_id'))
        ->join('datos_facturacion_electronicas', 'empresas.id', '=', 'datos_facturacion_electronicas.empresa_id')
        ->join('numeradores', 'empresas.id', '=', 'numeradores.empresa_id')
        ->join('comprobantes', 'empresas.id', '=', 'comprobantes.empresa_id')
        ->join('facturas', 'comprobantes.id', '=', 'facturas.comprobante_id')
        ->join('info_facturas', 'facturas.id', '=', 'info_facturas.factura_id')
        ->select(
            'empresas.razonsocial as empRazonSocial',
            'empresas.nombre as empNombre',
            'empresas.direccion as empDireccion',
            'empresas.lugar as empLugar',
            'empresas.telefono as empTelefono',
            'empresas.celular as empCelular',
            'empresas.correo as empCorreo',
            'empresas.ruc as empRuc',
            'empresas.contribuyente as empContribuyente',
            'empresas.contabilidad as empContabilidad',
            'empresas.logo as empLogo',
            'datos_facturacion_electronicas.contribuyente as datContribuyente',
            'datos_facturacion_electronicas.ubicacionarchivop12 as datUbicacionarchivoP12',
            'datos_facturacion_electronicas.contrasena as datContrasena',
            'datos_facturacion_electronicas.ubicacionruta as datUbicacionRuta',
            'datos_facturacion_electronicas.pruebaproduccion as datPruebaProduccion',
            'numeradores.establecimiento as numEstablecimiento',
            'numeradores.puntoemision as numPuntoEmision',
            'numeradores.correlativo as numCorrelativo',
            'comprobantes.nombre as compNombre',
            'comprobantes.codigo as compCodigo',
            'comprobantes.fechainicio as compFechaInicio',

            'facturas.id as facId',
            'facturas.fecha_emision as facFechaEmision',
            'facturas.claveacceso as facClaveAcceso',
            'facturas.serie as facSerie',
            'facturas.modalidad_pago_id as facModalidadPago',
            'facturas.fecha_emision as facFechaEmision',
            'facturas.estado_compra_id as facEstado',
            'facturas.iva as facIva',
            'facturas.total as facTotal',
            'info_facturas.nombres as info_facNombres',
            'info_facturas.apellidos as info_facApellidos',
            'info_facturas.ruc_cedula as info_facRucCedula',
            'info_facturas.direccion as info_facDireccion',
            'info_facturas.telefono as info_facTelefono',

        )
        ->get();

        foreach ($infoTributaria as $item) {
        }


        do {
            $randomNumber = rand(0, 100000000);
        } while (Str::length($randomNumber) == 7);


        $clave = "" . date('dmY', strtotime($item->facFechaEmision)) . "" . str_pad($item->compCodigo, '2', '0', STR_PAD_LEFT) . "" . $item->empRuc . "" . $item->datPruebaProduccion . "" . "" . str_pad($item->facSerie, 15, 0, STR_PAD_LEFT) . "" . str_pad($randomNumber, '8', '0', STR_PAD_LEFT) . "" . '1' . "";
        $claveAcceso = $this->validar_clave($clave);

        $claveA = "" . $clave . "" . $claveAcceso;

        //Información Tributaria

        $infoTributaria = DB::table('empresas')->first();

        $valor = $item->numCorrelativo;
        $long = 9;
        $secuencial = str_pad($valor, $long, '0', STR_PAD_LEFT);

       $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;

        $xml_fac = $xml->createElement('factura');
        $cabecera = $xml->createAttribute('id');
        $cabecera->value = 'comprobante';
        $cabecerav = $xml->createAttribute('version');
        $cabecerav->value = '1.0.0';
        $xml_inf = $xml->createElement('infoTributaria');
        $xml_amb = $xml->createElement('ambiente', $item->datPruebaProduccion);
        $xml_tip = $xml->createElement('tipoEmision', '1');
        $xml_raz = $xml->createElement('razonSocial', $item->empRazonSocial);
        $xml_nom = $xml->createElement('nombreComercial', $item->empNombre);
        $xml_ruc = $xml->createElement('ruc', $item->empRuc);

        $xml_cla = $xml->createElement('claveAcceso', $claveA);
        $xml_doc = $xml->createElement('codDoc', $item->compCodigo);
        $xml_est = $xml->createElement('estab', $item->numEstablecimiento);
        $xml_emi = $xml->createElement('ptoEmi', $item->numPuntoEmision);
        $xml_sec = $xml->createElement('secuencial', $secuencial);
        $xml_dir = $xml->createElement('dirMatriz', $item->empDireccion);
       // $xml_contri = $xml->createElement('contribuyenteRimpe','CONTRIBUYENTE REGIMEN RIMPE');

        $xml_inf->appendChild($xml_amb);
        $xml_inf->appendChild($xml_tip);
        $xml_inf->appendChild($xml_raz);
        $xml_inf->appendChild($xml_nom);
        $xml_inf->appendChild($xml_ruc);
        $xml_inf->appendChild($xml_cla);
        $xml_inf->appendChild($xml_doc);
        $xml_inf->appendChild($xml_est);
        $xml_inf->appendChild($xml_emi);
        $xml_inf->appendChild($xml_sec);
        $xml_inf->appendChild($xml_dir);
       // $xml_inf->appendChild($xml_contri);
        $xml_fac->appendChild($xml_inf);


        $xml_def = $xml->createElement('infoFactura');
        $xml_fec = $xml->createElement('fechaEmision', date('d/m/Y', strtotime($item->facFechaEmision)));
        $xml_des = $xml->createElement('dirEstablecimiento', $item->empDireccion);
        $xml_obl = $xml->createElement('obligadoContabilidad', $item->empContabilidad);
        if (Str::length($item->info_facRucCedula) == 10) {
            $xml_ide = $xml->createElement('tipoIdentificacionComprador', '05');
        } elseif (Str::length($item->info_facRucCedula) == 13) {
            $xml_ide = $xml->createElement('tipoIdentificacionComprador', '04');
        }
        $xml_rco = $xml->createElement('razonSocialComprador', $item->info_facNombres . " " . $item->info_facApellidos);
        $xml_idc = $xml->createElement('identificacionComprador', $item->info_facRucCedula);
        $xml_dirc = $xml->createElement('direccionComprador', $item->info_facDireccion);
        $xml_tsi = $xml->createElement('totalSinImpuestos', $item->facTotal);
        $xml_tds = $xml->createElement('totalDescuento', '0.00');

        $xml_def->appendChild($xml_fec);
        $xml_def->appendChild($xml_des);
        $xml_def->appendChild($xml_obl);
        $xml_def->appendChild($xml_ide);
        $xml_def->appendChild($xml_rco);
        $xml_def->appendChild($xml_idc);
        $xml_def->appendChild($xml_dirc);
        $xml_def->appendChild($xml_tsi);
        $xml_def->appendChild($xml_tds);

        $xml_imp = $xml->createElement('totalConImpuestos');
        $xml_tim = $xml->createElement('totalImpuesto');
        $xml_tco = $xml->createElement('codigo', '2');
        $xml_cpr = $xml->createElement('codigoPorcentaje', '2');
        if (Session::get('envio') == '1') {
            $xml_bas = $xml->createElement('baseImponible', $item->facTotal+3.50);
        }elseif (Session::get('envio') == '2') {
            $xml_bas = $xml->createElement('baseImponible', $item->facTotal+4.50);
        }

        $xml_val = $xml->createElement('valor', $item->facIva);

        $xml_def->appendChild($xml_imp);
        $xml_imp->appendChild($xml_tim);
        $xml_tim->appendChild($xml_tco);
        $xml_tim->appendChild($xml_cpr);
        $xml_tim->appendChild($xml_bas);
        $xml_tim->appendChild($xml_val);

        $xml_pro = $xml->createElement('propina', '0.00');
        if (Session::get('envio') == '1') {
            $xml_imt = $xml->createElement('importeTotal',  number_format(($item->facTotal + $item->facIva)+3.50,2));
        }elseif (Session::get('envio') == '2') {
            $xml_imt = $xml->createElement('importeTotal',  number_format(($item->facTotal + $item->facIva)+4.50,2));
        }

        $xml_mon = $xml->createElement('moneda', 'DOLAR');

        $xml_pgs = $xml->createElement('pagos');
        $xml_pag = $xml->createElement('pago');
        if (Session::get('pago') == '1') {
            $xml_fpa = $xml->createElement('formaPago', '19');
        } elseif (Session::get('pago') == '2') {
            $xml_fpa = $xml->createElement('formaPago', '20');
        }

        if (Session::get('envio') == '1') {
            $xml_tot = $xml->createElement('total',  number_format(($item->facTotal + $item->facIva)+3.50,2));
        }elseif (Session::get('envio') == '2') {
            $xml_tot = $xml->createElement('total',  number_format(($item->facTotal + $item->facIva)+4.50,2));
        }

        $xml_pla = $xml->createElement('plazo', '1');
        $xml_uti = $xml->createElement('unidadTiempo', 'DIAS');

        $xml_def->appendChild($xml_pro);
        $xml_def->appendChild($xml_imt);
        $xml_def->appendChild($xml_mon);

        $xml_def->appendChild($xml_pgs);
        $xml_pgs->appendChild($xml_pag);
        $xml_pag->appendChild($xml_fpa);
        $xml_pag->appendChild($xml_tot);
        $xml_pag->appendChild($xml_pla);
        $xml_pag->appendChild($xml_uti);
        $xml_fac->appendChild($xml_def);

        $datalle_factura = DB::table('detalle_facturas')
            ->where('factura_id', $item->facId)
            ->select(
                'productos.codigo_producto',
                'detalle_facturas.cantidad',
                'articulos.descripcion',
                'detalle_facturas.preciou',
                'detalle_facturas.total',
            )
            ->join('productos', 'detalle_facturas.producto_id', '=', 'productos.id')
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
            ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
            ->get();

        $xml_dts = $xml->createElement('detalles');
        foreach ($datalle_factura as $detalle) {

            $xml_det = $xml->createElement('detalle');
            $xml_cop = $xml->createElement('codigoPrincipal', $detalle->codigo_producto);
            $xml_dcr = $xml->createElement('descripcion', $detalle->descripcion);
            $xml_can = $xml->createElement('cantidad', $detalle->cantidad);
            $xml_pru = $xml->createElement('precioUnitario', $detalle->preciou);
            $xml_dsc = $xml->createElement('descuento', '0.00');
            $xml_tsm = $xml->createElement('precioTotalSinImpuesto', $detalle->total);

            $xml_ips = $xml->createElement('impuestos');
            $xml_ipt = $xml->createElement('impuesto');
            $xml_cdg = $xml->createElement('codigo', '2');
            $xml_cpt = $xml->createElement('codigoPorcentaje', '2');
            $xml_trf = $xml->createElement('tarifa', '12');
            $xml_bsi = $xml->createElement('baseImponible', $detalle->total);
            $xml_vrl = $xml->createElement('valor',number_format((($detalle->total)*0.12),2) );

            $xml_dts->appendChild($xml_det);
            $xml_det->appendChild($xml_cop);
            $xml_det->appendChild($xml_dcr);
            $xml_det->appendChild($xml_can);
            $xml_det->appendChild($xml_pru);
            $xml_det->appendChild($xml_dsc);
            $xml_det->appendChild($xml_tsm);
            $xml_det->appendChild($xml_ips);

            $xml_ips->appendChild($xml_ipt);
            $xml_ipt->appendChild($xml_cdg);
            $xml_ipt->appendChild($xml_cpt);
            $xml_ipt->appendChild($xml_trf);
            $xml_ipt->appendChild($xml_bsi);
            $xml_ipt->appendChild($xml_vrl);
        }


        //Envio

        $envios=Metodo_Envio::where('id',Session::get('envio'))->get();

        foreach($envios as $envio){

        }
        $xml_det = $xml->createElement('detalle');
        $xml_cop = $xml->createElement('codigoPrincipal', $envio->codigo);
        $xml_dcr = $xml->createElement('descripcion', $envio->descripcion);
        $xml_can = $xml->createElement('cantidad', '1');
        $xml_pru = $xml->createElement('precioUnitario', $envio->precio);
        $xml_dsc = $xml->createElement('descuento', '0.00');
        $xml_tsm = $xml->createElement('precioTotalSinImpuesto', $envio->precio);

        $xml_ips = $xml->createElement('impuestos');
        $xml_ipt = $xml->createElement('impuesto');
        $xml_cdg = $xml->createElement('codigo', '2');
        $xml_cpt = $xml->createElement('codigoPorcentaje', '0');
        $xml_trf = $xml->createElement('tarifa', '0.00');
        $xml_bsi = $xml->createElement('baseImponible', $envio->precio);
        $xml_vrl = $xml->createElement('valor', '0.00');

        $xml_dts->appendChild($xml_det);
        $xml_det->appendChild($xml_cop);
        $xml_det->appendChild($xml_dcr);
        $xml_det->appendChild($xml_can);
        $xml_det->appendChild($xml_pru);
        $xml_det->appendChild($xml_dsc);
        $xml_det->appendChild($xml_tsm);
        $xml_det->appendChild($xml_ips);

        $xml_ips->appendChild($xml_ipt);
        $xml_ipt->appendChild($xml_cdg);
        $xml_ipt->appendChild($xml_cpt);
        $xml_ipt->appendChild($xml_trf);
        $xml_ipt->appendChild($xml_bsi);
        $xml_ipt->appendChild($xml_vrl);

        $xml_fac->appendChild($xml_dts);

        $xml_ifa = $xml->createElement('infoAdicional');
        $xml_cpa1 = $xml->createElement('campoAdicional', $item->info_facNombres . " " . $item->info_facApellidos);
        $nombre = $xml->createAttribute('nombre');
        $nombre->value = 'nombres';
        $users=DB::table('users')->where('id', Auth::user()->id)->get();
        foreach($users as $user){

        }

        $xml_cpa2 = $xml->createElement('campoAdicional', $user->email);
        $correo = $xml->createAttribute('nombre');
        $correo->value = 'email';

        $xml_cpa3 = $xml->createElement('campoAdicional', $item->info_facDireccion);
        $direccion = $xml->createAttribute('nombre');
        $direccion->value = 'direccion';
        $xml_cpa4 = $xml->createElement('campoAdicional', $item->info_facTelefono);
        $telefono = $xml->createAttribute('nombre');
        $telefono->value = 'telefono';

        $xml_fac->appendChild($xml_ifa);
        $xml_cpa1->appendChild($nombre);
        $xml_cpa2->appendChild($correo);
        $xml_cpa3->appendChild($direccion);
        $xml_cpa4->appendChild($telefono);
        $xml_ifa->appendChild($xml_cpa1);
        $xml_ifa->appendChild($xml_cpa2);
        $xml_ifa->appendChild($xml_cpa3);
        $xml_ifa->appendChild($xml_cpa4);

        $xml_fac->appendChild($cabecera);
        $xml_fac->appendChild($cabecerav);
        $xml->appendChild($xml_fac);

      //  echo 'CREADO:' . $xml->save(public_path('Factura\No_Firmado\\' . $claveA . '.xml'));
// if($xml->save(public_path('Factura/No_Firmado/' . $claveA . '.xml'))){
 //    if($xml->save(public_path('Factura\No_Firmado\\' . $claveA . '.xml'))){
    if ($xml->save(public_path('Factura/lib_firma_sri/comprobantes/docGenerados/' . $claveA . '.xml'))) {

        $ruta=new Rutas();
        $ruta->factura_id=Session::get('factura_id');
        //   $ruta->rutagenerado='Factura/No_Firmado/' . $claveA . '.xml';
     //   $ruta->rutagenerado='\Factura\No_Firmado\\' . $claveA . '.xml';
     $ruta->rutagenerado = '\Factura\lib_firma_sri\comprobantes\docGenerados\\' . $claveA . '.xml';
        $ruta->save();

        $numeradores = Numeradores::find(1)->get();
        $numera = Numeradores::find(1);
        foreach($numeradores as $num){
        }
            $valor = ($num->correlativo)+1;
            $numera->id=1;
            $numera->correlativo=$valor;
            $numera->update();

        $factura = Factura::find(Session::get('factura_id'));
        $factura->claveacceso=$claveA;
        $factura->update();


     }

    }
}
