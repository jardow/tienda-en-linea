<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Factura extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'fecha_emision',
        'modalidad_pago',
        'estado',
        'iva',
        'total',

    ];
    protected $table = "facturas";

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
