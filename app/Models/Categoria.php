<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Talla;

class Categoria extends Model
{
    use HasFactory;
    protected $fillable = [
        'descripcion',
        'estado',

    ];
    protected $table = 'categorias';
    protected $primaryKey = 'id';

    public function tallas()
    {
        return $this->hasMany(Talla::class);
     //   return $this->belongsTo(Talla::class);
        // OR return $this->hasOne('App\Phone');
    }

    public function relaciones()
    {
        return $this->hasMany(Relaccion::class);
    }
}
