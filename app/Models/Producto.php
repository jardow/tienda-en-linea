<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $fillable = [
        'relaccion_id',
        'origen_id',
        'codigo_producto',
        'precio',
        'stock',
        'descripcion',
        'fecha_ingreso',
        'estado',
    ];

    public function relaccion(){
        return $this->belongsTo(Relaccion::class);
       }

       public function origen(){
        return $this->belongsTo(Origen::class);
       }





}
