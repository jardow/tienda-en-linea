<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValidarPago extends Model
{
    use HasFactory;

    protected $table = 'validar_pagos';
    protected $primaryKey = 'id';

    protected $fillable = [
        'factura_id',
        'fecha_pago',
        'estado',
        'voucher',
    ];
}
