<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Info_Factura extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $fillable = [
        'factura_id',
        'nombres',
        'apellidos',
        'ruc_cedula',
        'direccion',
        'telefono',

    ];
    protected $table = "info_facturas";
}
