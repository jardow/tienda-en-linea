<?php


namespace App\Models\UseTrail;

trait UserTrait {


    //es: desde aqui
    //en: from here

    public function roles(){
        return $this->belongsToMany('App\Models\Role')->withTimesTamps();
       }

    public function havePermission($permission){
        foreach($this->roles as $role){
            if($role['full-access']=='yes'){
                return true;
            }else{
                return false;
            }
        }
        /*
            foreach ($role->permissions as $perm){
                if($perm->slug ==$permission){
                    return true;
                    }
            }
        return false;
        */

       }


    public function haveRole(){
        foreach($this->roles as $role){
            if($role['slug']=='admin'){
                return true;
            }
        }

        return false;
    }

    public function haveRoleM(){
        foreach($this->roles as $role){
            if($role['slug']=='repartidor'){
                return true;
            }
        }
        return false;
    }

    public function haveRoleL(){
        foreach($this->roles as $role){
            if($role['slug']=='logistica'){
                return true;
            }
        }
        return false;
    }

}





