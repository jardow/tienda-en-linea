<?php

namespace App\Models;

use App\Models\UseTrail\UserTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserResetPassword; //libreria
use Illuminate\Support\Facades\Auth;// liberia

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable,UserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id';
    protected $fillable = [

        'name',
        'surname',
        'idcard',
        'phone',
        'city',
        'avatar',
        'email',
        'state',
        'password',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPassword($token));
    }

    public function adminlte_image(){

        return auth()->user()->avatar;
      //  return 'https://picsum.photos/300/300';
    }

    public function adminlte_desc(){


        $roles=auth::user()->roles;
        foreach($roles as $role){


            if ($role['slug']=='admin') {
                return 'Aministrador'; //Si es Administrador redirige al Home
            } elseif ($role['slug']=='repartidor') {
                return 'Repartidor'; //Si es Administrador redirige al Home
            }elseif ($role['slug']=='logistica') {
                return 'Logistica'; //Si es Administrador redirige al Home
            }
        }



        //return 'Aministrador';
    }

    public function adminlte_profile_url(){
        return 'administrador/perfil';
    }




}
