<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Metodo_Envio extends Model
{
    use HasFactory;
    protected $table = 'metodo_envios';
}
