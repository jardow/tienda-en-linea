<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Seccion;
use App\Models\Categoria;

class Talla extends Model
{
    use HasFactory;
    protected $table = 'tallas';
    protected $primaryKey = 'id';

    protected $fillable = [
        'talla',
        'estado',
        'seccion_id',
        'categoria_id',
    ];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function seccion()
    {
        return $this->belongsTo(Seccion::class);
    }

    public function relaciones()
    {
        return $this->hasMany(Relaccion::class);
    }


}
