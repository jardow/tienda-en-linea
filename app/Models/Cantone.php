<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cantone extends Model
{
    use HasFactory;
    protected $table = "cantones";
    protected $primaryKey = 'id';
    protected $fillable = [
        'provincia_id',
        'canton_id',
        'canton',
    ];
}
