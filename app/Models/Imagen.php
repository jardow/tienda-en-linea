<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    protected $primaryKey = 'id';
    use HasFactory;

    protected $fillable = [
        'url',
        'perfil',

    ];

    public function relacciones(){
        return $this->hasMany(Relaccion::class)->withTimesTamps();
       }

}
