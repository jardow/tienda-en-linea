<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleEnvio extends Model
{
    use HasFactory;
    protected $table = 'detalle_envios';
    protected $primaryKey = 'id';

    protected $fillable = [
        'envio_id',
        'user_id',
        'repartidor_id',
        'guia',
        'estado',
    ];
}
