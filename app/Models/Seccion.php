<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Talla;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Seccion extends Model
{
    use HasFactory;
    protected $table = 'seccions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'descripcion',
        'estado',

    ];

    public function tallas()
    {
        return $this->hasMany(Talla::class);
        // OR return $this->hasOne('App\Phone');
    }

    public function relaciones()
    {
        return $this->hasMany(Relaccion::class);
    }
}
