<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Origen extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $fillable = [
        'descripcion',
        'estado',

    ];

    public function productos(){
        return $this->hasMany(Producto::class);
       }
}
