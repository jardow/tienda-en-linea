<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relaccion_Imagen extends Model
{
    use HasFactory;
    protected $fillable = [
        'imagen_id',
        'relaccion_id',

    ];
    protected $table = 'relaccion__imagens';
    protected $primaryKey = 'id';

    public function imagen(){
        return $this->belongsTo(Imagen::class);
       }

    public function relaccion(){
        return $this->belongsTo(Relaccion::class);
       }



}
