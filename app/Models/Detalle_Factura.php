<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detalle_Factura extends Model
{
    use HasFactory;

    protected $fillable = [
        'producto_id',
        'factura_id',
        'cantidad',
        'total',
    ];

    protected $table = "detalle_facturas";
}
