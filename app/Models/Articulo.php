<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{

    use HasFactory;
    protected $primaryKey = 'id';
    protected $fillable = [
        'descripcion',
        'estado',

    ];

    public function relaciones()
    {
        return $this->hasMany(Relaccion::class);
    }
}
