<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
    protected $primaryKey = 'id';
    use HasFactory;

    protected $fillable = [
        'producto_id',
        'fecha_publicado',
        'descuento',
        'estado',
        'precio_oferta'

    ];
}
