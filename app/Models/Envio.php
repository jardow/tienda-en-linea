<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Envio extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = "envios";
    protected $fillable = [
        'user_id',
        'factura_id',
        'tipo_envio',
        'nombres',
        'ciudad',
        'apellidos',
        'canton',
        'parroquia',
        'referencia',
        'telefono',
        'logistica_id',


    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function factura()
    {
        return $this->belongsTo(Factura::class);
    }


}
