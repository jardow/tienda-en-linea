<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Color;
use App\Models\Categoria;
use App\Models\Seccion;
use App\Models\Articulos;

class Relaccion extends Model
{
    protected $primaryKey = 'id';
    use HasFactory;

    public function productos(){
        return $this->hasMany(Producto::class);
       }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function seccion()
    {
        return $this->belongsTo(Seccion::class);
    }

    public function articulo()
    {
        return $this->belongsTo(Articulo::class);
    }

    public function talla()
    {
        return $this->belongsTo(Talla::class);
    }










}
