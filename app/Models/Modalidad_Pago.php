<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modalidad_Pago extends Model
{
    use HasFactory;
    protected $table = 'modalidad_pagos';
}
