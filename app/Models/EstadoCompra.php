<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstadoCompra extends Model
{
    protected $primaryKey = 'id';
    protected $table = "estado_compras";
    use HasFactory;
}
