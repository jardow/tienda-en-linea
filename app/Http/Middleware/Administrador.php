<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;// liberia

class Administrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $roles=auth::user()->roles;
        foreach($roles as $role){


            if ($role['slug']=='admin') {
                return $next($request); //Si es Administrador redirige al Home

        }


       return redirect('tienda'); //si es usuario normal redirige a la ruta User
    }
}
}
