<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;// liberia

class Cliente
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

            $roles=auth::user()->roles;
            foreach($roles as $role){


                if ($role['slug']=='cliente') {
                    return $next($request); //Si es Cliente
            }




       return redirect('administrador/panel'); //si es usuario normal redirige a la ruta User
    }
}
}
