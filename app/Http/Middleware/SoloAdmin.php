<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;// liberia
use App\Models\User;


use Closure;
use Illuminate\Http\Request;

class SoloAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //return $next($request);
        $roles=auth::user()->roles;
        foreach($roles as $role){


            if ($role['slug']=='admin') {
                return $next($request); //Si es Administrador redirige al Home
            } elseif ($role['slug']=='repartidor') {
                return $next($request); //Si es Administrador redirige al Home
            }elseif ($role['slug']=='logistica') {
                return $next($request); //Si es Administrador redirige al Home
            }
        }


       return redirect('tienda'); //si es usuario normal redirige a la ruta User
      // return redirect()->route('user');
    }

}
