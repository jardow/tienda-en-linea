<?php

namespace App\Http\Controllers;

use App\Models\DatosFacturacionElectronica;
use Illuminate\Http\Request;

class DatosFacturacionElectronicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DatosFacturacionElectronica  $datosFacturacionElectronica
     * @return \Illuminate\Http\Response
     */
    public function show(DatosFacturacionElectronica $datosFacturacionElectronica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DatosFacturacionElectronica  $datosFacturacionElectronica
     * @return \Illuminate\Http\Response
     */
    public function edit(DatosFacturacionElectronica $datosFacturacionElectronica)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DatosFacturacionElectronica  $datosFacturacionElectronica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DatosFacturacionElectronica $datosFacturacionElectronica)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DatosFacturacionElectronica  $datosFacturacionElectronica
     * @return \Illuminate\Http\Response
     */
    public function destroy(DatosFacturacionElectronica $datosFacturacionElectronica)
    {
        //
    }
}
