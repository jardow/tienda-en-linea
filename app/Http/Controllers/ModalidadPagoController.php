<?php

namespace App\Http\Controllers;

use App\Models\Modalidad_Pago;
use Illuminate\Http\Request;

class ModalidadPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modalidad_Pago  $modalidad_Pago
     * @return \Illuminate\Http\Response
     */
    public function show(Modalidad_Pago $modalidad_Pago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modalidad_Pago  $modalidad_Pago
     * @return \Illuminate\Http\Response
     */
    public function edit(Modalidad_Pago $modalidad_Pago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modalidad_Pago  $modalidad_Pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modalidad_Pago $modalidad_Pago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modalidad_Pago  $modalidad_Pago
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modalidad_Pago $modalidad_Pago)
    {
        //
    }
}
