<?php

namespace App\Http\Controllers;

use App\Models\DetalleEnvio;
use Illuminate\Http\Request;

class DetalleEnvioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetalleEnvio  $detalleEnvio
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleEnvio $detalleEnvio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetalleEnvio  $detalleEnvio
     * @return \Illuminate\Http\Response
     */
    public function edit(DetalleEnvio $detalleEnvio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetalleEnvio  $detalleEnvio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetalleEnvio $detalleEnvio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetalleEnvio  $detalleEnvio
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetalleEnvio $detalleEnvio)
    {
        //
    }
}
