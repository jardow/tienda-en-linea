<?php

namespace App\Http\Controllers\Admin;

use App\Models\Factura;
use App\Models\Rutas;
use App\Models\Numeradores;
use App\Models\ValidarPago;
use App\Models\Metodo_Envio;
use Illuminate\Http\Request;
use App\Models\XmlManualTrait;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\DetalleEnvio;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use \Carbon\Carbon;

class MetodoAdminController extends Controller
{

    use XmlManualTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('administrador', ['only' => ['rechazadoPago', 'detalleFacturaAdmin', 'facturaPendiente', 'facturaAutorizado', 'facturaRechazado', 'descargarPDF', 'reporteEntregas', 'generarNumerador','ReporteEntregasGuia']]);
    }

    public function rechazadoPago($id)
    {

        $validar_pago = ValidarPago::find($id);
        $validar_pago->id = $id;
        $validar_pago->fecha_pago = "1988-02-29";
        $validar_pago->estado = 2;
        $validar_pago->update();


        return redirect()->back()
            ->with('status_success', 'Pago actualizado con éxito ');
    }

    public function detalleFacturaAdmin($id)
    {

        $factura_id = Crypt::decrypt($id);

       //  return $factura_id;

        $facturas = DB::table('facturas')
            ->where('facturas.id', $factura_id)
            ->join('users', 'facturas.user_id', '=', 'users.id')
            // ->join('rutas','facturas.id','=','rutas.factura_id')
            ->get();

        //  return $facturas;

        foreach ($facturas as $factura) {
            $metodo_envio = $factura->metodo_envio_id;
        }

        $metodos_envios = Metodo_Envio::where('id', $metodo_envio)->get();


        $detalle_facturas = DB::table('facturas')
            ->where('facturas.id', $factura_id)
            ->join('detalle_facturas', 'facturas.id', '=', 'detalle_facturas.factura_id')
            ->join('productos', 'detalle_facturas.producto_id', '=', 'productos.id')
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
            ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
            ->get();

        //return $detalle_facturas;

        return view('page/admin/detalleFactura/detalleFactura',compact('facturas', 'detalle_facturas', 'metodos_envios'));
        // return $facturas;

    }

    public function facturaPendiente()
    {

        $facturas = DB::table('facturas')
            ->where('facturas.estado_compra_id', 1)
            ->where('validar_pagos.estado', 1)
            ->select(
                'facturas.id',
                'facturas.fecha_emision',
                'facturas.serie',
                'validar_pagos.fecha_pago',
                'validar_pagos.estado'
            )
            ->join('validar_pagos', 'facturas.id', '=', 'validar_pagos.factura_id')
            ->get();

         //   return $facturas;

        return view('page/admin/facturapendiente/index', compact('facturas'));
    }

    public function facturaAutorizado()
    {

        $facturas = DB::table('facturas')
            ->where('rutas.estado', 'AUTORIZADO')
            ->join('rutas', 'facturas.id', '=', 'rutas.factura_id')
            ->orderBy('rutas.factura_id', 'desc')
            ->get();
        return view('page/admin/facturaautorizado/index', compact('facturas'));
    }

    public function facturaRechazado()
    {
        $facturas = DB::table('facturas')
            ->where('rutas.estado', 'RECHAZADO')
            ->orwhere('rutas.estado', 'ERROR')
            ->join('rutas', 'facturas.id', '=', 'rutas.factura_id')
            ->orderBy('rutas.factura_id', 'desc')
            ->get();

        return view('page/admin/facturarechazado/index', compact('facturas'));
    }

    public function descargarPDF($id)
    {

        $factura_id =  Crypt::decrypt($id);
        $rutas = DB::table('facturas')
            ->where('rutas.factura_id', $factura_id)
            ->where('rutas.estado', 'AUTORIZADO')
            ->select('rutas.rutafirmado', 'rutas.rutapdf')
            ->join('rutas', 'facturas.id', '=', 'rutas.factura_id')
            ->get();

        foreach ($rutas as $ruta) {
        }

        if (Storage::disk('local')->exists($ruta->rutapdf)) {
            $attachment = Storage::disk('local')->get($ruta->rutapdf);
            $type = Storage::disk('local')->mimeType($ruta->rutapdf);
            return Response::make($attachment, 200)->header("Content-Type", $type);
        } else {
            return Response::json('This file does not exists.');
        }
    }

    public function reporteEntregas()
    {

        $detalle_envios = DB::table('detalle_envios')
            ->where('detalle_envios.estado', 1)
            ->select(
                'detalle_envios.id',
                'detalle_envios.envio_id',
                'detalle_envios.user_id',
                'detalle_envios.repartidor_id',
                'detalle_envios.guia',
                'detalle_envios.estado',
                'detalle_envios.created_at'
            )
            ->join('envios', 'detalle_envios.envio_id', '=', 'envios.factura_id')
            ->get();

        // return $detalle_envios;

        return view('page/admin/reporte/reporteentrega', compact('detalle_envios'));
    }

    public function generarNumerador($id)
    {
        $factura_id =  Crypt::decrypt($id);
        $rutas=Rutas::where('factura_id',$factura_id)->get();
        if($rutas->isempty()){
            $factura = Factura::find($factura_id);
            $numeradores = Numeradores::get();
            foreach ($numeradores as $numerador) {
                $valor = ($numerador->correlativo);
                $long = 9;
                $secuencial = str_pad($valor, $long, '0', STR_PAD_LEFT);
                $factura->serie = $numerador->establecimiento . '' . $numerador->puntoemision . '' . $secuencial;
            }
            $factura->claveacceso = '0';
            $factura->update();

            $validarPagos=DB::table('validar_pagos')->where('factura_id',$factura_id)->get();
            foreach($validarPagos as $validarPago){

            }

           // return $validarPago->id;

           $validarPago = ValidarPago::find($validarPago->id);
            $validarPago->estado=1;
            $validarPago->update();

            $this->xmlFactura($factura_id); //crear el XML

        }else{
            foreach($rutas as $ruta){
                if(!$ruta->estado=='AUTORIZADO'){

                    $factura = Factura::find($factura_id);
                    $numeradores = Numeradores::get();
                    foreach ($numeradores as $numerador) {
                        $valor = ($numerador->correlativo);
                        $long = 9;
                        $secuencial = str_pad($valor, $long, '0', STR_PAD_LEFT);
                        $factura->serie = $numerador->establecimiento . '' . $numerador->puntoemision . '' . $secuencial;
                    }
                    $factura->claveacceso = '0';
                    $factura->update();

                    $this->xmlFactura($factura_id); //crear el XML

                }
            }

        }
//Cambiar URL

     return  view('page/admin/facturapendiente/index', compact('factura_id'));
      //  return redirect()->back()->with('status_success', 'Información guardada con exito ');
    }

    public function idGenerarFactura($id){
        $factura_id =  Crypt::decrypt($id);
        return  view('page/admin/facturapendiente/index', compact('factura_id'));
    }

    public function comprobante($id){
        $comprobante =  Crypt::decrypt($id);
        $guia=ValidarPago::where('factura_id', $comprobante)->get();

        return view('page/admin/validarpago/guia', compact('guia'));

    }

    public function ReporteEntregasGuia($id){
        $comprobante =  Crypt::decrypt($id);
        $guia=DetalleEnvio::where('id',$comprobante)->get();
        return view('page/admin/reporte/guia', compact('guia'));

    }

    public function productosvendidos(){

        $fechaActual=Carbon::now()->format('Y-m-d');
        $productosvendidos=db::table('detalle_facturas')
        ->join('productos','detalle_facturas.producto_id','=','productos.id')
        ->join('relaccions','productos.relaccion_id','=','relaccions.id')
        ->join('articulos',  'relaccions.articulo_id','=','articulos.id')
       ->join('categorias', 'relaccions.categoria_id','=','categorias.id')
       ->join('seccions', 'relaccions.seccion_id','=','seccions.id')
       ->join('facturas','detalle_facturas.factura_id','=','facturas.id')
       ->join('relaccion__imagens' ,'relaccions.id','=','relaccion__imagens.relaccion_id')
       ->join('imagens','relaccion__imagens.imagen_id','=','imagens.id')
       ->where('imagens.perfil','si')
       ->wherebetween('fecha_emision', ['2021-10-1 00:00:00', $fechaActual])
        ->select('detalle_facturas.producto_id'
        ,'productos.codigo_producto',
        'articulos.descripcion'
        ,'categorias.descripcion as categorias',
        'seccions.descripcion as seccions','imagens.url'
      //  ,'facturas.fecha_emision'
      ,
        db::raw(' sum(detalle_facturas.cantidad) as total_articulos') )
        ->groupby('detalle_facturas.producto_id'
        ,'productos.codigo_producto',
        'articulos.descripcion'
        ,'categorias','seccions','imagens.url'
        //,'facturas.fecha_emision'
        )
        ->orderby('total_articulos','desc')
        ->take(10)
        ->get();

        return view('page/admin/reporte/productosvendidos',compact('productosvendidos'));
    }


    public function productosPublicados(){
   $ProductosPublicados=  DB::table('productos')
   ->where('imagens.perfil','si')
   ->where('publicados.estado','1')
   ->select(

       'imagens.url as imagen',
       'articulos.descripcion as articulo',
       'publicados.fecha_publicado',
       'publicados.producto_id',
       'publicados.id',
       'publicados.estado',
       'productos.stock',
    //   'seccions.descripcion as seccion',
   //    'categorias.descripcion as categoria'




   )
   ->join('publicados', 'productos.id', '=', 'publicados.producto_id')
   ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
   ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
  // ->join('categorias', 'relaccions.articulo_id', '=', 'categorias.id')
 //  ->join('seccions', 'relaccions.articulo_id', '=', 'seccions.id')
   ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
   ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
   ->get();



        return view('page/admin/reporte/productospublicados',compact('ProductosPublicados'));

    }
}
