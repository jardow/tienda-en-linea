<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Origen;
use App\Models\Categoria;
use App\Models\Seccion;

class OrigenController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('administrador',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $origens =  Origen::orderBy('id','Asc')->paginate(5);

        // $roles = Role::get();
       return view('page/admin/origen/index',compact('origens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('page/admin/origen/crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'descripcion'          => 'required|max:50|',
            'estado'   => 'required|in:1,2',

        ]);

        Origen::create($request->all());
        return redirect()->route('origen.index')
        ->with('status_success','Origen guardado con éxito ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Origen $origen)
    {
        return view('page/admin/origen/editar',compact('origen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Origen $origen)
    {
        $request->validate([
            'descripcion'          => 'required|max:50|',
            'estado'   => 'required|in:1,2'
        ]);

        $origen->update($request->all());
        return redirect()->route('origen.index')
        ->with('status_success','Origen actualizado con éxito ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Origen $origen)
    {
        $origen->delete();
        return redirect()->route('origen.index')
        ->with('status_success','Origen sé eliminó  con éxito ');
    }
}
