<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Imagen;
use App\Models\Relaccion;
use App\Models\Relaccion_Imagen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Image; //libreria
use JeroenNoten\LaravelAdminLte\Components\Form\Input;
use Illuminate\Support\Facades\File;

class ImagenController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('administrador', ['only' => ['index', 'create', 'store', 'show', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Imagen $imagen)
    {

        $imagens=Imagen::where('id',$imagen->id)->get();




       //return $imagen;
        return view('page/admin/imagen/editar', compact('imagens', 'imagen'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Imagen $imagen)
    {

        $request->validate([

            'imagen'  => 'required|file|max:1024|mimes:jpeg,bmp,png,jpg',

        ]);


        if ($request->hasfile('imagen')) {
            $avatar = $request->file('imagen');
            $filename = Str::random(12) . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('images\producto\\' . $filename));
            $url = '\images\producto\\' . $filename;


        }

        $image_path = public_path().$imagen->url;
        if($request->hasfile('imagen')){

            if($image_path!=null){
                if (File::exists($image_path)) {
                    //File::delete($image_path);
                    unlink($image_path);
                    $imagen->update([
                        'url' => $url,

                    ]);
                }else{
                    $imagen->update([
                        'url' => $url,

                    ]);
                }









            }

        }







        return redirect()->back()->with('status_success','Imagen actualizado con éxito ');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Imagen $imagen)
    {

    //    $image_path = public_path().'/imgages/products/mi_imagen.jpg';
     //   unlink($image_path);
    }
}
