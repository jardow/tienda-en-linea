<?php

namespace App\Http\Controllers\Admin;

use \Carbon\Carbon;
use App\Models\Color;
use App\Models\Talla;
use Image; //libreria
use App\Models\Imagen;
use App\Models\Origen;
use App\Models\Seccion;
use App\Models\Articulo;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Relaccion;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use GuzzleHttp\Handler\Proxy;
use App\Models\Relaccion_Imagen;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use function PHPUnit\Framework\returnSelf;

class ProductoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('administrador', ['only' => ['index', 'create', 'store', 'show', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = DB::table('productos')
            ->where('imagens.perfil','si')
            ->select(
                'productos.codigo_producto',
                'productos.id',
                'relaccion__imagens.relaccion_id',
                'productos.stock',
                'productos.estado',
                'imagens.url as imagen',
                'articulos.descripcion as articulo',
                'categorias.descripcion as categoria',
                'productos.created_at',
                'imagens.perfil',
                'productos.relaccion_id'
            )
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
            ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
            ->join('categorias', 'relaccions.categoria_id', '=', 'categorias.id')
            ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
            ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
            ->get();
 //return $productos;
        return view('page/admin/producto/index', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::where('estado', '1')->get();
        $seccions = Seccion::where('estado', '1')->get();
        $origens = Origen::where('estado', '1')->get();
        $colors = Color::where('estado', '1')->get();
        $date = Carbon::now()->format('d/m/Y');
        $codProducto =  strtoupper(Str::random(10));
        return view('page/admin/producto/crear', compact('categorias', 'seccions', 'origens', 'colors', 'date', 'codProducto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'cod'      => 'required|max:10',
            'nombre'   => 'required|min:3|max:50',
            'precio'   => 'required|numeric|min:1|max:99',
            'stock'    => 'required|numeric|min:1|max:99',
            'categoria' => 'required|integer|not_in:0',
            'seccion'  => 'required|integer|not_in:0',
            'origen'   => 'required|integer|not_in:0',
            'color'    => 'required|integer|not_in:0',
            'imagen1'  => 'required|file|max:1024|mimes:jpeg,bmp,png,jpg',
            'imagen2'  => 'file|max:1024|mimes:jpeg,bmp,png,jpg',
            'imagen3'  => 'file|max:1024|mimes:jpeg,bmp,png,jpg',
            'imagen4'  => 'file|max:1024|mimes:jpeg,bmp,png,jpg',
            'imagen5'  => 'file|max:1024|mimes:jpeg,bmp,png,jpg',
            'descripcion' => 'required|min:5|max:100',
            'estado'   => 'required|in:1,2'
        ]);

        $imagen1 = new Imagen();
        if ($request->hasfile('imagen1')) {
            $avatar = $request->file('imagen1');
            $filename = Str::random(12) . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('images\producto\\' . $filename));
            $url1 = '\images\producto\\' . $filename;
            $imagen1->url = $url1;
            $imagen1->perfil = "si";
        } else{
            $imagen1->url = "null";
        }


        $imagen2 = new Imagen();
        if ($request->hasfile('imagen2')) {
            $avatar = $request->file('imagen2');
            $filename = Str::random(12) . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('images\producto\\' . $filename));
            $url2 = '\images\producto\\' . $filename;
            $imagen2->url = $url2;
        }else{
            $imagen2->url = "null";
        }
        $imagen3 = new Imagen();
        if ($request->hasfile('imagen3')) {
            $avatar = $request->file('imagen3');
            $filename = Str::random(12) . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('images\producto\\' . $filename));
            $url3 = '\images\producto\\' . $filename;
            $imagen3->url = $url3;
        } else{
            $imagen3->url = "null";
        }
        $imagen4 = new Imagen();
        if ($request->hasfile('imagen4')) {
            $avatar = $request->file('imagen4');
            $filename = Str::random(12) . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('images\producto\\' . $filename));
            $url4 = '\images\producto\\' . $filename;
            $imagen4->url = $url4;
        }else{
            $imagen4->url = "null";
        }
        $imagen5 = new Imagen();
        if ($request->hasfile('imagen5')) {
            $avatar = $request->file('imagen5');
            $filename = Str::random(12) . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('images\producto\\' . $filename));
            $url5 = '\images\producto\\' . $filename;
            $imagen5->url = $url5;
        }else{
            $imagen5->url = "null";
        }

        $data = $request->all();
        $relaccion = new Relaccion();
        $articulo = new Articulo();
        $articulo = new Articulo();
        $relaccion->estado = 'activo';
        $relaccion->color_id = $data['color'];
        $relaccion->categoria_id = $data['categoria'];
        $relaccion->seccion_id = $data['seccion'];
        if ($data['talla'] == 0) {
            $relaccion->talla_id = $data['talla'];
        } else {
            $relaccion->talla_id = $data['talla'];
        }
        $articulo->descripcion = $data['nombre'];
        $articulo->estado = 'activo';
        $articulo->save();
        $relaccion->articulo_id = $articulo->id;
        $relaccion->save();

        $imagen1->save();
        $relaccion_imagen = new Relaccion_Imagen();
        $relaccion_imagen->relaccion_id = $relaccion->id;
        $relaccion_imagen->imagen_id = $imagen1->id;
        $relaccion_imagen->save();
        $relaccion_imagen1 = new Relaccion_Imagen();
        $imagen2->save();
        $relaccion_imagen1->relaccion_id = $relaccion->id;
        $relaccion_imagen1->imagen_id = $imagen2->id;
        $relaccion_imagen1->save();

        $imagen3->save();
        $relaccion_imagen2 = new Relaccion_Imagen();
        $relaccion_imagen2->relaccion_id = $relaccion->id;
        $relaccion_imagen2->imagen_id = $imagen3->id;
        $relaccion_imagen2->save();

        $imagen4->save();
        $relaccion_imagen4 = new Relaccion_Imagen();
        $relaccion_imagen4->relaccion_id = $relaccion->id;
        $relaccion_imagen4->imagen_id = $imagen4->id;
        $relaccion_imagen4->save();

        $imagen5->save();
        $relaccion_imagen5 = new Relaccion_Imagen();
        $relaccion_imagen5->relaccion_id = $relaccion->id;
        $relaccion_imagen5->imagen_id = $imagen5->id;
        $relaccion_imagen5->save();

        $producto = new Producto();
        $producto->relaccion_id = $relaccion->id;
        $producto->origen_id = $data['origen'];
        $producto->codigo_producto = $data['cod'];
        $producto->precio = $data['precio'];
        $producto->stock = $data['stock'];
        $producto->descripcion = $data['descripcion'];
        $date = Carbon::now();
        $producto->fecha_ingreso = $date;
        $producto->estado = $data['estado'];
        // dd($producto);
        $producto->save();
        return redirect()->route('producto.index')
            ->with('status_success', 'Producto guardado con éxito ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
 $idOrigen=0;
        $relaccions = Relaccion::with('articulo', 'categoria', 'seccion', 'talla', 'color')
            ->where('id', $producto->relaccion_id)
            ->get();

        $producto1 = Producto::with('origen')
            ->where('id', $producto->origen_id)
            ->get();
        foreach ($producto1 as $origen) {
            $idOrigen = $origen->id;
        }

        foreach ($relaccions as $relaccion) {
            $idSeccion = $relaccion->seccion->id;
            $idCategoria = $relaccion->categoria->id;
        }



        $imagenes = DB::table('productos')->where('productos.id', $producto->id)
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
            ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
            ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
            ->select(
                'imagens.id',
                'imagens.url as url',
                'perfil'
            )
            ->get();

     //     return $imagenes;

        $categorias = Categoria::get();
        $seccions = Seccion::get();
        $origens = Origen::get();
        $colors = Color::get();

        $tallas = Talla::where('seccion_id', $idSeccion)
            ->where('categoria_id', $idCategoria)
            //  ->where('talla', '!=',0)
            ->get();


      //  return $relaccions;

        return view('page/admin/producto/editar', compact('producto', 'relaccions', 'idOrigen', 'imagenes', 'categorias', 'seccions', 'origens', 'colors', 'tallas','producto1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {

      // return $request->all();

        $request->validate([

            'precio'   => 'required|numeric|min:1|max:99',
            'stock'    => 'required|numeric|min:1|max:99',
            'descripcion' => 'required|min:5|max:200',
            'estado'   => 'required|in:1,2'
        ]);


        $producto->update([
            'precio' => $request->input('precio'),
            'stock' => $request->input('stock'),
            'estado' => $request->input('estado'),
            'descripcion' => $request->input('descripcion'),

        ]);
        return redirect()->back()->with('status_success','Producto actualizado con éxito ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
