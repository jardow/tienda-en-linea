<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categoria;
use App\Models\Relaccion;
use App\Models\Seccion;
use Illuminate\Http\Request;
use App\Models\Talla;
use Illuminate\Support\Facades\DB; //libre

class TallaController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('administrador',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $tallas=Talla::with('seccion','categoria')
     ->get();
   // $relacion=Relaccion::with('color','articulo','categoria', 'seccion')->get();
   //  return $relacion;
 return view('page/admin/talla/index',compact('tallas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias=Categoria::where('descripcion','Ropa')
        ->orwhere('descripcion','Calzado')
        ->get();
        $seccions=Seccion::where('descripcion','Hombre')
        ->orwhere('descripcion','Mujer')
        ->orwhere('descripcion','Ñiños')
        ->get();
        return view('page/admin/talla/crear',compact('categorias','seccions'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Talla $talla)
    {
        $request->validate([
            'descripcion'          => 'required|max:50|',
            'estado'   => 'required|in:1,2',
            'categoria'=>'required|in:1,2',
            'seccion'=>'required|in:1,2,3',

        ]);

        $data=$request->all();
        $talla=new Talla();
        $talla->talla=$data['descripcion'];
        $talla->estado=$data['estado'];
        $talla->categoria_id=$data['categoria'];
        $talla->seccion_id=$data['seccion'];
        $talla->save();
        return redirect()->route('talla.index')
        ->with('status_success','Talla guardado con éxito ');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Talla $talla)
    {


       $categoria_id=$talla->categoria_id;
       $seccion_id=$talla->seccion_id;

        $categorias=Categoria::where('descripcion','Ropa')
        ->orwhere('descripcion','Calzado')
        ->get();
        $seccions=Seccion::where('descripcion','Hombre')
        ->orwhere('descripcion','Mujer')
        ->orwhere('descripcion','Ñiños')
        ->get();

        return view('page/admin/talla/editar',compact('categorias','seccions','talla','categoria_id','seccion_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Talla $talla)
    {
        //  return $talla;
        $request->validate([
            'descripcion'          => 'required|max:50|',
            'estado'   => 'required|in:activo,deshabilitado',
            'categoria'=>'required|in:1,2',
            'seccion'=>'required|in:1,2,3',
        ]);
        $data=$request->all();
        $talla->id=$talla->id;
        $talla->talla=$data['descripcion'];;
        $talla->estado=$data['estado'];
        $talla->categoria_id=$data['categoria'];
        $talla->seccion_id=$data['seccion'];
        $talla->update($request->all());
        return redirect()->route('talla.index')
        ->with('status_success','Talla actualizado con éxito ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Talla $talla)
    {
        $talla->delete();
        return redirect()->route('talla.index')
        ->with('status_success','Talla sé eliminó  con éxito ');
    }
}
