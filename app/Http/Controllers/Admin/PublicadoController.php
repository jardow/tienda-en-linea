<?php

namespace App\Http\Controllers\Admin;

use App\Models\Publicado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PublicadoController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('administrador',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $productos = DB::table('productos')
        ->where('imagens.perfil','si')
        ->select(

            'imagens.url as imagen',
            'articulos.descripcion as articulo',
            'publicados.fecha_publicado',
            'publicados.producto_id',
            'publicados.id',
            'publicados.estado',



        )
        ->join('publicados', 'productos.id', '=', 'publicados.producto_id')
        ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
        ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
        ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
        ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
        ->get();



       return view('page/admin/publicado/index',compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
