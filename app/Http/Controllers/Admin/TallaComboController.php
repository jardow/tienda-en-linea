<?php

namespace App\Http\Controllers\Admin;

use \Carbon\Carbon;
use App\Models\User;
use App\Models\Color;
use App\Models\Talla;
use App\Models\Oferta;
use App\Models\Origen;
use App\Models\Seccion;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Publicado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class TallaComboController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('administrador', ['only' => ['consultaTallas', 'usuarioEstado', 'colorEstado', 'seccionEstado', 'tallaEstado', 'origenEstado', 'publicarProducto','estadoPublicado','ofertarProducto','estadoOferta','eliminarOferta']]);
    }
    function consultaTallas(Request $request)
    {



        $categoria = $request->categoria;
        $seccion = $request->seccion;
        $tallas = Talla::where('categoria_id', $categoria)
            ->where('seccion_id', $seccion)
            ->where('estado', '1')
            ->get();
        return response()->json(['data' => $tallas]);
    }

    function usuarioEstado(Request $request)
    {
        $user = User::findOrFail($request->id);
        if ($request->state == "1") {
            $user->update([
                'state' => "2",
            ]);
            return redirect()->back()->with('status_success', 'Estado de ' . $request->name . ' actualizado con éxito');
        } else {
            $user->update([
                'state' => "1",
            ]);
            return redirect()->back()->with('status_success', 'Estado de ' . $request->name . ' actualizado con éxito');
        }
    }

    function categoriaEstado(Request $request)
    {

        $categoria = Categoria::findOrFail($request->id);
        if ($request->estado == "1") {
            $categoria->update([
                'estado' => "2",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->descripcion . ' actualizado con éxito');
        } else {
            $categoria->update([
                'estado' => "1",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->descripcion . ' actualizado con éxito');
        }
    }

    function colorEstado(Request $request)
    {

        $color =Color::findOrFail($request->id);
        if ($request->estado == "1") {
            $color->update([
                'estado' => "2",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->descripcion . ' actualizado con éxito');
        } else {
            $color->update([
                'estado' => "1",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->descripcion . ' actualizado con éxito');
        }
    }

    function seccionEstado(Request $request)
    {
        $seccion = Seccion::findOrFail($request->id);
        if ($request->estado == "1") {
            $seccion->update([
                'estado' => "2",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->descripcion . ' actualizado con éxito');
        } else {
            $seccion->update([
                'estado' => "1",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->descripcion . ' actualizado con éxito');
        }
    }

    function tallaEstado(Request $request)
    {

        $talla = Talla::findOrFail($request->id);
        if ($request->estado == "1") {
            $talla->update([
                'estado' => "2",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->talla . ' actualizado con éxito');
        } else {
            $talla->update([
                'estado' => "1",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->talla . ' actualizado con éxito');
        }
    }

    function origenEstado(Request $request)
    {

        $origen = Origen::findOrFail($request->id);
        if ($request->estado == "1") {
            $origen->update([
                'estado' => "2",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->descripcion . ' actualizado con éxito');
        } else {
            $origen->update([
                'estado' => "1",
            ]);
            return redirect()->back()->with('status_success', 'Estado ' . $request->descripcion . ' actualizado con éxito');
        }
    }
    function publicarProducto(Request $request)
    {

        $fechaPublicado = Carbon::now()->format('Y-m-d');
        $publicado = Publicado::create([
            'producto_id' => $request->producto_id,
            'estado' => 1,
            'fecha_publicado' => $fechaPublicado,
        ]);
        if ($publicado != null) {
            $producto = Producto::findOrFail($request->producto_id);
            $producto->update([
                'id' => $request->producto_id,
                'estado' => 2,
            ]);
        }
        return redirect()->back()->with('status_success', 'Producto publicado con éxito');
    }

    function estadoPublicado(Request $request)
    {

        $publicado = Publicado::findOrFail($request->id);
        if ($request->estado == "1") {
            $publicado->update([
                'estado' => "2",
            ]);
            return redirect()->back()->with('status_success', 'Estado  actualizado con éxito');
        } elseif($request->estado == "2") {
            $publicado->update([
                'estado' => "1",
            ]);
            return redirect()->back()->with('status_success', 'Estado  actualizado con éxito');
        }
    }

    function ofertarProducto(Request $request)
    {
        $descuento=0.20;
        $fechaPublicado = Carbon::now()->format('Y-m-d');
        $precio=Producto::findOrFail($request->producto_id)->precio;
        $precio_oferta=$precio-($precio*$descuento);



        $oferta = Oferta::create([
            'producto_id' => $request->producto_id,
            'estado' => 1,
            'fecha_publicado' => $fechaPublicado,
            'descuento'=>$descuento*100,
            'precio_oferta'=>$precio_oferta,

        ]);

        if ($oferta != null) {
            $publicado = Publicado::findOrFail($request->publicado_id);
            $publicado->update([
                'id' => $request->publicado_id,
                'estado' =>3,
            ]);


        }
        return redirect()->back()->with('status_success', 'Estado  actualizado con éxito');

    }

    function estadoOferta(Request $request){

        $oferta = Oferta::findOrFail($request->id);
        if ($request->estado == "1") {
            $oferta->update([
                'estado' => "2",
            ]);
            return redirect()->back()->with('status_success', 'Estado  actualizado con éxito');
        } elseif($request->estado == "2") {
            $oferta->update([
                'estado' => "1",
            ]);
            return redirect()->back()->with('status_success', 'Estado  actualizado con éxito');
        }
    }

    function eliminarOferta(Request $request){

            $publicado = Publicado::findOrFail($request->idpublicado);
          //  return $publicado;
            $publicado->update([
                'id' => $request->idpublicado,
                'estado' =>2,
            ]);

            $oferta = Oferta::findOrFail($request->id);
            $oferta->delete();
            return redirect()->back()->with('status_success', 'Estado  actualizado con éxito');
    }


}
