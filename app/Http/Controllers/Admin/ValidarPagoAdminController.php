<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ValidarPagoAdminController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('administrador',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /*
        1) Pendiente
        2) Proceso de Varificación
        3) Pagado
        */

        $facturas=DB::table('facturas')
       ->where('facturas.estado_compra_id',2)
        ->where('validar_pagos.estado',3)
    //  ->ORwhere('validar_pagos.estado',1)
        ->select('facturas.id',
                 'facturas.fecha_emision',
                 'facturas.total',
                 'facturas.iva',
                 'facturas.metodo_envio_id',
                 'validar_pagos.id as validarPagos_id',
                 'validar_pagos.estado',
                 'validar_pagos.fecha_pago',
                 'validar_pagos.voucher' )
        ->join('validar_pagos', 'facturas.id', '=', 'validar_pagos.factura_id')
       // ->join('rutas','facturas.id','=','rutas.factura_id')
        ->get();
        //return $facturas;
        return view('page/admin/validarpago/index', compact('facturas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
