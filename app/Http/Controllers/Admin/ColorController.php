<?php

namespace App\Http\Controllers\Admin;

use App\Models\Color;
use App\Models\Estado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColorController extends Controller
{


   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('administrador',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors =  Color::get();
        // $roles = Role::get();
       return view('page/admin/color/index',compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$estados=Estado::get();
        return view('page/admin/color/crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'descripcion'          => 'required|max:50|',
            'estado'   => 'required|in:1,2'
        ]);

        Color::create($request->all());
        return redirect()->route('color.index')
        ->with('status_success','Color guardado con éxito ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color)
    {
        $colors="dd";
       // return view('page/admin/color/mostrar',compact('color'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    {

        return view('page/admin/color/editar',compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Color $color)
    {
        $request->validate([
            'descripcion'          => 'required|max:50|',
            'estado'   => 'required|in:1,2'
        ]);

        $color->update($request->all());
        return redirect()->route('color.index')
        ->with('status_success','Color actualizado con éxito ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        $color->delete();
        return redirect()->route('color.index')
        ->with('status_success','Color sé eliminó  con éxito ');
    }
}
