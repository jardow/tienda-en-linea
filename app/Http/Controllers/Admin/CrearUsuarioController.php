<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class CrearUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('page/admin/crearusuario/crearusuario');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => ['required', 'string', 'max:20', 'min:3'],
            'apellido' => ['required', 'string', 'max:20', 'min:3'],
            'idcard' => ['required', 'string', 'max:10','min:10','unique:users'],
            'telefono' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'rol'   => 'required|in:2,4'


        ]);

        $user=User::create([
            'name' => $request->nombre,
            'surname' => $request->apellido,
            'idcard' => $request->idcard,
            'phone' => $request->telefono,
            'city' => "Quito",
            'avatar' =>'\images\perfil\\'.$request->avatar,
            'email' => $request->email,
            'state' => '1',
            'password' => Hash::make('admin'),
        ]);
        //Linea para Asginar Rol CLiente a cada cuenta creada
       $user->roles()->sync([$request->rol]);

       return redirect()->route('reporte_usuario')
       ->with('status_success', 'Usuario creado con éxito ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
