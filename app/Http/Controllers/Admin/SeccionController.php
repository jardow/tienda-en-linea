<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Seccion;

class SeccionController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('administrador',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seccions =  Seccion::get();
        // $roles = Role::get();
       return view('page/admin/seccion/index',compact('seccions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page/admin/seccion/crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'descripcion'          => 'required|max:50|',
            'estado'   => 'required|in:activo,deshabilitado'
        ]);

        Seccion::create($request->all());
        return redirect()->route('seccion.index')
        ->with('status_success','Sección guardado con éxito ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Seccion $seccion)
    {
        return view('page/admin/seccion/editar',compact('seccion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Seccion $seccion)
    {
        $request->validate([
            'descripcion'          => 'required|max:50|',
            'estado'   => 'required|in:activo,deshabilitado'
        ]);

        $seccion->update($request->all());
        return redirect()->route('seccion.index')
        ->with('status_success','Sección actualizado con éxito ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seccion $seccion)
    {
        $seccion->delete();
        return redirect()->route('seccion.index')
        ->with('status_success','Sección sé eliminó  con éxito ');
    }
}
