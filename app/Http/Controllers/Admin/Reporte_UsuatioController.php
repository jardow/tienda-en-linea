<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Reporte_UsuatioController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('administrador',['only'=>['reporteUsuario', 'reporteCliente']]);

    }

    // Reporte Usuarios con  Rol

    public function reporteUsuario(){

        $usuarios=DB::table('users')
        ->where('slug', 'repartidor')
       ->orWhere('slug', 'logistica')
        ->join('role_user', 'users.id' ,'=', 'role_user.user_id')
        ->join('roles', 'role_user.role_id','=', 'roles.id')
        ->select('users.id','users.name',
        'users.surname','users.phone',
        'users.city' , 'users.email',
        'users.state',
        'roles.rol')
        ->get();
     //  return $usuarios;

        return view('page/admin/reporte/usuario', compact('usuarios'));
    }

    // Reporte Cliente  con  Rol

    public function reporteCliente(){

        $usuarios=DB::table('users')
        ->where('slug', 'cliente')
        ->join('role_user', 'users.id' ,'=', 'role_user.user_id')
        ->join('roles', 'role_user.role_id','=', 'roles.id')
        ->select('users.id','users.name',
        'users.surname','users.phone',
        'users.city' , 'users.email',
        'users.state',
        'roles.rol')
        ->get();
     //   return $usuarios;
        return view('page/admin/reporte/cliente', compact('usuarios'));

    }
}
