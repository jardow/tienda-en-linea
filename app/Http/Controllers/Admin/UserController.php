<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Image; //libreria
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;// liberia

class UserController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('soloadmin',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = auth::user();
        $email = $users['email'];
        $users=User::where('users.email', $email)
        ->get();
    return view('page/admin/perfil/index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $iduser =  Crypt::decrypt($id);
        $users=User::where('id', $iduser)
        ->get();

  //  return $users;
        return view('page/admin/perfil/editar', compact('users','iduser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([

            'nombre'   => 'required|min:3|max:50',
            'apellido'   => 'required|min:3|max:50',
            'celular'   => 'required|min:3|max:50',
            'direccion'   => 'required|min:3|max:200',
            'imagen'  => '|file|max:1024|mimes:jpeg,bmp,png,jpg',

        ]);
        $url=null;
        if($request->hasfile('imagen')){
            $avatar = $request->file('imagen');
            $filename = Str::random(12) . '.' . $avatar->getClientOriginalExtension();
               Image::make($avatar)->resize(300, 300)->save( public_path('images\perfil\\' . $filename) );
               $url= '\images\perfil\\' . $filename;
            }

        $user = User::findOrFail($id);
        $image_path = public_path().$user->avatar;
        $user->id = $id;
        $user->name = $request->nombre;
        $user->surname = $request->apellido;
       // $user->idcard = $request->cedula;
        $user->phone = $request->celular;
        $user->city = $request->direccion;
        if($request->hasfile('imagen')){
            unlink($image_path);
            $user->avatar =$url;

        }





        $user->update();

        $users = auth::user();
        $email = $users['email'];
        $users=User::where('users.email', $email)
        ->get();



        return redirect()->route('perfil.index')
        ->with('status_success', 'Usuario actualizado con éxito ');

      //  return view('page/admin/perfil/index', compact('users'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
