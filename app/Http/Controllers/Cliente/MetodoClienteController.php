<?php

namespace App\Http\Controllers\Cliente;

use App\Models\DetalleEnvio;
use App\Models\Metodo_Envio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Mail\ContactanosMailable;   //libre
use App\Models\Cantone;
use App\Models\Parroquia;
use Illuminate\Support\Facades\Mail;  //libre

class MetodoClienteController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('cliente', ['only' => ['detalleFaturaCliente', 'validarPagoCliente', 'envio', 'descargarPDF', 'descargarXML', 'detalleFaturaClientePendiente', 'guia']]);
    }

    public function detalleFaturaCliente(Request $request)
    {

        $factura_id = Crypt::decrypt($request->id);

        //  return $factura_id;

        $facturas = DB::table('facturas')
            ->where('facturas.id', $factura_id)
            ->join('users', 'facturas.user_id', '=', 'users.id')
            ->join('rutas', 'facturas.id', '=', 'rutas.factura_id')
            ->get();

        //return $facturas;

        foreach ($facturas as $factura) {
            $metodo_envio = $factura->metodo_envio_id;
        }

        // return $metodo_envio;

        $metodos_envios = Metodo_Envio::where('id', $metodo_envio)->get();


        $detalle_facturas = DB::table('facturas')
            ->where('facturas.id', $factura_id)
            ->join('detalle_facturas', 'facturas.id', '=', 'detalle_facturas.factura_id')
            ->join('productos', 'detalle_facturas.producto_id', '=', 'productos.id')
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
            ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
            ->get();

        //return $detalle_facturas;

        return view('page/cliente/detalleCliente/detalleCliente', compact('facturas', 'detalle_facturas', 'metodos_envios'));
    }

    public function detalleFaturaClientePendiente(Request $request)
    {

        $factura_id = Crypt::decrypt($request->id);

        //  return $factura_id;

        $facturas = DB::table('facturas')
            ->where('facturas.id', $factura_id)
            ->join('users', 'facturas.user_id', '=', 'users.id')
            // ->join('rutas','facturas.id','=','rutas.factura_id')
            ->get();

        //  return $facturas;

        foreach ($facturas as $factura) {
            $metodo_envio = $factura->metodo_envio_id;
        }

        $metodos_envios = Metodo_Envio::where('id', $metodo_envio)->get();


        $detalle_facturas = DB::table('facturas')
            ->where('facturas.id', $factura_id)
            ->join('detalle_facturas', 'facturas.id', '=', 'detalle_facturas.factura_id')
            ->join('productos', 'detalle_facturas.producto_id', '=', 'productos.id')
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
            ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
            ->get();

        //return $detalle_facturas;

        return view('page/cliente/detalleCliente/detalleClientePendiente', compact('facturas', 'detalle_facturas', 'metodos_envios'));
    }


    public function validarPagoCliente(Request $request)
    {
    }

    public function envio()
    {
        $detalle_envios = DB::table('detalle_envios')
            ->where('users.id', Auth::user()->id)
            ->join('facturas', 'detalle_envios.envio_id', '=', 'facturas.id')
            ->join('users', 'facturas.user_id', '=', 'users.id')
            ->select(
                'detalle_envios.id',
                'detalle_envios.envio_id',
                'detalle_envios.repartidor_id',
                'detalle_envios.guia',
                'detalle_envios.created_at',
                'detalle_envios.estado'
            )
            ->get();
        //  return $detalle_envios;

        return view('page/cliente/envio/envio', compact('detalle_envios'));
    }

    public function descargarPDF($id)
    {

        $factura_id =  Crypt::decrypt($id);
        $rutas = DB::table('facturas')
            ->where('rutas.factura_id', $factura_id)
            ->where('facturas.user_id', Auth::user()->id)
            ->where('rutas.estado', 'AUTORIZADO')
            ->select('rutas.rutafirmado', 'rutas.rutapdf')
            ->join('rutas', 'facturas.id', '=', 'rutas.factura_id')
            ->get();

        foreach ($rutas as $ruta) {
        }

        if (Storage::disk('local')->exists($ruta->rutapdf)) {
            $attachment = Storage::disk('local')->get($ruta->rutapdf);
            $type = Storage::disk('local')->mimeType($ruta->rutapdf);
            return Response::make($attachment, 200)->header("Content-Type", $type);
        } else {
            return Response::json('This file does not exists.');
        }
    }

    public function descargarXML($id)
    {

        $factura_id =  Crypt::decrypt($id);
        $rutas = DB::table('facturas')
            ->where('rutas.factura_id', $factura_id)
            ->where('facturas.user_id', Auth::user()->id)
            ->where('rutas.estado', 'AUTORIZADO')
            ->select('rutas.rutafirmado', 'rutas.rutapdf')
            ->join('rutas', 'facturas.id', '=', 'rutas.factura_id')
            ->get();

        foreach ($rutas as $ruta) {
        }

        if (Storage::disk('local')->exists($ruta->rutafirmado)) {
            $attachment = Storage::disk('local')->get($ruta->rutafirmado);
            $type = Storage::disk('local')->mimeType($ruta->rutafirmado);

            return Response::make($attachment, 200)->header('Content-Type', "application/json", $type);

            return Response::json('This file does not exists.');
        }
    }

    public function guia($id)
    {
        $envio_id =  Crypt::decrypt($id);
        $guia = DetalleEnvio::where('envio_id', $envio_id)->get();
        return view('page/cliente/envio/guia', compact('guia'));
    }


    public function contactanos()
    {
        return view('page/cliente/contactanos/contacto');
    }

    public function enviarContacto(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:50|min:3',
            'correo' => 'required|email|max:50|min:6',
            'telefono' => 'required|max:10|min:9',
            'mensaje' => 'required|max:300|min:25',

        ]);

        Session::put("nombre", $request->nombre);
        Session::put("correo", $request->correo);
        Session::put("telefono", $request->telefono);
        Session::put("mensaje", $request->mensaje);



        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            $correo = new ContactanosMailable;

            Mail::to('facturacion.store.valentina@gmail.com')->send($correo);
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('status_success', 'Información enviado con exito ', compact('request'));
        }
    }

    public function acercaDe()
    {

        return view('page/cliente/acercade/acercade');
    }

    public function pedidosPendientes()
    {

        $pendientes = DB::table('facturas')
            ->where('user_id', Auth::user()->id)
            ->where('facturas.estado_compra_id', 1)
            ->where('logistica_id', null)
            ->join('envios', 'facturas.id', '=', 'envios.factura_id')
            ->get();

        // return $pendientes;

        return view('page/cliente/pendiente/pendiente', compact('pendientes'));
    }

    public function cantones(Request $request)
    {
        Session::put('provincia_id',$request->canton);

        if (isset($request->canton)) {
            $cantones = Cantone::where('provincia_id', $request->canton)->get();
            return response()->json(
                [
                    'lista' => $cantones,
                    'success' => true
                ]
            );
        } else {
            return response()->json(
                [

                    'success' => false
                ]
            );
        }
    }

    public function parroquias(Request $request)
    {

        if (isset($request->parroquia)) {

            $parroquias = Parroquia::where('canton_id', $request->parroquia)
            ->where('provincia_id',Session::get('provincia_id'))
            ->get();


            return response()->json(
                [
                    'listap' => $parroquias,
                    'success' => true
                ]
            );
        } else {
            return response()->json(
                [
                    'success' => false
                ]
            );
        }
    }
}
