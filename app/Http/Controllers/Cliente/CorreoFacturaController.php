<?php

namespace App\Http\Controllers\Cliente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ContactanosMailable;   //libre
use Illuminate\Support\Facades\Mail;  //libre

class CorreoFacturaController extends Controller
{
    public function envioFactura(Request $request){
        $correo = new ContactanosMailable;

        Mail::to('bacoth@gmail.com')->send($correo);

    }
}
