<?php

namespace App\Http\Controllers\Cliente;

use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('cliente',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provincias=DB::table('provincias')
        ->get();
        $cantones=DB::table('cantones')
        ->get();
        $parroquias=DB::table('parroquias')
        ->get();



        if(!Session::has("proceso")){
            Session::put("proceso", 1);
        }
        $cartCollection = \Cart::getContent();
        $total=\Cart::getTotal();
        Session::put("TOTAL", number_format($total,2));
        Session::put("IVA", number_format($total*0.12,2));

        if(\Cart::getTotal()!=0){
            return view('page/cliente/checkout/checkout')
            ->with(['cartCollection' => $cartCollection, 'total'=>$total, 'provincias'=>$provincias, 'cantones'=>$cantones, 'parroquias'=>$parroquias]);
        }else{

            // dd($cartCollection);
            return  redirect()->back();
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
