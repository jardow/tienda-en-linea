<?php

namespace App\Http\Controllers\Cliente;

use \Carbon\Carbon;
use Image; //libreria
use App\Models\ValidarPago;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ValidarPagoClienteController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('cliente',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facturas=DB::table('facturas')
        ->where('facturas.estado_compra_id',2)
        ->where('users.id', Auth::user()->id )
        ->select('facturas.id',
                 'facturas.fecha_emision',
                 'validar_pagos.id as validarPagos_id',
                 'validar_pagos.estado',
                 'validar_pagos.voucher',
                 'validar_pagos.fecha_pago' )
        ->join('validar_pagos', 'facturas.id', '=', 'validar_pagos.factura_id')
        ->join('users','facturas.user_id','=','users.id')
        ->get();
 //return $facturas;
        return view('page/cliente/validarpago/validarpago', compact('facturas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'voucher'  => 'required|file|max:1024|mimes:jpeg,bmp,png,jpg',
        ]);

        if($request->hasfile('voucher')){
            $avatar = $request->file('voucher');
            $filename = Str::random(12) . '.' . $avatar->getClientOriginalExtension();
               Image::make($avatar)->resize(800, 800)->save( public_path('images\voucher\\' . $filename) );
               $url= '\images\voucher\\' . $filename;
            }

            $validar_pago = ValidarPago::find($id);
            $validar_pago->id=$id;
            $validar_pago->estado=3;
            $validar_pago->fecha_pago=$date =  Carbon::now()->format('Y-m-d');;
            $validar_pago->voucher=$url;
            $validar_pago->update();

            return redirect()->back()
            ->with('status_success','Pago actualizado con éxito ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
