<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

class OfertaDetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idProducto =  Crypt::decrypt($id);

        $producto = DB::table('productos')
            ->where('productos.id',$idProducto)
            ->where('imagens.perfil','si')
            ->select(
                'productos.codigo_producto',
                'productos.id',
                'origens.descripcion as origen',
                'productos.stock',
                'productos.precio',
                'ofertas.precio_oferta',
                'ofertas.descuento',
                'productos.descripcion',
                'categorias.descripcion as categoria',
                'articulos.descripcion as nombre',
                'colors.descripcion as color',
                'tallas.talla',
                'seccions.descripcion as seccion',
                'imagens.url as imagen'


            )
            ->join('ofertas', 'productos.id', '=', 'ofertas.producto_id')
            ->join('publicados', 'productos.id', '=', 'publicados.producto_id')
            ->join('origens', 'productos.origen_id', '=', 'origens.id')
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
            ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
            ->join('colors', 'relaccions.color_id', '=', 'colors.id')
            ->join('tallas', 'relaccions.talla_id', '=', 'tallas.id')
            ->join('seccions', 'relaccions.seccion_id', '=', 'seccions.id')
            ->join('categorias', 'relaccions.categoria_id', '=', 'categorias.id')
            ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
            ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
            ->get();


            $imagenes = DB::table('productos')
            ->where('productos.id',$idProducto)
            ->select(
                'imagens.url'
            )
            ->join('origens', 'productos.origen_id', '=', 'origens.id')
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
            ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
            ->join('colors', 'relaccions.color_id', '=', 'colors.id')
            ->join('tallas', 'relaccions.talla_id', '=', 'tallas.id')
            ->join('seccions', 'relaccions.seccion_id', '=', 'seccions.id')
            ->join('categorias', 'relaccions.categoria_id', '=', 'categorias.id')
            ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
            ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
            ->get();


       //

        return view('page/cliente/detalle/detalleOferta', compact('idProducto', 'producto', 'imagenes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
