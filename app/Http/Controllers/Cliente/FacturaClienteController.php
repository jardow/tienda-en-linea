<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FacturaClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('cliente', ['only' => ['factura']]);
    }

    public function factura()
    {

        $rutas = DB::table('facturas')
            ->where('facturas.user_id', Auth::user()->id)
            ->where('rutas.estado', 'AUTORIZADO')
            ->select(
                'rutas.rutafirmado',
                'rutas.rutapdf',
                'facturas.fecha_emision',
                'facturas.serie',
                'rutas.factura_id'
            )
            ->join('rutas', 'facturas.id', '=', 'rutas.factura_id')
            ->get();

        return view('page/cliente/factura/factura', compact('rutas'));
    }
}
