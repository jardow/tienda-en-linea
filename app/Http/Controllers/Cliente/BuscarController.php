<?php

namespace App\Http\Controllers\Cliente;

use App\Models\Seccion;
use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class BuscarController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['auth', 'verified']);
       // $this->middleware('cliente', ['only' => ['buscar']]);
    }

    public function buscar(Request $request){


        if(!empty($request->all())){
            $categorias=Categoria::where('estado','1')
            ->get();

            $seccions=Seccion::where('estado','1')
            ->get();


            $products = DB::table('productos')
            ->where('imagens.perfil', 'si')
            ->where('publicados.estado','1')
            ->where('categorias.id',$request->categoria)
          //  ->where('seccions.id', $request->seccion)
            ->select(
                'productos.codigo_producto',
                'productos.id',
                'relaccion__imagens.relaccion_id',
                'productos.stock',
                'productos.estado',
                'productos.precio',
                'imagens.url as imagen',
                'articulos.descripcion as articulo',
                'categorias.descripcion as categoria',
                'productos.created_at',
                'imagens.perfil',
                'imagens.url'
            )
            ->join('publicados', 'productos.id', '=', 'publicados.producto_id')
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
           // ->join('seccions', 'relaccions.seccion_id', '=', 'seccions.id')
            ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
            ->join('categorias', 'relaccions.categoria_id', '=', 'categorias.id')
            ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
            ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
          //; ->cursorPaginate(12);
        //  ->simplePaginate(12);
            ->paginate(12);

            return view('page/cliente/catalogo/catalogo')->with(['products' => $products,'categorias' =>$categorias, 'seccions' => $seccions]);

        }else{

            $categorias=Categoria::where('estado','1')
        ->get();

        $seccions=Seccion::where('estado','1')
        ->get();


        $products = DB::table('productos')
        ->where('imagens.perfil', 'si')
        ->where('publicados.estado','1')
        ->select(
            'productos.codigo_producto',
            'productos.id',
            'relaccion__imagens.relaccion_id',
            'productos.stock',
            'productos.estado',
            'productos.precio',
            'imagens.url as imagen',
            'articulos.descripcion as articulo',
            'categorias.descripcion as categoria',
            'productos.created_at',
            'imagens.perfil',
            'imagens.url'
        )
        ->join('publicados', 'productos.id', '=', 'publicados.producto_id')
        ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
        ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
        ->join('categorias', 'relaccions.categoria_id', '=', 'categorias.id')
        ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
        ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
      //; ->cursorPaginate(12);
    //  ->simplePaginate(12);
        ->paginate(12);

        return view('page/cliente/catalogo/catalogo')->with(['products' => $products,'categorias' =>$categorias, 'seccions' => $seccions]);
        }


    }




}
