<?php

namespace App\Http\Controllers\Cliente;

use \Carbon\Carbon;

use App\Models\Envio;
use App\Models\Factura;
use App\Models\Producto;
use App\Models\XmlTrait;
use App\Models\Numeradores;
use App\Models\ValidarPago;
use App\Models\Info_Factura;
use Illuminate\Http\Request;
use App\Models\Detalle_Factura;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Darryldecode\Cart\Facades\CartFacade;
use Illuminate\Support\Facades\Validator;
//use App\Models\XmlTrait;

class AccionesController extends Controller

{
use XmlTrait;

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('cliente', ['only' => ['info','atras','siguiente','atras2','finalizar','finalizarcompra']]);
    }

    public function info(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:50|min:3',
            'apellido' => 'required|max:50|min:3',
            'provincia'=>'required|not_in:0',
            'canton'=>'required|not_in:0',
            'parroquia'=>'required|not_in:0',
            'referencia' => 'required|max:50|min:5',
            'cedula_indentidad' => 'required|max:10|min:10',
            'direccion' => 'required|max:50|min:9',
            'telefono' => 'required|max:10|min:7',
        ]);

        //  return $request->all();

        $datos = array(
            array(
                'nombre'   => $request->nombre,
                'apellido' => $request->apellido,
                'provincia' => $request->provincia,
                'canton' => $request->canton,
                'parroquia' => $request->parroquia,
                'referencia' => $request->referencia,
                'direccion' => $request->direccion,
                'cedula_identidad' => $request->cedula_indentidad,
                'telefono' => $request->telefono,
                "factura_nombre" => $request->factura_nombre,
                "factura_apellido" => $request->factura_apellido,
                "factura_cédula"  => $request->factura_cédula,
                "factura_dirección"  => $request->factura_dirección,
                "factura_telefono" => $request->factura_telefono,
            )
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            Session::put("proceso", 2);
            Session::put("info", $datos);
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('status_success', 'Información guardada con exito ');
        }
    }

    public function atras()
    {

        Session::put("proceso", 1);
        return redirect()->back();
    }

    public function siguiente(Request $request)
    {
        Session::put("envio", $request->envio);
        Session::put("pago", $request->pago);
        Session::put("proceso", 3);
        return redirect()->back()->with('status_success', 'Información guardada con exito ');
    }

    public function atras2()
    {
        Session::put("proceso", 2);
        return redirect()->back();
    }

    //Finaloizar

    public function finalizar(Request $request)
    {


    //  var_dump('LLEGA');die;

        foreach (Session::get('info') as $test) {
        }

        $factura = new Factura();
        $factura->user_id = Auth::user()->id;
        $factura->comprobante_id=1;
        if (Session::get('pago') == 1) {
            $factura->modalidad_pago_id=1;
        } elseif (Session::get('pago') == 2) {
            $factura->modalidad_pago_id=2;
        }

        if (Session::get('envio') == 1) {
            $factura->metodo_envio_id=1;
        } elseif (Session::get('envio') == 2) {
            $factura->metodo_envio_id=2;
        }

        if (Session::get('pago') == 1) {
            $factura->estado_compra_id=1;
        } elseif (Session::get('pago') == 2) {
            $factura->estado_compra_id=2;
        }

        $factura->fecha_emision = $date =  Carbon::now()->format('Y-m-d');
        $factura->claveacceso='0';
        if (Session::get('pago')==1){

            $numeradores=Numeradores::get();
            foreach($numeradores as $numerador){

                $valor = ($numerador->correlativo);
                $long = 9;
                $secuencial = str_pad($valor, $long, '0', STR_PAD_LEFT);
                $factura->serie=$numerador->establecimiento.''.$numerador->puntoemision.''.$secuencial;
            }
        }
        if(Session::get('pago')==2){
            $factura->serie='0';

        }


        if (Session::get('envio') == 1) {
            $iva = (Session::get('TOTAL')) * 0.12;
            $factura->iva = $iva;
        } elseif (Session::get('envio') == 2) {
            $iva = (Session::get('TOTAL')) * 0.12;
            $factura->iva = $iva;
        }
        $factura->total = Session::get('TOTAL');

        if ($factura->save()) {
            foreach (CartFacade::getContent() as $item) {
                $detalle_factura = new Detalle_Factura();
                $detalle_factura->producto_id = $item->id;
                $detalle_factura->factura_id = $factura->id;
                $detalle_factura->preciou=$item->price;
                $detalle_factura->cantidad = $item->quantity;
                $detalle_factura->total = (($item->quantity) * ($item->price));
                if ($detalle_factura->save()) {
                    $productos = Producto::where('id', $item->id)->get();
                    foreach ($productos as $var) {
                        $stock = $var->stock;
                    }
                    Producto::where('id', $item->id)
                        ->update(['stock' =>  $stock - $item->quantity]);
            }
            Session::put('factura_id',$factura->id);
        }
            $info_factura = new Info_Factura();
            $info_factura->factura_id = $factura->id;
            $info_factura->nombres = $test['factura_nombre'];
            $info_factura->apellidos = $test['factura_apellido'];
            $info_factura->ruc_cedula = $test['factura_cédula'];
            $info_factura->direccion = $test['factura_dirección'];
            $info_factura->telefono = $test['factura_telefono'];
            $info_factura->save();

            $envio = new Envio();
            $envio->factura_id = $factura->id;
          //  $envio->tipo_envio = Session::get('envio');
            $envio->nombres = $test['nombre'];
            $envio->apellidos = $test['apellido'];
            $envio->cedula = $test['cedula_identidad'];

            $provincias=DB::table('provincias')
            ->where('provincias.provincia_id',$test['provincia'])
            ->get();
            foreach($provincias as $provincia){
            }
         //   dd(Session::get('provincia_id'));
            $cantones=DB::table('cantones')
            ->where('cantones.canton_id',$test['canton'])
            ->where('cantones.provincia_id',Session::get('provincia_id'))
            ->get();
            foreach($cantones as $canton){
            }
            $parroquias=DB::table('parroquias')
            ->where('parroquias.id',$test['parroquia'])
            ->get();
            foreach($parroquias as $parroquia){
            }
            $envio->ciudad = $provincia->provincia;
            $envio->canton = $canton->canton;
            $envio->parroquia = $parroquia->parroquia;
            $envio->referencia = $test['referencia'];
            $envio->telefono = $test['telefono'];
            $envio->estado = 2;//  2Pendiente
            $envio->save();

            if (Session::get('pago')==1){
                $this->xmlFactura(); //crear el XML
            }
            if(Session::get('pago')==2){
                $validar_pago=new ValidarPago();
                $validar_pago->factura_id= Session::get('factura_id');
                $validar_pago->estado=2;
                $validar_pago->save();

            }

            //Clase Exampl3







        }

        CartFacade::clear();
        Session::forget("proceso");
        Session::forget("TOTAL");
        Session::forget("IVA");
        Session::forget("pago");
        Session::forget("envio");
        Session::forget("info");

        return redirect()

        ->route('finalizar.compra.deposito')
        ->with('status_success','Compra realizada con éxito ');
    }

    public function finalizarcompra(){


      return  view('page/cliente/finalizarcompra/finalizardeposito');

    }
}
