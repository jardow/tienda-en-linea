<?php

namespace App\Http\Controllers\Cliente;

use App\Http\Controllers\Controller;
use App\Models\Categoria;
use App\Models\Seccion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categorias=Categoria::where('estado','1')
        ->get();

        $seccions=Seccion::where('estado','1')
        ->get();


        $products = DB::table('productos')
        ->where('imagens.perfil', 'si')
        ->where('publicados.estado','1')
        ->select(
            'productos.codigo_producto',
            'productos.id',
            'relaccion__imagens.relaccion_id',
            'productos.stock',
            'productos.estado',
            'productos.precio',
            'imagens.url as imagen',
            'articulos.descripcion as articulo',
            'categorias.descripcion as categoria',
            'productos.created_at',
            'imagens.perfil',
            'imagens.url'
        )
        ->join('publicados', 'productos.id', '=', 'publicados.producto_id')
        ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
        ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
        ->join('categorias', 'relaccions.categoria_id', '=', 'categorias.id')
        ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
        ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
      //; ->cursorPaginate(12);
    //  ->simplePaginate(12);
        ->paginate(12);

        return view('page/cliente/catalogo/catalogo')->with(['products' => $products,'categorias' =>$categorias, 'seccions' => $seccions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
