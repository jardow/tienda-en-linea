<?php

namespace App\Http\Controllers\Cliente;

use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class CartController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('cliente', ['only' => ['cart', 'add', 'remove', 'update', 'clear']]);
    }


    public function cart()
    {
        $cartCollection = \Cart::getContent();
        // dd($cartCollection);
        return view('page/cliente/carrito/cart')->with(['cartCollection' => $cartCollection]);
    }

    public function add(Request $request)
    {
        \Cart::add(array(
            'id' => $request->id,
            'name' => $request->name,
            'price' => $request->price ? $request->price : 20.20,
            'quantity' => $request->quantity ? $request->quantity : 1,
            'attributes' => array(
                'image' => $request->img,
                'stock' => $request->stock
            )

        ));

        return redirect()->route('cart.index')->with('success_msg', "$request->name !se ha agregado con éxito al carrito!");



        //  return redirect()->route('cart.index')->with('success_msg', 'Item actualizado !');
    }


    public function remove(Request $request)
    {
        \Cart::remove($request->id);
        return redirect()->route('cart.index')->with('success_msg', "$request->name !se ha eliminado con éxito del carrito!");
    }

    public function update(Request $request)
    {
        \Cart::update(
            $request->id,
            array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->quantity
                ),
            )
        );
        return redirect()->route('cart.index')->with('success_msg', "$request->name !se ha actualizado con éxito el carrito!");
    }

    public function clear()
    {
        \Cart::clear();
        return redirect()->route('cart.index')->with('success_msg', 'Su carrito de compras está vacio');
    }
}
