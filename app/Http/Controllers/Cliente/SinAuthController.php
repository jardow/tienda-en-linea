<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use App\Mail\ContactanosMailable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SinAuthController extends Controller
{

    public function acercaDe(){

        return view('page/cliente/acercade/acercade');
    }

    public function contactanos()
    {
        return view('page/cliente/contactanos/contacto');
    }

    public function enviarContacto(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:50|min:3',
            'correo' => 'required|email|max:50|min:6',
            'telefono' => 'required|max:10|min:9',
            'mensaje' => 'required|max:300|min:25',

        ]);

        Session::put("nombre", $request->nombre);
        Session::put("correo", $request->correo);
        Session::put("telefono", $request->telefono);
        Session::put("mensaje", $request->mensaje);



        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

        $correo = new ContactanosMailable;

        Mail::to('facturacion.store.valentina@gmail.com')->send($correo);
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('status_success', 'Información enviado con exito ', compact('request'));
        }



    }
}
