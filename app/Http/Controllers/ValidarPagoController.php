<?php

namespace App\Http\Controllers;

use App\Models\ValidarPago;
use Illuminate\Http\Request;

class ValidarPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ValidarPago  $validarPago
     * @return \Illuminate\Http\Response
     */
    public function show(ValidarPago $validarPago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ValidarPago  $validarPago
     * @return \Illuminate\Http\Response
     */
    public function edit(ValidarPago $validarPago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ValidarPago  $validarPago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ValidarPago $validarPago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ValidarPago  $validarPago
     * @return \Illuminate\Http\Response
     */
    public function destroy(ValidarPago $validarPago)
    {
        //
    }
}
