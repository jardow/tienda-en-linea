<?php

namespace App\Http\Controllers;

use App\Models\Comprobantes;
use Illuminate\Http\Request;

class ComprobantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comprobantes  $comprobantes
     * @return \Illuminate\Http\Response
     */
    public function show(Comprobantes $comprobantes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comprobantes  $comprobantes
     * @return \Illuminate\Http\Response
     */
    public function edit(Comprobantes $comprobantes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comprobantes  $comprobantes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comprobantes $comprobantes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comprobantes  $comprobantes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comprobantes $comprobantes)
    {
        //
    }
}
