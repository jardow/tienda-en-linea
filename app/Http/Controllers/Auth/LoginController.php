<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\User;   //libreria
use Illuminate\Support\Facades\Auth;  //libreria
use Laravel\Socialite\Facades\Socialite;  //libreria

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    // Google login
public function redirectToGoogle()
{
    return Socialite::driver('google')->redirect();
}

// Google callback
public function handleGoogleCallback()
{
    $user = Socialite::driver('google')->user();

    $this->_registerOrLoginUser($user);

    // Return home after login
    return redirect()->route('home');
}

// Facebook login
public function redirectToFacebook()
{
    return Socialite::driver('facebook')->redirect();
}

// Facebook callback
public function handleFacebookCallback()
{
    $user = Socialite::driver('facebook')->user();

    $this->_registerOrLoginUser($user);

    // Return home after login
    return redirect()->route('home');
}

protected function _registerOrLoginUser($data)
{
    $user = User::where('email', '=', $data->email)->first();
    if (!$user) {
        $user = new User();
        $user->name = $data->name;
        $user->surname ='null';
        $user->idcard ='null';
        $user->phone ='null';
        $user->city ='null';
        $user->avatar = $data->avatar;
        $user->email = $data->email;
        $user->save();

        $user->roles()->sync([3]);
    }

    Auth::login($user);
}

protected function credentials(\Illuminate\Http\Request $request)
{
    return ['email' => $request->{$this->username()}, 'password' => $request->password, 'state' => 1];
}

protected function sendFailedLoginResponse(\Illuminate\Http\Request $request)
    {

        if ( !User::where('email', $request->email)->first() ) {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => 'El correo no es correcto',
                ]);
        }


        if ( !User::where('email', $request->email)->where('password', bcrypt($request->password))->first() ) {


            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    'password' => 'Error de autentificación',
                ]);
        }
/*

        if ( !User::where('email', $request->email)->where('password', bcrypt($request->password))->where('state', 1)->first() ) {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    'password' => 'Usuario desactivado',
                ]);
        }

*/




}
}
