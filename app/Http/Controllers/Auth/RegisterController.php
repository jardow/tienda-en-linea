<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage; //libreria
use App\Models\Role;
use Image; //libreria
use Illuminate\Support\Facades\Auth;// liberia
use Illuminate\Support\Facades\DB; //libre
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Gate; //libre


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {


    }


  /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255', 'min:3'],
            'surname' => ['required', 'string', 'max:255', 'min:3'],
            'idcard' => ['required', 'string', 'max:255','min:10','unique:users'],
            'phone' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'avatar'=> 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:1', 'confirmed'],
        ]);

    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
$request = app('request');
    $url= '\images\perfil\\' . $data['avatar'];
         $user=User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'idcard' => $data['idcard'],
            'phone' => $data['phone'],
            'city' => $data['city'],
            'avatar' =>$url,
            'email' => $data['email'],
            'state' => '1',
            'password' => Hash::make($data['password']),
        ]);
       $user->roles()->sync([3]);
       return $user;
    }



    public function actualizar(){
   // Gate::authorize('haveaccess','no');
    $roles=auth::user()->roles;
    foreach($roles as $role){
        if ($role['slug']=='admin') {
            return redirect()->route('home');//Si es Administrador redirige al Home
        } elseif ($role['slug']=='repartidor') {
            return redirect()->route('home');//Si es Administrador redirige al Home
        }elseif ($role['slug']=='logistica') {
            return redirect()->route('home');//Si es Administrador redirige al Home
        }
    }
      $users=auth::user();
          $email=$users['email'];
        $users = DB::table('users')->where('users.email',$email)
        ->join('role_user','users.id', '=', 'role_user.user_id')
        ->join('roles','role_user.role_id', '=', 'roles.id')
        ->get();
        return $users;
    }
}
