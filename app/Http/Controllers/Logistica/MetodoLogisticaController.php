<?php

namespace App\Http\Controllers\Logistica;

use App\Models\Metodo_Envio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\DetalleEnvio;
use App\Models\Envio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class MetodoLogisticaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('logistica',['only'=>['pedidoPendiente', 'pedidoEntregado', 'detalleFactura']]);

    }

    public function pedidoPendiente(){

        $detalle_envios=DetalleEnvio::
        where('estado',2)
        ->get();
        return view('page/logistica/pedidopendiente/index', compact('detalle_envios'));
    }

    public function pedidoEntregado(){

        $detalle_envios=DB::table('detalle_envios')
        ->where('detalle_envios.estado',1)
        ->get();
        return view('page/logistica/pedidoentregado/index', compact('detalle_envios'));
    }

    public function detalleFactura($id){

        $factura_id =  Crypt::decrypt($id);

        $facturas=DB::table('facturas')
        ->where('facturas.id',$factura_id)
       ->join('users','facturas.user_id','=','users.id')
       ->join('rutas','facturas.id','=','rutas.factura_id')
     ->get();
     foreach($facturas as $factura){
          $metodo_envio=$factura->metodo_envio_id;
     }

     $metodos_envios=Metodo_Envio::where('id', $metodo_envio)->get();
     $detalle_facturas=DB::table('facturas')
     ->where('facturas.id', $factura_id)
     ->join('detalle_facturas', 'facturas.id' ,'=','detalle_facturas.factura_id')
     ->join('productos', 'detalle_facturas.producto_id','=','productos.id')
     ->join('relaccions','productos.relaccion_id','=','relaccions.id')
     ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
     ->get();
        return view('page/logistica/detallefactura/detalleFactura', compact('facturas', 'detalle_facturas','metodos_envios'));
    }
    public function asignarPedido(Request $request){
        $validator = Validator::make($request->all(), [
            'mensajero' => 'required|integer|not_in:0'
        ]);

      //  return $request->all();
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            DB::table('envios')
            ->where('id', $request->envio_id)
            ->update(['logistica_id' => Auth::user()->id ]);
            $detalle_envios=new DetalleEnvio();
            $detalle_envios->envio_id=$request->envio_id;
            $detalle_envios->user_id=Auth::user()->id;
            $detalle_envios->repartidor_id=$request->mensajero;
            $detalle_envios->estado=2;
            $detalle_envios->save();
            //1. Entregado
            //2. Proceso de Entrega
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('status_success', 'Pedido Asignado con Exito');
        }
    }

    public function guia($id)
    {
        $envio_id =  Crypt::decrypt($id);
        $guia = DetalleEnvio::where('envio_id', $envio_id)->get();
        return view('page/logistica/pedidoentregado/guia', compact('guia'));
    }
}
