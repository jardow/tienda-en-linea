<?php

namespace App\Http\Controllers\Logistica;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EntregarPedidoLogisticaController extends Controller
{


    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('logistica',['only'=>['index', 'create', 'store', 'show', 'update', 'destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repartidores=DB::table('users')
        ->where('users.state', 1)
        ->where('roles.rol', 'Repartidor')
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles','role_user.role_id','=','roles.id')
        ->select('users.id', 'users.surname')
        ->get();



        $envios=DB::table('envios')
        ->where('logistica_id', null)
        ->where('facturas.estado_compra_id', 1)
        ->where('rutas.estado','AUTORIZADO')
        ->join('facturas','envios.factura_id','=','facturas.id')
        ->join('rutas', 'facturas.id', '=', 'rutas.factura_id')
        ->select('facturas.id','facturas.metodo_envio_id', 'envios.nombres', 'envios.apellidos',
        'envios.ciudad', 'envios.canton','envios.parroquia','envios.telefono')
        ->get();

     // return $envios;

        return view('page/logistica/entregarpedido/index',compact('repartidores','envios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
