<?php

namespace App\Http\Controllers;

use App\Models\Numeradores;
use Illuminate\Http\Request;

class NumeradoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Numeradores  $numeradores
     * @return \Illuminate\Http\Response
     */
    public function show(Numeradores $numeradores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Numeradores  $numeradores
     * @return \Illuminate\Http\Response
     */
    public function edit(Numeradores $numeradores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Numeradores  $numeradores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Numeradores $numeradores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Numeradores  $numeradores
     * @return \Illuminate\Http\Response
     */
    public function destroy(Numeradores $numeradores)
    {
        //
    }
}
