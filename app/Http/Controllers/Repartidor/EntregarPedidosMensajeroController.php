<?php

namespace App\Http\Controllers\Repartidor;

use App\Models\Envio;
use Image; //libreria
use Illuminate\Support\Str;
use App\Models\DetalleEnvio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EntregarPedidosMensajeroController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('repartidor', ['only' => ['index', 'create', 'store', 'show', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detalle_envios = DB::table('detalle_envios')
        ->where('detalle_envios.estado', 1)
        ->where('detalle_envios.repartidor_id', Auth::user()->id )
        ->select('detalle_envios.id','envios.nombres','envios.apellidos','envios.telefono'
        ,'envios.ciudad','envios.canton','envios.parroquia','detalle_envios.created_at',
        'detalle_envios.guia','detalle_envios.estado','detalle_envios.envio_id')
        ->join('envios','detalle_envios.envio_id','=','envios.factura_id')
        ->get();

       // return $detalle_envios;

        return view('page/mensajero/pedidosentregados/index', compact('detalle_envios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'guia'  => 'required|file|max:1024|mimes:jpeg,bmp,png,jpg',
        ]);

        // return $request->all();


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {


            $url=null;
            if($request->hasfile('guia')){
                $avatar = $request->file('guia');
                $filename = Str::random(12) . '.' . $avatar->getClientOriginalExtension();
                   Image::make($avatar)->resize(800, 800)->save( public_path('images\guia\\' . $filename) );
                   $url= '\images\guia\\' . $filename;
                }



            DB::table('detalle_envios')
                ->where('id', $id)
                ->update(
                    ['guia' => $url, 'estado' => 1],

                );


            //1. Entregado
            //2. Proceso de Entrega
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('status_success', 'Pedido Entregado  con Exito');
        }




        return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
