<?php

namespace App\Http\Controllers\Repartidor;

use App\Models\DetalleEnvio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class MetodoRepartidorController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('repartidor',['only'=>['pedidoPendiente', 'guia']]);

    }

    public function pedidoPendiente(){

        $detalle_envios=DB::table('detalle_envios')
        ->where('detalle_envios.estado',2)
        ->where('detalle_envios.repartidor_id', Auth::user()->id )
        ->join('facturas','detalle_envios.envio_id','=','facturas.id')
        ->join('envios','facturas.id','=','envios.factura_id')
        ->select('detalle_envios.id', 'envios.nombres', 'envios.apellidos',
        'envios.ciudad', 'envios.canton', 'envios.parroquia', 'envios.referencia', 'envios.telefono','envios.estado')
        ->get();

       // return $detalle_envios;


        return view('page/mensajero/pedidospendientes/index', compact('detalle_envios'));
    }

    public function guia($id)
    {
        $envio_id =  Crypt::decrypt($id);
        $guia = DetalleEnvio::where('envio_id', $envio_id)->get();
        return view('page/mensajero/pedidosentregados/guia', compact('guia'));
    }
}
