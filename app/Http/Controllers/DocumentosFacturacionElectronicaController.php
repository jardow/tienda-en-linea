<?php

namespace App\Http\Controllers;

use App\Models\DocumentosFacturacionElectronica;
use Illuminate\Http\Request;

class DocumentosFacturacionElectronicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DocumentosFacturacionElectronica  $documentosFacturacionElectronica
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentosFacturacionElectronica $documentosFacturacionElectronica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DocumentosFacturacionElectronica  $documentosFacturacionElectronica
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentosFacturacionElectronica $documentosFacturacionElectronica)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DocumentosFacturacionElectronica  $documentosFacturacionElectronica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentosFacturacionElectronica $documentosFacturacionElectronica)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DocumentosFacturacionElectronica  $documentosFacturacionElectronica
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocumentosFacturacionElectronica $documentosFacturacionElectronica)
    {
        //
    }
}
