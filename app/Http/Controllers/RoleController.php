<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Cantone;
use App\Models\Factura;
use App\Models\Producto;
use App\Models\Parroquia;
use App\Models\Provincia;
use App\Models\Permission;
use App\Models\Numeradores;
use App\Models\ValidarPago;
use Illuminate\Support\Str;
use App\Models\Metodo_Envio;
use Illuminate\Http\Request;
use App\Models\Detalle_Factura;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use App\Models\DatosFacturacionElectronica;
use Illuminate\Support\Facades\Gate; //libre



class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function validar_clave($clave)
    {



        if ($clave == "") {
            $verificado = false;
            return $verificado;
        }

        $x = 2;
        $sumatoria = 0;
        for ($i = strlen($clave) - 1; $i >= 0; $i--) {
            if ($x > 7) {
                $x = 2;
            }
            $sumatoria = $sumatoria + ($clave[$i] * $x);
            $x++;
        }
        $digito = $sumatoria % 11;
        $digito = 11 - $digito;

        switch ($digito) {
            case 10:
                $digito = "1";
                break;
            case 11:
                $digito = "0";
                break;
        }

        /*
      if (strtolower($digito_v)==$digito){
      $verificado=true;
      } else {
      $verificado=false;
      }

     */

        return $digito;
    }



    public function index()
    {
/*
        select *from parroquias
where provincia_id =17
AND canton_id=5;

*/
        $parroquias=DB::table('cantones')
        ->where('cantones.canton_id',2)
        ->where('cantones.provincia_id',17)
        ->get();

        return $parroquias;




        /*

      //  Gate::authorize('haveaccess','role.index');
        $roles =  Role::orderBy('id','Asc')->paginate(2);
         // $roles = Role::get();
        return view('role.index',compact('roles'));
*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('haveaccess', 'role.create');
        $permissions = Permission::get();

        return view('role.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('haveaccess', 'role.create');
        $request->validate([
            'name'          => 'required|max:50|unique:roles,name',
            'slug'          => 'required|max:50|unique:roles,slug',
            'full-access'   => 'required|in:yes,no'
        ]);

        $role = Role::create($request->all());

        //    if ($request->get('permission')){
        $role->permissions()->sync($request->get('permission'));
        //}
        return redirect()->route('role.index')
            ->with('status_success', 'Role saved successfully ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        Gate::authorize('haveaccess', 'role.show');
        $permission_role = [];

        foreach ($role->permissions as $permission) {
            $permission_role[] = $permission->id;
        }
        //return   $permission_role;


        //return $role;
        $permissions = Permission::get();




        return view('role.view', compact('permissions', 'role', 'permission_role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        Gate::authorize('haveaccess', 'role.edit');
        $permission_role = [];
        foreach ($role->permissions as $permission) {

            $permission_role[] = $permission->id;
        }
        //return $permission_role;
        $permissions = Permission::get();
        return view('role.edit', compact('permissions', 'role', 'permission_role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        Gate::authorize('haveaccess', 'role.edit');
        $request->validate([
            'name'          => 'required|max:50|unique:roles,name,' . $role->id,
            'slug'          => 'required|max:50|unique:roles,slug,' . $role->id,
            'full-access'   => 'required|in:yes,no'
        ]);

        $role->update($request->all());

        //   if ($request->get('permission')) {
        //return $request->all();
        $role->permissions()->sync($request->get('permission'));
        //  }
        return redirect()->route('role.index')
            ->with('status_success', 'Role updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        Gate::authorize('haveaccess', 'role.delete');
        $role->delete();

        return redirect()->route('role.index')
            ->with('status_success', 'Role successfully removed');
    }
}
