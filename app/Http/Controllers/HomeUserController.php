<?php

namespace App\Http\Controllers;

use Closure;
use App\Models\Seccion;
use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;// liberia

class HomeUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        $this->middleware('cliente',['only'=>['getUser','catalogo']]);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function getUser(){

        $products=DB::table('productos')
        ->where('imagens.perfil','si')
        ->where('ofertas.estado','1')
        ->select(
            'productos.codigo_producto',
            'productos.id',
            'relaccion__imagens.relaccion_id',
            'productos.stock',
            'productos.estado',
            'ofertas.precio_oferta',
            'productos.descripcion',
            'imagens.url as imagen',
            'articulos.descripcion as articulo',
            'categorias.descripcion as categoria',
            'productos.created_at',
            'imagens.perfil',
            'imagens.url'
        )
        ->join('ofertas', 'productos.id', '=', 'ofertas.producto_id')
        ->join('publicados', 'productos.id', '=', 'publicados.producto_id')
        ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
        ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
        ->join('categorias', 'relaccions.categoria_id', '=', 'categorias.id')
        ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
        ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
        ->get();



        return view('welcome')->with(['products' =>$products]);

    }

    function catalogo(Request $request){



        $categorias=Categoria::where('estado','1')
        ->get();

        $seccions=Seccion::where('estado','1')
        ->get();


        $products = DB::table('productos')
        ->where('imagens.perfil', 'si')
       ->where('categorias.descripcion', $request->categoria)
       ->where('seccions.descripcion', $request->seccion)
        ->where('publicados.estado','1')
        ->select(
            'productos.codigo_producto',
            'productos.id',
            'relaccion__imagens.relaccion_id',
            'productos.stock',
            'productos.estado',
            'productos.precio',
            'imagens.url as imagen',
            'articulos.descripcion as articulo',
            'categorias.descripcion as categoria',
            'productos.created_at',
            'imagens.perfil',
            'imagens.url'
        )
        ->join('publicados', 'productos.id', '=', 'publicados.producto_id')
        ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
        ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
        ->join('categorias', 'relaccions.categoria_id', '=', 'categorias.id')
        ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
        ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
      //; ->cursorPaginate(12);
    //  ->simplePaginate(12);
        ->paginate(12);


        return view('page/cliente/catalogo/catalogo')->with(['products' => $products,'categorias' =>$categorias, 'seccions' => $seccions]);

}

}
