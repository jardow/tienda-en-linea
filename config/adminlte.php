<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For detailed instructions you can look the title section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/6.-Basic-Configuration
    |
    */

    'title' => 'AdminLTE 3',
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For detailed instructions you can look the favicon section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/6.-Basic-Configuration
    |
    */

    'use_ico_only' => false,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For detailed instructions you can look the logo section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/6.-Basic-Configuration
    |
    */



    'logo' => 'STORE VALENTINA',
    'logo_img' => 'https://c0.klipartz.com/pngpicture/564/115/sticker-png-logo-brand-font-online-shop-text-rectangle-logo-microsoft-azure.png',
    'logo_img_class' => 'brand-image img-circle elevation-3',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'MiniShop',

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For detailed instructions you can look the user menu section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/6.-Basic-Configuration
    |
    */

    'usermenu_enabled' => true,
    'usermenu_header' => true,
    'usermenu_header_class' => 'bg-info',
    'usermenu_image' => true,
    'usermenu_desc' => true,
    'usermenu_profile_url' => true,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For detailed instructions you can look the layout section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/7.-Layout-and-Styling-Configuration
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => true, //Sidebar fijo valor de True
    'layout_fixed_navbar' => true, //menu fijo
    'layout_fixed_footer' => null, // pie de pagina fijo

    /*
    |--------------------------------------------------------------------------
    | Authentication Views Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the authentication views.
    |
    | For detailed instructions you can look the auth classes section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/7.-Layout-and-Styling-Configuration
    |
    */
    /*

    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',

    */

    'classes_auth_card' =>'',
    'classes_auth_header'=>'bg_gradient-info',
    'classes_auth_body' =>'',
    'classes_auth_footer' =>'text-center',
    'classes_auth_icon' =>'fa-lg text-info',
    'classes_auth_btn' => 'btn-flat btn-primary',

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For detailed instructions you can look the admin panel classes here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/7.-Layout-and-Styling-Configuration
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => '',
    'classes_content' => '',
    'classes_sidebar' => 'sidebar-light-success elevation-4',  //para cambiar aparicnecia light / para cambiar boton activo success
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-info navbar-light', // cambiar color de Menú
    'classes_topnav_nav' => 'navbar-expand',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For detailed instructions you can look the sidebar section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/7.-Layout-and-Styling-Configuration
    |
    */

    'sidebar_mini' => false,  //para ocultar mini menu sidebar izquirda
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For detailed instructions you can look the right sidebar section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/7.-Layout-and-Styling-Configuration
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'light',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For detailed instructions you can look the urls section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/6.-Basic-Configuration
    |
    */

    'use_route_url' => false,
    'dashboard_url' => 'administrador/panel',
    'logout_url' => 'logout',
    'login_url' => 'login',
    'register_url' => 'register',
    'password_reset_url' => 'password/reset',
    'password_email_url' => 'password/email',
    'profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For detailed instructions you can look the laravel mix section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/9.-Other-Configuration
    |
    */

    'enabled_laravel_mix' => false,
    'laravel_mix_css_path' => 'css/app.css',
    'laravel_mix_js_path' => 'js/app.js',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/8.-Menu-Configuration
    |
    */

    //https://fontawesome.com/icons  iconos

    'menu' => [
        /*

        [
            'text' => 'search',
            'search' => true,
            'topnav' => true,
        ],
*/
/*
['header' => 'Facturación'],
/*
        [
            'text' => 'blog',
            'color' => 'red',
            'url'  => 'admin/blog',
            'can'  => 'administrador',


        ],

        [
            'text'        => 'Notificaciones',
            'url'         => 'admin/pages',
            'icon'        => 'far fa-fw fa-file',
            'label'       => 4,
            'label_color' => 'success',
            'can'  => 'administrador',
        ],
  */
        //
['header' => 'ENTREGAS',
'can'  => 'mensajero',
],

[
    'text' => 'Proceso Entrega',
    'url'  => 'repartidor/pedidos/pendientes',
    'icon' => 'fas fa-motorcycle',
    'can'  => 'mensajero',
],
[
    'text' => 'Reporte Entrega',
    'url'  => 'repartidor/pedidos/entregados',
    'icon' => 'fas fa-paste',
    'can'  => 'mensajero',
],
//


        //
        ['header' => 'LOGÍSTICA',
        'can'  => 'logistica',
        ],

        [
            'text' => 'Entregar Pedido',
            'url'  => 'logistica/entregar/pedido',
            'icon' => 'fas fa-fw fa-user',
            'can'  => 'logistica',
        ],
        [
            'text' => 'Proceso Entrega',
            'url'  => 'logistica/pedido/pendiente',
            'icon' => 'fas fa-motorcycle',
            'can'  => 'logistica',
        ],
        [
            'text' => 'Reporte Entrega',
            'url'  => 'logistica/pedido/entregado',
            'icon' => 'fas fa-paste',
            'can'  => 'logistica',
        ],


        //
        /*

        ['header' => 'account_settings'],
        [
            'text' => 'profile',
            'url'  => '/administrador/perfil',
            'icon' => 'fas fa-fw fa-user',
        ],

        */


///
[
    'text'    => 'USUARIOS',
    'icon'    => 'fas fa-users',
    'can'  => 'administrador',
    'submenu' => [



        [
            'text' => 'Clientes',
            'icon' => 'fas fa-users-cog',
            'url'  => '/administrador/modulo_usuario/reporte_cliente',
        ],


        [
            'text'    => 'Usuarios',
            'icon' => 'fas fa-id-badge',
            'url'     => '/administrador/modulo_usuario/reporte_usuario',

        ],

        [
            'text'    => 'Crear Usuario',
            'icon' => 'fas fa-user-plus',
            'url'     => '/administrador/modulo_usuario/crear',

        ],
    ],
],

[
    'text'    => 'FACTURACIÓN',
    'icon'    => 'fas fa-hand-holding-usd',
    'can'  => 'administrador',
    'submenu' => [

        [
            'text'    => 'Autorizado',
            'icon'    => 'fas fa-file-alt',
            'url'     => 'administrador/facturacion/factura/autorizado',

        ],

        [
            'text' => 'Rechazado',
            'icon'    => 'fas fa-file-excel',
            'url'  => 'administrador/facturacion/factura/rechazado',
        ],


        [
            'text'    => 'EMITIR FACTURA',
            'icon'    => 'fas fa-file-invoice-dollar',
            'url'     => '#',
            'submenu' => [

                [
                    'text'    => 'Validar Pago',
                    'icon'    => 'fas fa-money-bill-alt',
                    'url'     => '/administrador/facturacion/validar/pago',

                ],
            ],
        ],
    ],
],

[
    'text'    => 'REPORTES',
    'icon'    => 'fas fa-paste',
    'can'  => 'administrador',
    'submenu' => [

        [
            'text'    => 'Entregas',
            'icon'    => 'fas fa-motorcycle',
            'url'     => '/administrador/reportes/entregas',

        ],

        [
            'text' => 'Top Vendidos',
            'icon'    => 'fas fa-cart-plus',
            'url'  => '/administrador/reportes/productos/vendidos',
        ],


        [
            'text'    => 'ublicados',
            'icon'    => 'fab fa-product-hunt',
            'url'     => '/administrador/reportes/productos/publicados',

        ],
    ],
],

        ///

        [
            'text'    => 'PRODUCTOS',
            'icon'    => 'fab fa-sellsy',
            'can'  => 'administrador',
            'submenu' => [

                [
                    'text' => 'Crear Producto',
                    'icon'    => 'fas fa-cart-plus',
                    'url'  => '/administrador/modulo_producto/producto',
                ],


                [
                    'text'    => 'PUBLICADO',
                    'icon'    => 'fas fa-upload',
                    'url'     => '#',
                    'submenu' => [
                        [
                            'text' => 'Publicado',
                            'icon'    => 'fas fa-globe-americas',
                            'url'  => '/administrador/modulo_producto/publicado/publicado',
                            'bg'=>'success',
                        ],
                        [
                            'text' => 'Oferta',
                            'icon'    => 'fas fa-percentage',
                            'url'  => '/administrador/modulo_producto/publicado/oferta',
                        ],
                    ],
                ],


                [
                    'text'    => 'GESTIÓN PRODUCTO',
                    'icon'    => 'fas fa-edit',
                    'url'     => '#',
                    'submenu' => [

                        /*

                        [
                            'text' => 'Articulo',
                            'icon'    => 'fas fa-palette',
                            'url'  => '/administrador/modulo_producto/gestion/articulo',
                        ],
                        */

                        [
                            'text' => 'Color',
                            'icon'    => 'fas fa-palette',
                            'url'  => '/administrador/modulo_producto/gestion/color',
                        ],

                        [
                            'text' => 'ategoría',
                            'icon'    => 'fas fa-copyright',
                            'url'  => '/administrador/modulo_producto/gestion/categoria',
                        ],

                        [
                            'text' => 'ección',
                            'icon'    => 'fas fa-strikethrough',
                            'url'  => '/administrador/modulo_producto/gestion/seccion',
                        ],

                        [
                            'text' => 'Talla',
                            'icon'    => 'fas fa-pencil-ruler',
                            'url'  => '/administrador/modulo_producto/gestion/talla',
                        ],

                        [
                            'text' => 'Origen',
                            'icon'    => 'fas fa-flag',
                            'url'  => '/administrador/modulo_producto/gestion/origen',
                        ],



                    ],
                ],


            ],
        ],

        /*
      //  ['header' => 'labels'],
     // ['header' => 'ASfc'],
      'can'  => 'isAdmin',
        [
            'text'       => 'important',
            'icon_color' => 'red',
            'url'        => '#',
        ],
        [
            'text'       => 'warning',
            'icon_color' => 'yellow',
            'url'        => '#',
        ],
        [
            'text'       => 'information',
            'icon_color' => 'cyan',
            'url'        => '#',
        ],
        */
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For detailed instructions you can look the menu filters section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/8.-Menu-Configuration
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For detailed instructions you can look the plugins section here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/9.-Other-Configuration
    |
    */

    'plugins' => [
        'Datatables' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        'Select2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        'Chartjs' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        'Sweetalert2' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        'Pace' => [
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Livewire
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Livewire support.
    |
    | For detailed instructions you can look the livewire here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/wiki/9.-Other-Configuration
    */

    'livewire' => false,
];
