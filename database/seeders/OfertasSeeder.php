<?php

namespace Database\Seeders;

use App\Models\Oferta;
use Illuminate\Database\Seeder;

class OfertasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Oferta::create([
            'producto_id'=>'6',
            'fecha_publicado' => '2021-10-05',
            'descuento' => '20',   // 'estado' => 'deshabilitado',1,
            'precio_oferta' =>'4.00',
            'estado' => '1'
        ]);
        Oferta::create([
            'producto_id'=>'7',
            'fecha_publicado' => '2021-10-05',
            'descuento' => '20',   // 'estado' => 'deshabilitado',1,
            'precio_oferta' =>'24.00',
            'estado' => '1'
        ]);
        Oferta::create([
            'producto_id'=>'8',
            'fecha_publicado' => '2021-10-05',
            'descuento' => '20',   // 'estado' => 'deshabilitado',1,
            'precio_oferta' =>'8.00',
            'estado' => '1'
        ]);
        Oferta::create([
            'producto_id'=>'9',
            'fecha_publicado' => '2021-10-05',
            'descuento' => '20',   // 'estado' => 'deshabilitado',1,
            'precio_oferta' =>'3.20',
            'estado' => '1'
        ]);
        Oferta::create([
            'producto_id'=>'10',
            'fecha_publicado' => '2021-10-05',
            'descuento' => '20',   // 'estado' => 'deshabilitado',1,
            'precio_oferta' =>'16.00',
            'estado' => '1'
        ]);
    }
}
