<?php

namespace Database\Seeders;

use App\Models\Relaccion_Imagen;
use Illuminate\Database\Seeder;

class Relaccion_ImagenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Relaccion_Imagen::create([
            'imagen_id' => '1',
            'relaccion_id' => '1',   // 'estado' => 'deshabilitado', 1
        ]);

        Relaccion_Imagen::create([
            'imagen_id' => '2',
            'relaccion_id' => '1',   // 'estado' => 'deshabilitado', 2
        ]);

        Relaccion_Imagen::create([
            'imagen_id' => '3',
            'relaccion_id' => '2',   // 'estado' => 'deshabilitado', 3
        ]);

        Relaccion_Imagen::create([
            'imagen_id' => '4',
            'relaccion_id' => '2',   // 'estado' => 'deshabilitado', 4
        ]);

        Relaccion_Imagen::create([
            'imagen_id' => '5',
            'relaccion_id' => '3',   // 'estado' => 'deshabilitado', 5
        ]);

        Relaccion_Imagen::create([
            'imagen_id' => '6',
            'relaccion_id' => '4',   // 'estado' => 'deshabilitado', 6
        ]);

        Relaccion_Imagen::create([
            'imagen_id' => '7',
            'relaccion_id' => '4',   // 'estado' => 'deshabilitado', 7
        ]);


        Relaccion_Imagen::create([
            'imagen_id' => '8',
            'relaccion_id' => '4',   // 'estado' => 'deshabilitado', 8
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '9',
            'relaccion_id' => '5',   // 'estado' => 'deshabilitado', 9
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '10',
            'relaccion_id' => '5',   // 'estado' => 'deshabilitado', 10
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '11',
            'relaccion_id' => '5',   // 'estado' => 'deshabilitado', 11
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '12',
            'relaccion_id' => '6',   // 'estado' => 'deshabilitado', 12
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '13',
            'relaccion_id' => '6',   // 'estado' => 'deshabilitado', 13
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '14',
            'relaccion_id' => '6',   // 'estado' => 'deshabilitado', 14
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '15',
            'relaccion_id' => '7',   // 'estado' => 'deshabilitado', 15
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '16',
            'relaccion_id' => '7',   // 'estado' => 'deshabilitado', 16
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '17',
            'relaccion_id' => '8',   // 'estado' => 'deshabilitado', 17
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '18',
            'relaccion_id' => '8',   // 'estado' => 'deshabilitado', 18
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '19',
            'relaccion_id' => '8',   // 'estado' => 'deshabilitado', 19
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '20',
            'relaccion_id' => '9',   // 'estado' => 'deshabilitado', 20
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '21',
            'relaccion_id' => '9',   // 'estado' => 'deshabilitado', 21
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '22',
            'relaccion_id' => '9',   // 'estado' => 'deshabilitado', 22
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '23',
            'relaccion_id' => '10',   // 'estado' => 'deshabilitado', 23
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '24',
            'relaccion_id' => '10',   // 'estado' => 'deshabilitado', 24
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '25',
            'relaccion_id' => '10',   // 'estado' => 'deshabilitado', 25
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '26',
            'relaccion_id' => '11',   // 'estado' => 'deshabilitado', 26
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '27',
            'relaccion_id' => '11',   // 'estado' => 'deshabilitado', 27
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '28',
            'relaccion_id' => '12',   // 'estado' => 'deshabilitado', 28
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '29',
            'relaccion_id' => '12',   // 'estado' => 'deshabilitado', 29
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '30',
            'relaccion_id' => '12',   // 'estado' => 'deshabilitado', 30
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '31',
            'relaccion_id' => '13',   // 'estado' => 'deshabilitado', 31
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '32',
            'relaccion_id' => '13',   // 'estado' => 'deshabilitado', 32
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '33',
            'relaccion_id' => '13',   // 'estado' => 'deshabilitado', 33
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '34',
            'relaccion_id' => '14',   // 'estado' => 'deshabilitado', 34
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '35',
            'relaccion_id' => '14',   // 'estado' => 'deshabilitado', 35
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '36',
            'relaccion_id' => '14',   // 'estado' => 'deshabilitado', 36
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '37',
            'relaccion_id' => '15',   // 'estado' => 'deshabilitado', 37
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '38',
            'relaccion_id' => '15',   // 'estado' => 'deshabilitado', 38
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '39',
            'relaccion_id' => '16',   // 'estado' => 'deshabilitado', 39
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '40',
            'relaccion_id' => '16',   // 'estado' => 'deshabilitado', 40
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '41',
            'relaccion_id' => '16',   // 'estado' => 'deshabilitado', 41
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '42',
            'relaccion_id' => '17',   // 'estado' => 'deshabilitado', 42
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '43',
            'relaccion_id' => '17',   // 'estado' => 'deshabilitado', 43
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '44',
            'relaccion_id' => '17',   // 'estado' => 'deshabilitado', 44
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '45',
            'relaccion_id' => '18',   // 'estado' => 'deshabilitado', 45
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '46',
            'relaccion_id' => '18',   // 'estado' => 'deshabilitado', 46
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '47',
            'relaccion_id' => '19',   // 'estado' => 'deshabilitado', 47
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '48',
            'relaccion_id' => '19',   // 'estado' => 'deshabilitado', 48
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '49',
            'relaccion_id' => '19',   // 'estado' => 'deshabilitado', 49
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '50',
            'relaccion_id' => '20',   // 'estado' => 'deshabilitado', 50
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '51',
            'relaccion_id' => '20',   // 'estado' => 'deshabilitado', 51
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '52',
            'relaccion_id' => '21',   // 'estado' => 'deshabilitado', 52
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '53',
            'relaccion_id' => '21',   // 'estado' => 'deshabilitado', 53
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '54',
            'relaccion_id' => '22',   // 'estado' => 'deshabilitado', 54
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '55',
            'relaccion_id' => '22',   // 'estado' => 'deshabilitado', 55
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '56',
            'relaccion_id' => '23',   // 'estado' => 'deshabilitado', 56
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '57',
            'relaccion_id' => '24',   // 'estado' => 'deshabilitado', 57
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '58',
            'relaccion_id' => '25',   // 'estado' => 'deshabilitado', 58
        ]);
        Relaccion_Imagen::create([
            'imagen_id' => '59',
            'relaccion_id' => '25',   // 'estado' => 'deshabilitado', 59
        ]);

    }
}
