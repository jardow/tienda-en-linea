<?php

namespace Database\Seeders;

use App\Models\Producto;
use Illuminate\Database\Seeder;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::create([
            'relaccion_id' => '1',
            'origen_id' => '2',
            'codigo_producto' => '8B1U9A3P01',
            'precio' => '25.00',
            'stock' => '50',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '2',
            'origen_id' => '2',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P02',
            'precio' => '20.00',
            'stock' => '45',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '3',
            'origen_id' => '1',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P03',
            'precio' => '50.00',
            'stock' => '40',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '4',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P04',
            'precio' => '15.00',
            'stock' => '25',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '5',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P05',
            'precio' => '20.00',
            'stock' => '25',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '6',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P06',
            'precio' => '5.00',
            'stock' => '50',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '7',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P07',
            'precio' => '30.00',
            'stock' => '30',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '8',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P08',
            'precio' => '10.00',
            'stock' => '30',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '9',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P09',
            'precio' => '4.00',
            'stock' => '50',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '10',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P10',
            'precio' => '20.00',
            'stock' => '50',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '2',
        ]);

        Producto::create([
            'relaccion_id' => '11',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P11',
            'precio' => '35.00',
            'stock' => '20',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '12',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P12',
            'precio' => '5.00',
            'stock' => '35',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '13',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P13',
            'precio' => '20.00',
            'stock' => '50',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '14',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P14',
            'precio' => '10.00',
            'stock' => '20',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '15',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P15',
            'precio' => '10.00',
            'stock' => '20',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '16',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P16',
            'precio' => '10.00',
            'stock' => '20',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '17',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P17',
            'precio' => '10.00',
            'stock' => '20',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '18',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P18',
            'precio' => '5.00',
            'stock' => '50',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '19',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P19',
            'precio' => '12.00',
            'stock' => '30',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '20',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P20',
            'precio' => '15.00',
            'stock' => '35',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);


        Producto::create([
            'relaccion_id' => '21',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P21',
            'precio' => '15.00',
            'stock' => '35',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '22',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P22',
            'precio' => '15.00',
            'stock' => '35',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '23',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P23',
            'precio' => '200.00',
            'stock' => '5',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '24',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P24',
            'precio' => '300.00',
            'stock' => '5',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);

        Producto::create([
            'relaccion_id' => '25',
            'origen_id' => '4',   // 'estado' => 'deshabilitado',  9
            'codigo_producto' => '8B1U9A3P25',
            'precio' => '10.00',
            'stock' => '35',
            'descripcion' => 'Una tienda virtual o tienda online es un sitio web diseñado especialmente para vender productos u ofrecer servicios mediante el comercio electrónico.',
            'fecha_ingreso' => '2021-09-02 14:09:06',
            'estado' => '1',
        ]);
    }
}
