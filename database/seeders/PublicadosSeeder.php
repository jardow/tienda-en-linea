<?php

namespace Database\Seeders;

use App\Models\Publicado;
use Illuminate\Database\Seeder;

class PublicadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Publicado::create([
            'producto_id'=>'1',
            'fecha_publicado' => '2021-10-05',
            'estado' => '1',   // 'estado' => 'deshabilitado',1,
        ]);
        Publicado::create([
            'producto_id'=>'2',
            'fecha_publicado' => '2021-10-05',
            'estado' => '1',   // 'estado' => 'deshabilitado',1,
        ]);
        Publicado::create([
            'producto_id'=>'3',
            'fecha_publicado' => '2021-10-05',
            'estado' => '1',   // 'estado' => 'deshabilitado',1,
        ]);
        Publicado::create([
            'producto_id'=>'4',
            'fecha_publicado' => '2021-10-05',
            'estado' => '1',   // 'estado' => 'deshabilitado',1,
        ]);
        Publicado::create([
            'producto_id'=>'5',
            'fecha_publicado' => '2021-10-05',
            'estado' => '1',   // 'estado' => 'deshabilitado',1,
        ]);

        Publicado::create([
            'producto_id'=>'6',
            'fecha_publicado' => '2021-10-05',
            'estado' => '3',   // 'estado' => 'deshabilitado',1,
        ]);
        Publicado::create([
            'producto_id'=>'7',
            'fecha_publicado' => '2021-10-05',
            'estado' => '3',   // 'estado' => 'deshabilitado',1,
        ]);
        Publicado::create([
            'producto_id'=>'8',
            'fecha_publicado' => '2021-10-05',
            'estado' => '3',   // 'estado' => 'deshabilitado',1,
        ]);
        Publicado::create([
            'producto_id'=>'9',
            'fecha_publicado' => '2021-10-05',
            'estado' => '3',   // 'estado' => 'deshabilitado',1,
        ]);
        Publicado::create([
            'producto_id'=>'10',
            'fecha_publicado' => '2021-10-05',
            'estado' => '3',   // 'estado' => 'deshabilitado',1,
        ]);
    }
}
