<?php

namespace Database\Seeders;

use App\Models\Empresa;
use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresa::create([
            'razonsocial' => 'CUENCA MACAS NELLY MARIA',
            'nombre' => 'CUENCA MACAS NELLY MARIA',   // 'estado' => 'deshabilitado',1
            'direccion' => 'PICHINCHA / QUITO / QUITUMBE / E9 S40-157 Y S41',
            'lugar' => 'Quito',
            'telefono' => '022787294',
            'celular' => '0984243670',
            'correo' => 'bacoth@gmail.com',
            'ruc' => '1104257033001',
            'contribuyente' => '0000',
            'contabilidad' => 'NO',
            'logo' => '\images\perfil\3XNPo0Ro8ykE.jpg',
        ]);
    }
}
