<?php

namespace Database\Seeders;

use App\Models\Estado_Compra;
use App\Models\Numeradores;
use App\Models\Publicado;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\User::factory(10)->create();

/*
        $this->call(PermissionInfoSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(CategoriaSeeder::class);
        $this->call(SeccionSeeder::class);
        $this->call(TallaSeeder::class);
        $this->call(ArticuloSeeder::class);
        $this->call(OrigenSeeder::class);
        $this->call(ImagenSeeder::class);
        $this->call(EmpresaSeeder::class);
        $this->call(ComprobantesSeeder::class);
        $this->call(NumeradoresSeeder::class);
        $this->call(Metodo_EnvioSeeder::class);
        $this->call(Modalidad_PagoSeeder::class);
        $this->call(EstadoCompraSeeder::class);
        $this->call(Datos_Facturacion_electronicaSeeder::class);
        $this->call(ProvinciaSeeder::class);
        $this->call(CantonSeeder::class);
        $this->call(ParroquiaSeeder::class);
*/


        $this->call(RelacionSeeder::class);
        $this->call(ProductoSeeder::class);
        $this->call(Relaccion_ImagenSeeder::class);
        $this->call(PublicadosSeeder::class);
        $this->call(OfertasSeeder::class);

    }
}
