<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::create([
            'descripcion' => 'Ropa',
            'estado' => '1',   // 'estado' => 'deshabilitado', 1

        ]);

        Categoria::create([
            'descripcion' => 'Calzado',
            'estado' => '1',   // 'estado' => 'deshabilitado', 2

        ]);

        Categoria::create([
            'descripcion' => 'Accesorios',
            'estado' => '1',   // 'estado' => 'deshabilitado', 3

        ]);
        Categoria::create([
            'descripcion' => 'Cuidado de la piel',
            'estado' => '1',   // 'estado' => 'deshabilitado', 4

        ]);

        Categoria::create([
            'descripcion' => 'Vitamina E',
            'estado' => '1',   // 'estado' => 'deshabilitado', 5

        ]);

        Categoria::create([
            'descripcion' => 'Jardinería',
            'estado' => '1',   // 'estado' => 'deshabilitado', 6

        ]);

        Categoria::create([
            'descripcion' => 'Maquillaje',
            'estado' => '1',   // 'estado' => 'deshabilitado', 7

        ]);

        Categoria::create([
            'descripcion' => 'Telefono',
            'estado' => '1',   // 'estado' => 'deshabilitado', 8

        ]);

        Categoria::create([
            'descripcion' => 'Televisor',
            'estado' => '1',   // 'estado' => 'deshabilitado', 9

        ]);
    }
}
