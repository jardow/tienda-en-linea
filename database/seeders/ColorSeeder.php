<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Color;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::create([
            'descripcion' => 'Negro',
            'estado' => '1',   // 'estado' => 'deshabilitado',  1
        ]);

        Color::create([
            'descripcion' => 'Rojo',
            'estado' => '1',   // 'estado' => 'deshabilitado',   2
        ]);

        Color::create([
            'descripcion' => 'Amarillo',
            'estado' => '1',   // 'estado' => 'deshabilitado',   3
        ]);

        Color::create([
            'descripcion' => 'Morado',
            'estado' => '1',   // 'estado' => 'deshabilitado',   4
        ]);


        Color::create([
            'descripcion' => 'Marrón',
            'estado' => '1',   // 'estado' => 'deshabilitado',   5
        ]);

        Color::create([
            'descripcion' => 'Rosado',
            'estado' => '1',   // 'estado' => 'deshabilitado',  6
        ]);


        Color::create([
            'descripcion' => 'Blanco',
            'estado' => '1',   // 'estado' => 'deshabilitado',  7
        ]);

        Color::create([
            'descripcion' => 'verde',
            'estado' => '1',   // 'estado' => 'deshabilitado',  8
        ]);

        Color::create([
            'descripcion' => 'Azul',
            'estado' => '1',   // 'estado' => 'deshabilitado',  9
        ]);


        Color::create([
            'descripcion' => 'No definido',
            'estado' => '1',   // 'estado' => 'deshabilitado',  10
        ]);




    }
}
