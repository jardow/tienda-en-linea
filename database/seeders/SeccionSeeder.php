<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Seccion;

class SeccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Seccion::create([
            'descripcion' => 'Hombre',
            'estado' => '1',   // 'estado' => 'deshabilitado',

            ]);

            Seccion::create([
                'descripcion' => 'Mujer',
                'estado' => '1',   // 'estado' => 'deshabilitado',

                ]);


                Seccion::create([
                    'descripcion' => 'Ñiños',
                    'estado' => '1',   // 'estado' => 'deshabilitado',

                    ]);

                    Seccion::create([
                        'descripcion' => 'Todo',
                        'estado' => '1',   // 'estado' => 'deshabilitado',

                        ]);
    }
}
