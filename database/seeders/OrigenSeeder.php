<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Origen;

class OrigenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Origen::create([
            'descripcion' => 'Ecuador',
            'estado' => '1',   // 'estado' => 'deshabilitado',1
        ]);

        Origen::create([
            'descripcion' => 'Colombia',
            'estado' => '1',   // 'estado' => 'deshabilitado',2
        ]);

        Origen::create([
            'descripcion' => 'Perú',
            'estado' => '1',   // 'estado' => 'deshabilitado',3
        ]);

        Origen::create([
            'descripcion' => 'No definido',
            'estado' => '1',   // 'estado' => 'deshabilitado', 4
        ]);
    }
}
