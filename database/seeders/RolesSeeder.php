<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'rol' => 'Logistica',
            'slug' => 'logistica',
            'description' =>'Entregar productos',
            'full-access' =>'no'
            ]);

            Role::create([
                'rol' => 'Cliente',
                'slug' => 'cliente',
                'description' =>'Realizar compras en linea',
                'full-access' =>'no'
                ]);

                Role::create([
                    'rol' => 'Repartidor',
                    'slug' => 'repartidor',
                    'description' =>'Entregar productos a domicilio',
                    'full-access' =>'no'
                    ]);



    }
}
