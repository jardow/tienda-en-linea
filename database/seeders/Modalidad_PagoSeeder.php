<?php

namespace Database\Seeders;

use App\Models\Modalidad_Pago;
use Illuminate\Database\Seeder;

class Modalidad_PagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Modalidad_Pago::create([
            'descripcion'=>'Tarjeta de Crédito / Débito',
            'codigo' => '19',
        ]);

        Modalidad_Pago::create([
            'descripcion'=>'Transferencia / Depósito',
            'codigo' => '20',
        ]);
    }
}
