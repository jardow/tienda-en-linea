<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PermissionInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

   //truncate tables
        DB::statement("SET foreign_key_checks=0");
        DB::table('role_user')->truncate();
        DB::table('permission_role')->truncate();
        Permission::truncate();
        Role::truncate();
        DB::statement("SET foreign_key_checks=1");


//User Admin
$useradmin=User::where('email','jcatagnaa@est.ups.edu.ec')->first();
if($useradmin){
	$useradmin->delete();
}
$useradmin=User::create([
'name' => 'Juan',
'surname' => 'Catagña',
'idcard' => '1717822080',
'phone' => '0984243670',
'city' => 'Quito',
'email' =>'jcatagnaa@est.ups.edu.ec',
'state'=> '1',
'avatar'=>'\images\perfil\3XNPo0Ro8ykE.jpg',
'password' =>Hash::make('admin')

]);

//rol admin
$roladmin=Role::create([
	'rol' => 'Admin',
	'slug' => 'admin',
	'description' => 'Administrador',
	'full-access' => 'yes'
]);

$useradmin->roles()->sync([$roladmin->id]);


//permission
$permission_all =[];
//permission role
$permission = Permission::create([
	'permission' => 'List role',
	'slug' => 'role.index',
	'description' => 'A user can list role',
]);
$permission_all[] =$permission->id;

//permission role
$permission = Permission::create([
	'permission' => 'Show role',
	'slug' => 'role.show',
	'description' => 'A user can show role',
]);
$permission_all[] =$permission->id;

//permission role
$permission = Permission::create([
	'permission' => 'Create role',
	'slug' => 'role.create',
	'description' => 'A user can create role',
]);
$permission_all[] =$permission->id;
//permission role
$permission = Permission::create([
	'permission' => 'Edit role',
	'slug' => 'role.edit',
	'description' => 'A user can edit role',
]);
$permission_all[] =$permission->id;

//permission role
$permission = Permission::create([
	'permission' => 'Destrory role',
	'slug' => 'role.destrory',
	'description' => 'A user can destrory role',
]);
$permission_all[] =$permission->id;

//permission user
$permission = Permission::create([
	'permission' => 'List user',
	'slug' => 'user.index',
	'description' => 'A user can list user',
]);
$permission_all[] =$permission->id;

//permission user
$permission = Permission::create([
	'permission' => 'Create user',
	'slug' => 'user.create',
	'description' => 'A user can create user',
]);
$permission_all[] =$permission->id;
//permission user
$permission = Permission::create([
	'permission' => 'Edit user',
	'slug' => 'user.edit',
	'description' => 'A user can edit user',
]);
$permission_all[] =$permission->id;

//permission user
$permission = Permission::create([
	'permission' => 'Destrory user',
	'slug' => 'user.destrory',
	'description' => 'A user can destrory user',
]);
$permission_all[] =$permission->id;



//Relacion permisos

//$roladmin->permissions()->sync($permission_all);   //comentamos

    }
}
