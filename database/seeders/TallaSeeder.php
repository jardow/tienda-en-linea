<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Talla;
use Illuminate\Support\Facades\DB;
use App\Models\Seccion;
use App\Models\Categoria;

class TallaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Talla::create([
            'talla' => 'Pequeña',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '1',

        ]);

        Talla::create([
            'talla' => 'Mediana',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '1',

        ]);


        Talla::create([
            'talla' => 'Grande',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '1',

        ]);


        Talla::create([
            'talla' => 'Extra Grande',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '1',

        ]);

        //Mujer Ropa /

        Talla::create([
            'talla' => 'Pequeña',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '1',

        ]);

        Talla::create([
            'talla' => 'Mediana',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '1',

        ]);


        Talla::create([
            'talla' => 'Grande',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '1',

        ]);


        Talla::create([
            'talla' => 'Extra Grande',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '1',

        ]);

        // Ñiños Ropa

        Talla::create([
            'talla' => '2',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '1',

        ]);

        Talla::create([
            'talla' => '3',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '1',

        ]);


        Talla::create([
            'talla' => '4',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '1',

        ]);


        Talla::create([
            'talla' => '5',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '1',

        ]);

        Talla::create([
            'talla' => '6',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '1',

        ]);

        Talla::create([
            'talla' => '7',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '1',

        ]);


        //Calzado Hombre

        Talla::create([
            'talla' => '36',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '37',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '2',

        ]);


        Talla::create([
            'talla' => '38',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '2',

        ]);


        Talla::create([
            'talla' => '39',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '2',

        ]);


        Talla::create([
            'talla' => '40',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '41',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '42',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '43',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '2',

        ]);


        Talla::create([
            'talla' => '44',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '1',
            'categoria_id' => '2',

        ]);


        //Mujer Calzado

        Talla::create([
            'talla' => '33',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '34',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '2',

        ]);


        Talla::create([
            'talla' => '35',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '36',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '2',

        ]);


        Talla::create([
            'talla' => '37',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '38',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '39',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '2',
            'categoria_id' => '2',

        ]);


        //Calzado Ñiños

        Talla::create([
            'talla' => '27',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '28',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '29',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '2',

        ]);


        Talla::create([
            'talla' => '30',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '2',

        ]);

        Talla::create([
            'talla' => '31',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '2',

        ]);


        Talla::create([
            'talla' => '32',
            'estado' => '1',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '3',
            'categoria_id' => '2',

        ]);


        Talla::create([
            'talla' => '0',
            'estado' => '2',   // 'estado' => 'deshabilitado', Hombre // Ropa
            'seccion_id' => '4',
            'categoria_id' => '3',

        ]);




    }
}
