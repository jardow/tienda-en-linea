<?php

namespace Database\Seeders;

use App\Models\Comprobantes;
use Illuminate\Database\Seeder;

class ComprobantesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comprobantes::create([
            'empresa_id'=>'1',
            'nombre' => 'FACTURA',
            'codigo' => '01',   // 'estado' => 'deshabilitado',1,
            'fechainicio' => '2022-01-24',

        ]);
    }
}
