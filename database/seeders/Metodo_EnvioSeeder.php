<?php

namespace Database\Seeders;

use App\Models\Metodo_Envio;
use Illuminate\Database\Seeder;

class Metodo_EnvioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Metodo_Envio::create([
            'codigo'=>'UIO-01',
            'descripcion' => 'Envío Local',
            'precio' => '3.50',   // 'estado' => 'deshabilitado',1,
        ]);

        Metodo_Envio::create([
            'codigo'=>'PROV-01',
            'descripcion' => 'Envío a Provincias',
            'precio' => '4.50',   // 'estado' => 'deshabilitado',1,
        ]);
    }
}
