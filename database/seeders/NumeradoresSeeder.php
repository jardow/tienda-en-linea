<?php

namespace Database\Seeders;

use App\Models\Numeradores;
use Illuminate\Database\Seeder;

class NumeradoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Numeradores::create([
            'empresa_id'=>'1',
            'comprobante_id' => '1',
            'establecimiento' => '003',   // 'estado' => 'deshabilitado',1,
            'puntoemision' => '001',
            'correlativo' =>'280'

        ]);
    }
}
