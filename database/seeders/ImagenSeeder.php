<?php

namespace Database\Seeders;

use App\Models\Imagen;
use Illuminate\Database\Seeder;

class ImagenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Imagen::create([
            'url' => '\images\producto\33.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 1
        ]);

        Imagen::create([
            'url' => '\images\producto\34.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 2
        ]);

        Imagen::create([
            'url' => '\images\producto\12.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 3
        ]);

        Imagen::create([
            'url' => '\images\producto\13.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 4
        ]);

        Imagen::create([
            'url' => '\images\producto\59.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 5
        ]);

        Imagen::create([
            'url' => '\images\producto\60.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 6
        ]);

        Imagen::create([
            'url' => '\images\producto\20.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 7
        ]);

        Imagen::create([
            'url' => '\images\producto\24.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 8
        ]);

        Imagen::create([
            'url' => '\images\producto\38.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 9
        ]);

        Imagen::create([
            'url' => '\images\producto\36.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 10
        ]);

        Imagen::create([
            'url' => '\images\producto\54.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 11
        ]);

        Imagen::create([
            'url' => '\images\producto\43.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 12
        ]);

        Imagen::create([
            'url' => '\images\producto\53.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 13
        ]);

        Imagen::create([
            'url' => '\images\producto\21.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 14
        ]);
        Imagen::create([
            'url' => '\images\producto\25.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 15
        ]);
        Imagen::create([
            'url' => '\images\producto\32.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 16
        ]);
        Imagen::create([
            'url' => '\images\producto\15.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 17
        ]);
        Imagen::create([
            'url' => '\images\producto\18.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 18
        ]);
        Imagen::create([
            'url' => '\images\producto\48.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 19
        ]);
        Imagen::create([
            'url' => '\images\producto\58.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 20
        ]);
        Imagen::create([
            'url' => '\images\producto\51.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 21
        ]);
        Imagen::create([
            'url' => '\images\producto\19.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 22
        ]);
        Imagen::create([
            'url' => '\images\producto\11.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 23
        ]);
        Imagen::create([
            'url' => '\images\producto\30.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 24
        ]);
        Imagen::create([
            'url' => '\images\producto\37.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 25
        ]);
        Imagen::create([
            'url' => '\images\producto\57.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 26
        ]);
        Imagen::create([
            'url' => '\images\producto\9.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 27
        ]);
        Imagen::create([
            'url' => '\images\producto\35.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 28
        ]);
        Imagen::create([
            'url' => '\images\producto\8.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 29
        ]);
        Imagen::create([
            'url' => '\images\producto\44.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 30
        ]);
        Imagen::create([
            'url' => '\images\producto\1.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 31
        ]);
        Imagen::create([
            'url' => '\images\producto\23.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 32
        ]);
        Imagen::create([
            'url' => '\images\producto\46.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 33
        ]);
        Imagen::create([
            'url' => '\images\producto\61.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 34
        ]);
        Imagen::create([
            'url' => '\images\producto\41.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 35
        ]);
        Imagen::create([
            'url' => '\images\producto\17.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 36
        ]);
        Imagen::create([
            'url' => '\images\producto\3.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 37
        ]);
        Imagen::create([
            'url' => '\images\producto\42.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 38
        ]);
        Imagen::create([
            'url' => '\images\producto\47.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 39
        ]);
        Imagen::create([
            'url' => '\images\producto\29.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 40
        ]);
        Imagen::create([
            'url' => '\images\producto\52.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 41
        ]);
        Imagen::create([
            'url' => '\images\producto\62.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 42
        ]);
        Imagen::create([
            'url' => '\images\producto\55.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 43
        ]);
        Imagen::create([
            'url' => '\images\producto\6.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 44
        ]);
        Imagen::create([
            'url' => '\images\producto\28.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 45
        ]);
        Imagen::create([
            'url' => '\images\producto\45.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 46
        ]);
        Imagen::create([
            'url' => '\images\producto\31.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 47
        ]);
        Imagen::create([
            'url' => '\images\producto\56.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 48
        ]);
        Imagen::create([
            'url' => '\images\producto\16.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 49
        ]);
        Imagen::create([
            'url' => '\images\producto\5.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 50
        ]);
        Imagen::create([
            'url' => '\images\producto\22.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 51
        ]);
        Imagen::create([
            'url' => '\images\producto\39.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 52
        ]);
        Imagen::create([
            'url' => '\images\producto\10.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 53
        ]);
        Imagen::create([
            'url' => '\images\producto\27.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 54
        ]);
        Imagen::create([
            'url' => '\images\producto\50.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 55
        ]);
        Imagen::create([
            'url' => '\images\producto\14.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 56
        ]);
        Imagen::create([
            'url' => '\images\producto\49.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 57
        ]);
        Imagen::create([
            'url' => '\images\producto\4.jpg',
            'perfil' => 'si',   // 'estado' => 'deshabilitado', 58
        ]);
        Imagen::create([
            'url' => '\images\producto\7.jpg',
            'perfil' => 'no',   // 'estado' => 'deshabilitado', 59
        ]);


    }
}
