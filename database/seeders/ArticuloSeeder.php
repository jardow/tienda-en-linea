<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Articulo;

class ArticuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Articulo::create([
            'descripcion' => 'Pantalon jeams',
            'estado' => '1',   // 'estado' => 'deshabilitado', 1
        ]);

        Articulo::create([
            'descripcion' => 'Camisa',
            'estado' => '1',   // 'estado' => 'deshabilitado', 2
        ]);

        Articulo::create([
            'descripcion' => 'Calzado casual',
            'estado' => '1',   // 'estado' => 'deshabilitado', 3
        ]);

        Articulo::create([
            'descripcion' => 'Auricular',
            'estado' => '1',   // 'estado' => 'deshabilitado', 4
        ]);

        Articulo::create([
            'descripcion' => 'Faja',
            'estado' => '1',   // 'estado' => 'deshabilitado', 5
        ]);

        Articulo::create([
            'descripcion' => 'Gancho giratorio',
            'estado' => '1',   // 'estado' => 'deshabilitado', 6
        ]);

        Articulo::create([
            'descripcion' => 'Mini Ventilador',
            'estado' => '1',   // 'estado' => 'deshabilitado', 7
        ]);

        Articulo::create([
            'descripcion' => 'Organizador',
            'estado' => '1',   // 'estado' => 'deshabilitado', 8
        ]);

        Articulo::create([
            'descripcion' => 'Pelusas',
            'estado' => '1',   // 'estado' => 'deshabilitado', 9
        ]);

        Articulo::create([
            'descripcion' => 'rodillo facial',
            'estado' => '1',   // 'estado' => 'deshabilitado', 10
        ]);

        Articulo::create([
            'descripcion' => 'Secadora de ropa',
            'estado' => '1',   // 'estado' => 'deshabilitado', 11
        ]);

        Articulo::create([
            'descripcion' => 'Soporte tablet',
            'estado' => '1',   // 'estado' => 'deshabilitado', 12
        ]);

        Articulo::create([
            'descripcion' => 'Vaporizador',
            'estado' => '1',   // 'estado' => 'deshabilitado', 13
        ]);

        Articulo::create([
            'descripcion' => 'Blanqueador',
            'estado' => '1',   // 'estado' => 'deshabilitado', 14
        ]);


        Articulo::create([
            'descripcion' => 'Mascarrilla de carbón',
            'estado' => '1',   // 'estado' => 'deshabilitado', 15
        ]);

        Articulo::create([
            'descripcion' => 'Humificador',
            'estado' => '1',   // 'estado' => 'deshabilitado', 16
        ]);

        Articulo::create([
            'descripcion' => 'Mascarrilla 24K gold',
            'estado' => '1',   // 'estado' => 'deshabilitado', 17
        ]);

        Articulo::create([
            'descripcion' => 'Capsula vitamina E',
            'estado' => '1',   // 'estado' => 'deshabilitado', 18
        ]);

        Articulo::create([
            'descripcion' => 'Llave giratoria de agua',
            'estado' => '1',   // 'estado' => 'deshabilitado', 19
        ]);

        Articulo::create([
            'descripcion' => 'Paleta gigante frozen',
            'estado' => '1',   // 'estado' => 'deshabilitado', 20
        ]);

        Articulo::create([
            'descripcion' => 'Tinta labios caramelo',
            'estado' => '1',   // 'estado' => 'deshabilitado', 21
        ]);

        Articulo::create([
            'descripcion' => 'Tinta labios helado',
            'estado' => '1',   // 'estado' => 'deshabilitado', 22
        ]);

        Articulo::create([
            'descripcion' => 'Samsung A20',
            'estado' => '1',   // 'estado' => 'deshabilitado', 23
        ]);


        Articulo::create([
            'descripcion' => 'LG OLED',
            'estado' => '1',   // 'estado' => 'deshabilitado', 24
        ]);

        Articulo::create([
            'descripcion' => 'Mascarilla de frutilla',
            'estado' => '1',   // 'estado' => 'deshabilitado', 24
        ]);




    }
}
