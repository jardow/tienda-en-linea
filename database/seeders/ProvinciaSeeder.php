<?php

namespace Database\Seeders;

use App\Models\Provincia;
use Illuminate\Database\Seeder;

class ProvinciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Provincia::create(['provincia_id'=>'17','provincia'=>'PICHINCHA',]);
        Provincia::create(['provincia_id'=>'1','provincia'=>'AZUAY']);
        Provincia::create(['provincia_id'=>'2','provincia'=>'BOLÍVAR']);
        Provincia::create(['provincia_id'=>'3','provincia'=>'CAÑAR']);
        Provincia::create(['provincia_id'=>'4','provincia'=>'CARCHI']);
        Provincia::create(['provincia_id'=>'5','provincia'=>'COTOPAXI']);
        Provincia::create(['provincia_id'=>'6','provincia'=>'CHIMBORAZO']);
        Provincia::create(['provincia_id'=>'7','provincia'=>'EL ORO']);
        Provincia::create(['provincia_id'=>'8','provincia'=>'ESMERALDAS']);
        Provincia::create(['provincia_id'=>'9','provincia'=>'GUAYAS']);
        Provincia::create(['provincia_id'=>'10','provincia'=>'IMBABURA']);
        Provincia::create(['provincia_id'=>'11','provincia'=>'LOJA']);
        Provincia::create(['provincia_id'=>'12','provincia'=>'LOS RIOS']);
        Provincia::create(['provincia_id'=>'13','provincia'=>'MANABI']);
        Provincia::create(['provincia_id'=>'14','provincia'=>'MORONA SANTIAGO']);
        Provincia::create(['provincia_id'=>'15','provincia'=>'NAPO']);
        Provincia::create(['provincia_id'=>'16','provincia'=>'PASTAZA']);
        Provincia::create(['provincia_id'=>'18','provincia'=>'TUNGURAHUA']);
        Provincia::create(['provincia_id'=>'19','provincia'=>'ZAMORA CHINCHIPE']);
        Provincia::create(['provincia_id'=>'20','provincia'=>'GALAPAGOS']);
        Provincia::create(['provincia_id'=>'21','provincia'=>'SUCUMBÍOS']);
        Provincia::create(['provincia_id'=>'22','provincia'=>'ORELLANA']);


    }
}
