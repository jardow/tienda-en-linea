<?php

namespace Database\Seeders;

use App\Models\EstadoCompra;
use Illuminate\Database\Seeder;

class EstadoCompraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoCompra::create([
            'descripcion'=>'Pagado',
        ]);
        EstadoCompra::create([
            'descripcion'=>'Pendiente',
        ]);

    }
}
