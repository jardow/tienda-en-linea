<?php

namespace Database\Seeders;

use App\Models\DatosFacturacionElectronica;
use Illuminate\Database\Seeder;

class Datos_Facturacion_electronicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DatosFacturacionElectronica::create([
            'empresa_id' => '1',
            'contribuyente' => 'no',
            'ubicacionarchivop12' => '\Factura\lib_firma_sri\firma\nelly_maria_cuenca_macas.p12',
            'contrasena' => 'yERAY110',
            'ubicacionruta' => '',
            'pruebaproduccion' => '1',

        ]);
    }
}
