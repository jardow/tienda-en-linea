<?php

namespace Database\Seeders;

use App\Models\Relaccion;
use Illuminate\Database\Seeder;

class RelacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Relaccion::create([
            'estado' => '1',// 'Jeams
            'color_id' => '9',
            'categoria_id' =>'1',
            'seccion_id' =>'1',
            'talla_id' => '2',
            'articulo_id' => '1',
        ]);

        Relaccion::create([
            'estado' => '1',// 'camisa
            'color_id' => '2',
            'categoria_id' =>'1',
            'seccion_id' =>'1',
            'talla_id' => '1',
            'articulo_id' => '2',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Calzado Casual
            'color_id' => '1',
            'categoria_id' =>'2',
            'seccion_id' =>'1',
            'talla_id' => '19',
            'articulo_id' => '3',
        ]);


        Relaccion::create([
            'estado' => '1',// 'Auricular
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '4',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Faja
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '5',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Gancho giratorio
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '6',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Mini Ventilador
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '7',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Organizador
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '8',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Pulusas
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '9',
        ]);


        Relaccion::create([
            'estado' => '1',// 'Rodillo facial
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '10',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Secadora de ropa
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '11',
        ]);


        Relaccion::create([
            'estado' => '1',// 'soporte
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '12',
        ]);

        Relaccion::create([
            'estado' => '1',// 'vaporizador
            'color_id' => '10',
            'categoria_id' =>'3',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '13',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Blanqueador
            'color_id' => '10',
            'categoria_id' =>'4',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '14',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Mascarilla de carbón
            'color_id' => '10',
            'categoria_id' =>'4',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '15',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Humificador
            'color_id' => '10',
            'categoria_id' =>'4',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '16',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Mascarilla 24K gold
            'color_id' => '10',
            'categoria_id' =>'4',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '17',
        ]);

        Relaccion::create([
            'estado' => '1',// 'capsulas vitamina E
            'color_id' => '10',
            'categoria_id' =>'5',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '18',
        ]);

        Relaccion::create([
            'estado' => '1',// 'LLave giratoria de agua
            'color_id' => '10',
            'categoria_id' =>'6',
            'seccion_id' =>'4',
            'talla_id' => '0',
            'articulo_id' => '19',
        ]);

        Relaccion::create([
            'estado' => '1',// 'LLave giratoria de agua
            'color_id' => '10',
            'categoria_id' =>'7',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '20',
        ]);



        Relaccion::create([
            'estado' => '1',// 'tinta labios caramelo
            'color_id' => '10',
            'categoria_id' =>'7',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '21',
        ]);

        Relaccion::create([
            'estado' => '1',// 'tinta labios helado
            'color_id' => '10',
            'categoria_id' =>'7',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '22',
        ]);

        Relaccion::create([
            'estado' => '1',// 'Samsun a20
            'color_id' => '10',
            'categoria_id' =>'8',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '23',
        ]);

        Relaccion::create([
            'estado' => '1',// 'LG
            'color_id' => '10',
            'categoria_id' =>'9',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '24',
        ]);


        Relaccion::create([
            'estado' => '1',// 'Mascarilla de frutilla
            'color_id' => '10',
            'categoria_id' =>'4',
            'seccion_id' =>'2',
            'talla_id' => '0',
            'articulo_id' => '25',
        ]);


    }
}
