<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('comprobante_id');
            $table->foreign('comprobante_id')
                ->references('id')
                ->on('comprobantes')
                ->onDelete('cascade');
            $table->unsignedBigInteger('modalidad_pago_id');
            $table->foreign('modalidad_pago_id')
                ->references('id')
                ->on('modalidad_pagos')
                ->onDelete('cascade');
            $table->unsignedBigInteger('metodo_envio_id');
            $table->foreign('metodo_envio_id')
                ->references('id')
                ->on('metodo_envios')
                ->onDelete('cascade');
                $table->unsignedBigInteger('estado_compra_id');
                $table->foreign('estado_compra_id')
                    ->references('id')
                    ->on('estado_compras')
                    ->onDelete('cascade');
            $table->dateTime('fecha_emision');
            $table->string('claveacceso');
            $table->string('serie');
            $table->decimal('iva', 18,2);
            $table->decimal('total', 18,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
