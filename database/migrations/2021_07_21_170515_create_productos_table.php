<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('relaccion_id');
            $table->foreign('relaccion_id')
                ->references('id')
                ->on('relaccions')
                ->onDelete('cascade');
            $table->unsignedBigInteger('origen_id');
            $table->foreign('origen_id')
                ->references('id')
                ->on('origens')
                ->onDelete('cascade');
            $table->string('codigo_producto', 50);
            $table->decimal('precio', 18,2);
            $table->integer('stock');
            $table->string('descripcion',300);
            $table->dateTime('fecha_ingreso');
            $table->string('estado',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
