<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnviosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('factura_id');
            $table->foreign('factura_id')
                ->references('id')
                ->on('facturas')
                ->onDelete('cascade');
            //$table->integer('tipo_envio');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('cedula');
            $table->string('canton');
            $table->string('parroquia');
            $table->string('referencia');
            $table->string('telefono');
            $table->string('ciudad');
            $table->integer('logistica_id')->nullable();
            $table->integer('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envios');
    }
}
