<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTallasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        Schema::create('tallas', function (Blueprint $table) {
            $table->id();
            $table->string('talla',50);
            $table->string('estado',1);
            $table->unsignedBigInteger('seccion_id');
            $table->foreign('seccion_id')
                  ->references('id')
                  ->on('seccions')
                  ->onDelete('cascade');
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')
                        ->references('id')
                        ->on('categorias')
                        ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tallas');
    }
}
