<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentosFacturacionElectronicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos_facturacion_electronicas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')
                ->references('id')
                ->on('empresas')
                ->onDelete('cascade');
            $table->string('estado');
            $table->string('ambiente')->nullable();
            $table->string('numeroautorizacion')->nullable();
            $table->string('fechaautorizacion')->nullable();
            $table->string('identificador')->nullable();
            $table->string('mensaje',455)->nullable();
            $table->string('informacionadicional')->nullable();
            $table->string('tipo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentos_facturacion_electronicas');
    }
}
