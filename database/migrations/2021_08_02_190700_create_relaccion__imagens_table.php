<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelaccionImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relaccion__imagens', function (Blueprint $table) {
            $table->id();
            $table->foreignId('imagen_id') -> references('id')->on('imagens')->onDelete('cascade');
            $table->foreignId('relaccion_id') -> references('id')->on('relaccions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relaccion__imagens');
    }
}
