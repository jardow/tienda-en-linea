<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('razonsocial');
            $table->string('nombre');
            $table->string('direccion');
            $table->string('lugar');
            $table->string('telefono');
            $table->string('celular');
            $table->string('correo');
            $table->string('ruc');
            $table->string('contribuyente');
            $table->string('contabilidad');
            $table->string('logo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
