<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelaccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relaccions', function (Blueprint $table) {
            $table->id();
            $table->string('estado',1);

            $table->unsignedBigInteger('color_id');
            $table->foreign('color_id')
                ->references('id')
                ->on('colors')
                ->onDelete('cascade');

            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')
                ->references('id')
                ->on('categorias')
                ->onDelete('cascade');

            $table->unsignedBigInteger('seccion_id');
            $table->foreign('seccion_id')
                ->references('id')
                ->on('seccions')
                ->onDelete('cascade');

            $table->unsignedBigInteger('talla_id');
            $table->foreign('talla_id')
            ->references('id')
            ->on('tallas')
            ->onDelete('cascade');
            $table->unsignedBigInteger('articulo_id');
            $table->foreign('articulo_id')
                ->references('id')
                ->on('articulos')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relaccions');
    }
}
