
function categoriaf() {
    let idCategoria = document.getElementById('categoria');
    let categoria = idCategoria.value;
    let idSeccion = document.getElementById('seccion');
    let seccion = idSeccion.value;
    let _token = $('meta[name="csrf-token"]').attr('content');
    // Empty the dropdown
    $('#talla').find('option').not(':first').remove();
    if ((categoria == 1 || categoria == 2) &&(seccion==1||seccion==2 ||seccion==3)) {

        $.ajax({
            type: 'post',
            url: '/combo',
            data: {
                categoria: categoria,
                seccion: seccion,
                _token: _token,
            },
            dataType: 'json',
            success: function (response) {
                var len = 0;
                if (response['data'] != null) {
                    len = response['data'].length;
                }
                if (len > 0) {
                    // Read data and create <option >
                    for (var i = 0; i < len; i++) {
                        var id = response['data'][i].id;
                        var name = response['data'][i].talla;
                        var option = "<option value='" + id + "'>" + name + "</option>";
                        $("#talla").append(option);
                    }
                }
            }
        });
        document.getElementById("talla").style.display = "inline";;
        document.getElementById("talla1").style.display = "inline";;

    }else{
        document.getElementById("talla").style.display = "none";;
        document.getElementById("talla1").style.display = "none";;
    }
}

function setTwoNumberDecimal(event) {
    this.value = parseFloat(this.value).toFixed(2);
}

