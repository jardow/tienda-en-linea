<?php

session_start();
require_once('../../lib/nusoap.php');
require_once('class/generarPDF.php');
$claveAcceso = $_POST['claveAcceso'];
$service = $_POST['service'];

$ambiente=null;
$numeroautorizacion = null;
$fechaautorizacion=null;
$informacionAdicional = null;
$tipo = null;
$id_error=null;
$mensaje=null;



//DATA BD
$host_bd = '127.0.0.1';
$pass_bd = '';
//$host_bd = $_POST['host_bd'];
$user_bd = 'root';
$database = 'libfactura';
$port_bd = 3306;
$conn = new mysqli($host_bd, $user_bd, $pass_bd, $database);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}



//EndPoint
$servicio = "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl"; //url del servicio
$parametros = array(); //parametros de la llamada
$parametros['claveAccesoComprobante'] = $claveAcceso;

$client = new nusoap_client($servicio);


$error = $client->getError();



$client->soap_defencoding = 'utf-8';


$result = $client->call("autorizacionComprobante", $parametros, "http://ec.gob.sri.ws.autorizacion");
var_dump($result);
$_SESSION['autorizacionComprobante'] = $result;
$response = array();



if ($client->fault) {

   // echo serialize($result);
    $result = serialize($result);

    $file_error = fopen('../../comprobantes/errores/' . $claveAcceso . ".xml", "w");
                fwrite($file_error, "Servicio: " . $service . PHP_EOL);
                fwrite($file_error, "Clave Acceso: " . $claveAcceso . PHP_EOL);
                fwrite($file_error, "Respuesta: " . print_r($result, true) . PHP_EOL);
                fwrite($file_error, "\n__________________________________________________________________\n" . PHP_EOL);
                fclose($file_error);

    $estado = 'ERROR';
} else {
    $error = $client->getError();
    if ($error) {
        echo serialize($error);
        $result = serialize($result);

        $file_error = fopen('../../comprobantes/errores/' . $claveAcceso . ".xml", "w");
                fwrite($file_error, "Servicio: " . $service . PHP_EOL);
                fwrite($file_error, "Clave Acceso: " . $claveAcceso . PHP_EOL);
                fwrite($file_error, "Respuesta: " . print_r($result, true) . PHP_EOL);
                fwrite($file_error, "\n__________________________________________________________________\n" . PHP_EOL);
                fclose($file_error);

        $estado = 'ERROR';
    } else {

        // echo serialize($result);
        if ($result['autorizaciones']['autorizacion']['estado'] != 'AUTORIZADO') {



                $file_error = fopen('../../comprobantes/errores/' . $claveAcceso . ".xml", "w");
                fwrite($file_error, "Servicio: " . $service . PHP_EOL);
                fwrite($file_error, "Clave Acceso: " . $claveAcceso . PHP_EOL);
                fwrite($file_error, "Respuesta: " . print_r($result, true) . PHP_EOL);
                fwrite($file_error, "\n__________________________________________________________________\n" . PHP_EOL);
                fclose($file_error);



            $resultado = serialize($result);
            $estado = $result['autorizaciones']['autorizacion']['estado'];
            $id_error = $result['autorizaciones']['autorizacion']["mensajes"]["mensaje"]["identificador"];
            $mensaje = $result['autorizaciones']['autorizacion']["mensajes"]["mensaje"]["mensaje"];
            $informacionAdicional = $result['autorizaciones']['autorizacion']["mensajes"]["mensaje"]["informacionAdicional"];
            $tipo = $result['autorizaciones']['autorizacion']["mensajes"]["mensaje"]["tipo"];




            $sqlCount = "select * from sris where claveaceso ='$claveAcceso'";
            $rowcount = 0;

            echo serialize($mensaje);
           foreach ($conn->query($sqlCount) as $row) {
                $rowcount = mysqli_num_rows($conn->query($sqlCount));
            }

            if($rowcount==0){
                $sql = "INSERT INTO `sris` (`claveacceso`, `coderror`, `tipo`, `mensaje`, `informacionadicional` , `estado`)
                                    VALUES ($claveAcceso, '$id_error','$tipo', '$mensaje', \"$informacionAdicional\",'$estado');";
                if ($conn->query($sql) === TRUE) {
                    echo "";
                } else {
                    die("Error created record: " . $conn->error);
                }
            }else{

                $sql = "UPDATE sris SET
                 coderror='$id_error',tipo='$tipo',
                  mensaje=\"$estado\, informacionadicional=\"$informacionAdicional\",
                   estado ='$estado'
                   WHERE claveacceso='$claveAcceso';";
                if ($conn->query($sql) === TRUE) {
                    echo "";
                } else {
                    die("Error created record: " . $conn->error);
                }

            }



        } else {
            if (!empty($result['autorizaciones']['autorizacion']['comprobante'])) {
                $estado = $result['autorizaciones']['autorizacion']['estado'];
                $file_comprobante = fopen('../../comprobantes/docFirmados/' . $claveAcceso . ".xml", "w");

                $rutafirmado  = 'Factura/lib_firma_sri/comprobantes/docFirmados/'. $claveAcceso . ".xml";
                $rutapdf  = 'Factura/lib_firma_sri/comprobantes/docRide/'. $claveAcceso . ".pdf";
                $comprobante = $client->responseData;
                $simplexml = simplexml_load_string(utf8_encode($comprobante));
                $dom = new DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $xml = str_replace(['&lt;', '&gt;'], ['<', '>'], $comprobante);

                $numeroautorizacion = $result['autorizaciones']['autorizacion']['numeroAutorizacion'];
                $fechaautorizacion = $result['autorizaciones']['autorizacion']['fechaAutorizacion'];
                $ambiente = $result['autorizaciones']['autorizacion']['ambiente'];
                $estado = $result['autorizaciones']['autorizacion']['estado'];


                fwrite($file_comprobante, $xml . PHP_EOL);
                fclose($file_comprobante);


                $dataComprobante = simplexml_load_string(utf8_encode($result['autorizaciones']['autorizacion']['comprobante']));


                if ($dataComprobante->infoFactura) {
                        var_dump($dataComprobante->infoFactura);

                    $facturaPDF = new generarPDF();
                    $facturaPDF->facturaPDF($dataComprobante, $claveAcceso);
                }
                if ($dataComprobante->infoNotaCredito) {
                    //     var_dump($dataComprobante->infoFactura);
                    $facturaPDF = new generarPDF();
                    $facturaPDF->notaCreditoPDF($dataComprobante, $claveAcceso);
                }
                if ($dataComprobante->infoCompRetencion) {
                    //     var_dump($dataComprobante->infoFactura);
                    $facturaPDF = new generarPDF();
                    $facturaPDF->comprobanteRetencionPDF($dataComprobante, $claveAcceso);
                }
                if ($dataComprobante->infoGuiaRemision) {
                    //     var_dump($dataComprobante->infoFactura);
                    $facturaPDF = new generarPDF();
                    $facturaPDF->guiaRemisionPDF($dataComprobante, $claveAcceso);
                }

                if ($dataComprobante->infoNotaDebito) {
                    //     var_dump($dataComprobante->infoFactura);
                    $facturaPDF = new generarPDF();
                    $facturaPDF->notaDebitoPDF($dataComprobante, $claveAcceso);
                }
            }

        }




        $sql = "UPDATE documento_facturacion_electronicas
        SET rutafirmado='".$rutafirmado."',
        rutapdf='".$rutapdf."' , estado='$estado' , ambiente='$ambiente' ,
         numeroautorizacion ='$numeroautorizacion', fechaautorizacion='$fechaautorizacion',
         identificador='$id_error',
         mensaje='$mensaje',
         informacionadicional='$informacionAdicional',
         tipo='$tipo'
         WHERE claveaceso=".$claveAcceso.";";
    $succes = false;


    if ($conn->query($sql) === TRUE) {
        $succes = true;
    } else {
        $succes = false;
    }


    }
}






