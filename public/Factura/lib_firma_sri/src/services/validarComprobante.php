<?php

session_start();
require_once('../../lib/nusoap.php');
header("Content-Type: text/plain");
$content = file_get_contents("../../facturaFirmada.xml");
$mensaje = base64_encode($content);
$claveAcceso = $_POST['claveAcceso'];
$service = $_POST['service'];
//Conexion BD
$host_bd = '127.0.0.1';
$pass_bd = '';
$user_bd = 'root';
$database = 'libfactura';
$port_bd = 3306;
$conn = new mysqli($host_bd, $user_bd, $pass_bd, $database);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//EndPoint
$servicio = "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantesOffline?wsdl"; //url del servicio
$parametros = array(); //parametros de la llamada
$parametros['xml'] = $mensaje;
$client = new nusoap_client($servicio);
$estado = 'CREADA';
$client->soap_defencoding = 'utf-8';
$result = $client->call("validarComprobante", $parametros, "http://ec.gob.sri.ws.recepcion");
$response = array();
$_SESSION['validarComprobante'] = $result;
if ($client->fault) {
    echo serialize($result);
    $estado = 'ERROR';
} else {
    $error = $client->getError();
    if ($error) {
        $estado = 'ERROR';
        echo serialize($error);
    } else {
        if ($result['estado'] == 'RECIBIDA') {
            $estado = 'RECIBIDA';
            $estado = $result['estado'];
            $id_error = 0;
            $mensaje = 'Comprobante Recibido';
            $informacionAdicional = 'Pendiente de autorizacion';
            $tipo = 'MENSAJE';
            $sqlCount = "select * from sris where claveacceso  ='$claveAcceso'";
            $rowcount = 0;
            foreach ($conn->query($sqlCount) as $row) {
                $rowcount = mysqli_num_rows($conn->query($sqlCount));
            }
            if($rowcount==0){
                $sql = "INSERT INTO `sris` (`claveacceso`, `coderror`, `tipo`, `mensaje`, `informacionadicional` , `estado`) VALUES ($claveAcceso, '$id_error','$tipo', '$mensaje', \"$informacionAdicional\",'$estado');";
                if ($conn->query($sql) === TRUE) {
                    echo "";
                } else {
                    die("Error created record: " . $conn->error);
                }
            }else{

                $sql = "UPDATE sris SET claveacceso ='$claveAcceso', coderror='$id_error',tipo='$tipo',  mensaje=\"$estado\, informacionadicional=\"$informacionAdicional\", estado ='$estado' WHERE claveacceso='$claveAcceso';";
                if ($conn->query($sql) === TRUE) {
                    echo "";
                } else {
                    die("Error created record: " . $conn->error);
                }
            }


        } else {
            $estado = $result['estado'];
            $estado = 'DEVUELTA';
            $id_factura = $id_factura;
            $id_error = $result["comprobantes"]["comprobante"]["mensajes"]["mensaje"]["identificador"];
            $mensaje = $result["comprobantes"]["comprobante"]["mensajes"]["mensaje"]["mensaje"];
            $informacionAdicional = $result["comprobantes"]["comprobante"]["mensajes"]["mensaje"]["informacionAdicional"];
            $tipo = $result["comprobantes"]["comprobante"]["mensajes"]["mensaje"]["tipo"];
            $sqlCount = "select * from sris where claveacceso  ='$claveAcceso'";
            $rowcount = 0;
            foreach ($conn->query($sqlCount) as $row) {
                $rowcount = mysqli_num_rows($conn->query($sqlCount));
            }
            if($rowcount==0){
                $sql = "INSERT INTO `sris` (`claveacceso`, `coderror`, `tipo`, `mensaje`, `informacionadicional` , `estado`) VALUES ($claveAcceso, '$id_error','$tipo', '$mensaje', \"$informacionAdicional\",'$estado');";
                if ($conn->query($sql) === TRUE) {
                    echo "";
                } else {
                    die("Error created record: " . $conn->error);
                }
            }else{

                $sql = "UPDATE sris SET claveacceso ='$claveAcceso', coderror='$id_error',tipo='$tipo',  mensaje=\"$estado\, informacionadicional=\"$informacionAdicional\", estado ='$estado' WHERE claveacceso='$claveAcceso';";
                if ($conn->query($sql) === TRUE) {
                    echo "";
                } else {
                    die("Error created record: " . $conn->error);
                }
            }


            echo $estado;
        }
        echo serialize($result);
    }
    $sql = "UPDATE documento_facturacion_electronicas SET estado='".$estado."' WHERE claveaceso =".$claveAcceso."";

    $succes = false;
    if ($conn->query($sql) === TRUE) {
        $succes = true;
    } else {
        $succes = false;
    }

}




