@extends('layouts.master.master')
@section('title', 'Inicio')
@section('content')
<section id="home-section" class="hero">
    <div class="home-slider owl-carousel">
    <div class="slider-item js-fullheight">
        <div class="overlay"></div>
      <div class="container-fluid p-0">
        <div class="row d-md-flex no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
            <img class="one-third order-md-last img-fluid" src="images/bg_1.png" alt="">
            <div class="one-forth d-flex align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                <div class="text">
                    <span class="subheading">Ofertas</span>
                    <div class="horizontal">
                      <h1 class="mb-4 mt-3">ROPA</h1>
                      <p class="mb-4">Hasta el 20% de descuento</p>

                      <p><a href="{{ route('catalogo.index') }}" class="btn-custom">Comprar Ahora</a></p>
                    </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="slider-item js-fullheight">
        <div class="overlay"></div>
      <div class="container-fluid p-0">
        <div class="row d-flex no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
            <img class="one-third order-md-last img-fluid" src="images/bg_2.png" alt="">
            <div class="one-forth d-flex align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                <div class="text">
                    <span class="subheading">Zapatos</span>
                    <div class="horizontal">
                      <h1 class="mb-4 mt-3"></h1>
                      <p class="mb-4">Nueva Colección</p>
                      <p><a href="{{ route('catalogo.index') }}" class="btn-custom">Comprar Ahora</a></p>
                    </div>
              </div>
            </div>
          </div>
      </div>
    </div>
</div>

<div class="container" style="margin-top: 80px">
    <!--
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Shop</li>
        </ol>
    </nav>
-->
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ofertas</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <hr>
            <div class="row">

                @foreach($products as $pro)
                    <div class="col-lg-3">
                        <div class="card" style="margin-bottom: 20px; height: auto;">

                            <img src="{{$pro->url}}"
                                 class="card-img-top mx-auto"
                                 style="height: 150px; width: 150px;display: block;"
                                 alt="{{ $pro->url }}"
                            >
                            <div class="card-body">

                                    <a href="{{ route('detalleOferta.edit',Crypt::encrypt($pro->id))}}"><h6 class="card-title">{{ $pro->articulo }}</h6></a>
                                    <p align="left"> </p>
                                    <p align="left">${{$pro->precio_oferta}}</p>

                                <form action="{{ route('cart.store') }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $pro->id }}" id="id" name="id">
                                    <input type="hidden" value="{{ $pro->articulo }}" id="name" name="name">
                                    <input type="hidden" value="{{ $pro->precio_oferta }}" id="price" name="price">
                                    <input type="hidden" value="{{ $pro->url }}" id="img" name="img">
                                    <input type="hidden" value="{{ $pro->stock }}" id="stock" name="stock">
                                    <input type="hidden" value="1" id="quantity" name="quantity">
                                    <div class="card-footer" style="background-color: white;">
                                          <div class="row">
                                            <button class="btn btn-secondary btn-sm" class="tooltip-test" title="añadir">
                                                <i class="fa fa-shopping-cart"></i> añadir al carrito
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
</section>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/aos.js') }}"></script>
<script src="{{ asset('js/scrollax.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<link  href="{{ asset('css/animate.css')}}" rel="stylesheet">
<link  href="{{ asset('css/owl.carousel.min.css')}}"  rel="stylesheet">
<link  href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">



@endsection
