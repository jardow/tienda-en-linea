@extends('adminlte::page')
@section('title', 'Repartidor || Proceso Entrega')

@section('plugins.Sweetalert2', true)


@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">


<style>
    input[type="file"] {
        font-size: 0.7em;
        padding-top: 0.35rem;
    }

</style>

@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Repartidor</li>
                        <li class="breadcrumb-item active" aria-current="page">Proceso Entrega</li>
                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')



        <table class="table table-striped" id="colores">
            <thead>
                <tr>
                    <th scope="col">Cod Envío</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Ciudad</th>
                    <th scope="col">Cantón</th>
                    <th scope="col">Parroquia</th>
                    <th scope="col">Referencia</th>
                    <th scope="col">Guía</th>
                    <th scope="col">Acciones</th>
                    <!--
            <th>ELIMINAR</th>
            -->

                </tr>
            </thead>

            <tbody>
                @foreach ($detalle_envios as $detalle_envio )


                    <tr>
                        <form action="{{ route('entregar-pedido-repartidor.update', $detalle_envio->id)}}" method="POST" enctype="multipart/form-data">
                            @method('Put')
                            @csrf
                        <td scope="row">{{ $detalle_envio->id }}</td>

                        <td>{{ $detalle_envio->nombres }}</td>
                        <td>{{ $detalle_envio->apellidos }}</td>
                        <td> <a href="{{ URL::to('https://wa.me/+593'.$detalle_envio->telefono.'/?text=Estimado cliente su producto no puede ser entregado, enviar su ubicación por este medio  Tienda en línea Store Valentina') }}">{{ $detalle_envio->telefono }}</a></td>
                        <td>{{ $detalle_envio->ciudad }}</td>
                        <td>{{ $detalle_envio->canton }}</td>
                        <td>{{ $detalle_envio->parroquia }}</td>
                        <td>{{ $detalle_envio->referencia }}</td>
                        <td><input style="position:relative" class="form-control" accept="image/png,image/jpeg,image/jpg" type="file" name="guia" id="guia"></td>
                        <td>
                            <input type="submit" class="btn btn-outline-success" value="Confirmar">
                        </td>
                    </form>
                    </tr>
                    @endforeach

            </tbody>

        </table>

    </div>
</div>
@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    $('#colores').DataTable({
        responsive: true
        , autoWidth: false,

        "language": {
            // "lengthMenu": "Mostrar _MENU_ registros por página",
            "lengthMenu": "Mostrar " +
                `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> ` +
                "registros por página"
            , "zeroRecords": "No se encontró nada,  - lo siento"
            , "info": "Mostrando página _PAGE_ de _PAGES_"
            , "infoEmpty": "No hay registros disponibles"
            , "infoFiltered": "(filtrado de _MAX_ registros totales)"
            , 'search': 'Buscar:'
            , 'paginate': {
                'next': 'Siguiente'
                , 'previous': 'Anterior'
            }
        }
    });

</script>
@endsection
