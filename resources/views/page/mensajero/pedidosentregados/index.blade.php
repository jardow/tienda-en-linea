@extends('adminlte::page')
@section('title', 'Repartidor || Reporte Entrega')

@section('plugins.Sweetalert2', true)


@section('css')
    <link  rel="stylesheet" href="/css/admin_custom.css">
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Repartidor</li>
                        <li class="breadcrumb-item active" aria-current="page">Entrega</li>
                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')




    <table class="table table-striped" id="colores">
        <thead>
            <tr>
                <th scope="col">Cod Envío</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Ciudad</th>
                <th scope="col">Cantón</th>
                <th scope="col">Parroquia</th>
                <th scope="col">Fecha entrega</th>
                <th scope="col">Guía</th>
                <th scope="col">Estado</th>
                <!--
            <th>ELIMINAR</th>
            -->

            </tr>
        </thead>
        <tbody>
@foreach ($detalle_envios as $detalle_envio )


            <tr>
                <td scope="row">{{ $detalle_envio->id }}</td>

                <td>{{ $detalle_envio->nombres }}</td>
                <td>{{ $detalle_envio->apellidos }}</td>
                <td>{{ $detalle_envio->telefono }}</td>
                <td>{{ $detalle_envio->ciudad }}</td>
                <td>{{ $detalle_envio->canton }}</td>
                <td>{{ $detalle_envio->parroquia }}</td>
                <td> {{ \Carbon\Carbon::parse(strtotime($detalle_envio->created_at))->formatLocalized('%d  %B %Y')  }}</td>
                <td><a class="btn btn-outline-success"  href="{{ url('repartidor/pedidos/entregados/guia' ,Crypt::encrypt( $detalle_envio->envio_id))}}"><img src="{{ $detalle_envio->guia }}" width="35"></img></a></td>

                    <td>Entregado</td>


            </tr>
            @endforeach

        </tbody>
    </table>

    </div>
  </div>
@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
