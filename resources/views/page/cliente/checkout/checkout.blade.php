@extends('layouts.master.master')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('title', 'Cliente Tienda || Inicio')
<style>
    .disablebutton {
        pointer-events: none;
        opacity: 0.4;
    }

</style>

@section('content')
<div class="container" style="margin-top: 80px">
    <div class="row">
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Cliente</li>
                    <li class="breadcrumb-item active" aria-current="page">Carrito</li>
                    <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container margin_30">
        @include('custom.message')
        <!-- /page_header -->
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div id="paso1" class="step first {{Session::get('proceso') ==1 ? '' : 'disablebutton'}}">
                    <h3>1. Información de envío y Facturación </h3>
                    <form action="{{route('info_envio')}}" method="POST">
                        @csrf
                        <div class="tab-content checkout">

                            @php

                            $hola=null;
                            if (Session::has('info')){

                            foreach(Session::get('info') as $test)
                            {

                            }


                            $hola="Hay seccion:";
                            $bandera="true";


                            }else{
                            $bandera="false";
                            }
                            @endphp

                            @if($bandera=="false")



                            <div class="tab-pane fade show active" id="tab_1" role="tabpanel" aria-labelledby="tab_1">


                                <hr>

                                <div class="row no-gutters">
                                    <div class="col-6 form-group pr-1">
                                        <input type="text" name="nombre" class="form-control" placeholder="Nombre" value="{{ Auth::user()->name}}" required>
                                    </div>
                                    <div class="col-6 form-group pl-1">
                                        <input type="text" name="apellido" class="form-control" placeholder="Apellido" value="{{ Auth::user()->surname}}" required>
                                    </div>
                                </div>

                                <hr>
                                <div class="row no-gutters">
                                    <div class="col-6 form-group pr-1">
                                        <div class="custom-select-form">
                                            <select class="browser-default custom-select" aria-label="Default select example" name="provincia" id="provincia" required>
                                                <option value="0"selected>Ciudad</option>
                                                @foreach($provincias as $provincia)
                                                <option value="{{$provincia->provincia_id}}"  {{old('provincia')==$provincia->provincia_id?'selected':''}}>{{$provincia->provincia}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 form-group pl-1">
                                        <div class="custom-select-form">
                                            <select class="browser-default custom-select" aria-label="Default select example" name="canton" id="canton" required>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="row no-gutters">
                                    <div class="col-6 form-group pr-1">
                                        <div class="custom-select-form">
                                            <select class="browser-default custom-select" aria-label="Default select example" name="parroquia" id="parroquia" required>


                                            </select>
                                        </div>
                                    </div>



                                        <div class="col-6 form-group pr-1">
                                            <input type="text" name="referencia" class="form-control" placeholder="Referencia" required>
                                        </div>







                                </div>



                                <!-- /row -->
                                <div class="form-group">

                                    <input type="text" name="direccion" class="form-control" value=" {{Auth::user()->city}}" required placeholder="Dirección">
                                </div>
                                <div class="row no-gutters">
                                    <div class="col-6 form-group pr-1">
                                        <input id="cedula" type="text" class="form-control @error('idcard') is-invalid @enderror" name="cedula_indentidad" value="{{Auth::user()->idcard}}" placeholder="C.I." required autocomplete="idcard" onblur="return check_cedula(this.form);">
                                    </div>
                                    <div class="col-6 form-group pl-1">
                                        <input type="text" name="telefono" class="form-control" placeholder="Telefono" value="{{ Auth::user()->phone }}" required>
                                    </div>
                                </div>


                                <hr>
                                <div class="form-group">
                                    <label class="container_check" id="other_addr">Cambiar datos de facturación
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div id="other_addr_c" class="pt-2">
                                    <div class="row no-gutters">
                                        <div class="col-6 form-group pr-1">
                                            <input type="text" name="factura_nombre" class="form-control" placeholder="Nombre" value="{{ Auth::user()->name }}" required>
                                        </div>
                                        <div class="col-6 form-group pl-1">
                                            <input type="text" name="factura_apellido" class="form-control" placeholder="Apellido" value="{{ Auth::user()->surname }}" required>
                                        </div>
                                    </div>
                                    <!-- /row -->
                                    <div class="form-group">
                                        <input name="factura_cédula" size="20" id="ruc" maxlength=13 type="text" class="form-control @error('idcard') is-invalid @enderror" value="{{Auth::user()->idcard}}" placeholder="Cédula/RUC/Pasaporte :" required autocomplete="idcard" onblur="
                                        if (validarDocumento())
                                     alert('EL NUMERO DE DOCUMENTO INGRESADO ES CORRECTO');
                                       else
                                        alert('NO SE PUEDE VALIDAR EL NUMERO DE DOCUMENTO');
                                          ">
                                    </div>

                                    <!-- /row -->
                                    <div class="form-group">
                                        <input type="text" name="factura_dirección" class="form-control" placeholder="Dirección" value="{{ Auth::user()->city }}" required>
                                    </div>


                                    <!-- /row -->
                                    <div class="form-group">
                                        <input type="text" name="factura_telefono" class="form-control" placeholder="Telefono" value="{{ Auth::user()->phone }}" required>
                                    </div>
                                </div>
                                <!-- /other_addr_c -->
                                <hr>

                            </div>
                            @endif

                            @if($bandera=="true")



                            <div class="tab-pane fade show active" id="tab_1" role="tabpanel" aria-labelledby="tab_1">


                                <hr>

                                <div class="row no-gutters">
                                    <div class="col-6 form-group pr-1">
                                        <input type="text" name="nombre" class="form-control" placeholder="Nombre" value="{{ $test['nombre']}}" required>
                                    </div>
                                    <div class="col-6 form-group pl-1">
                                        <input type="text" name="apellido" class="form-control" placeholder="Apellido" value="{{ $test['apellido']}}" required>
                                    </div>
                                </div>

                                <hr>
                                <div class="row no-gutters">
                                    <div class="col-6 form-group pr-1">
                                        <div class="custom-select-form">



                                            <select class="browser-default custom-select" aria-label="Default select example" name="provincia" id="provincia" required>
                                                @foreach($provincias as $provincia)
                                                @if($provincia->provincia_id==$test['provincia'])
                                                <option value="{{$provincia->provincia_id}}" selected  {{old('provincia')==$provincia->provincia_id?'selected':''}}>{{$provincia->provincia}}</option>
                                                @else
                                                <option value="{{$provincia->provincia_id}}"  {{old('provincia')==$provincia->provincia_id?'selected':''}}>{{$provincia->provincia}}</option>
                                                @endif

                                            @endforeach


                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 form-group pl-1">
                                        <div class="custom-select-form">
                                            @php
                                                $cantones=DB::table('cantones')
                                                ->where('canton_id',$test['canton'])
                                                ->where('provincia_id',Session::get('provincia_id'))
                                                ->get();
                                            @endphp
                                            <select class="browser-default custom-select" aria-label="Default select example" name="canton" id="canton" required>
                                                @foreach($cantones as $canton)
                                                @if($canton->canton_id==$test['canton'])
                                                <option value="{{$canton->canton_id}}" selected  {{old('canton')==$canton->canton_id?'selected':''}}>{{$canton->canton}}</option>

                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="row no-gutters">
                                    <div class="col-6 form-group pr-1">
                                        <div class="custom-select-form">
                                            <select class="browser-default custom-select" aria-label="Default select example" name="parroquia" id="parroquia" required>
                                                @foreach($parroquias as $parroquia)
                                                @if($parroquia->id==$test['parroquia'])
                                                <option value="{{$parroquia->id}}" selected  {{old('canton')==$parroquia->id?'selected':''}}>{{$parroquia->parroquia}}</option>

                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 form-group pr-1">
                                        <input type="text" name="referencia" class="form-control" value=" {{$test['referencia']}}" placeholder="Referencia" required>
                                    </div>
                                </div>
                                <!-- /row -->
                                <div class="form-group">

                                    <input type="text" name="direccion" class="form-control" value=" {{$test['direccion']}}" required placeholder="Dirección">
                                </div>
                                <div class="row no-gutters">
                                    <div class="col-6 form-group pr-1">
                                        <input id="cedula" type="text" class="form-control @error('idcard') is-invalid @enderror" name="cedula_indentidad" value=" {{$test['cedula_identidad']}}" placeholder="C.I." required autocomplete="idcard" onblur="return check_cedula(this.form);">
                                    </div>
                                    <div class="col-6 form-group pl-1">
                                        <input type="text" name="telefono" class="form-control" placeholder="Telefono" value="{{$test['telefono']}}" required>
                                    </div>
                                </div>


                                <hr>
                                <div class="form-group">
                                    <label class="container_check" id="other_addr">Cambiar datos de facturación
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div id="other_addr_c" class="pt-2">
                                    <div class="row no-gutters">
                                        <div class="col-6 form-group pr-1">
                                            <input type="text" name="factura_nombre" class="form-control" placeholder="Nombre" value=" {{$test['factura_nombre']}}" required>
                                        </div>
                                        <div class="col-6 form-group pl-1">
                                            <input type="text" name="factura_apellido" class="form-control" placeholder="Apellido" value="{{$test['factura_apellido']}}" required>
                                        </div>
                                    </div>
                                    <!-- /row -->
                                    <div class="form-group">
                                        <input name="factura_cédula" size="20" id="ruc" maxlength=13 type="text" class="form-control @error('idcard') is-invalid @enderror" value="{{$test['factura_cédula']}}" placeholder="Cédula/RUC/Pasaporte :" required autocomplete="idcard" onblur="
                                        if (validarDocumento())
                                     alert('EL NUMERO DE DOCUMENTO INGRESADO ES CORRECTO');
                                       else
                                        alert('NO SE PUEDE VALIDAR EL NUMERO DE DOCUMENTO');
                                          ">
                                    </div>

                                    <!-- /row -->
                                    <div class="form-group">
                                        <input type="text" name="factura_dirección" class="form-control" placeholder="Dirección" value="{{$test['factura_dirección']}}" required>
                                    </div>


                                    <!-- /row -->
                                    <div class="form-group">
                                        <input type="text" name="factura_telefono" class="form-control" placeholder="Telefono" value="{{$test['factura_telefono']}}" required>
                                    </div>
                                </div>
                                <!-- /other_addr_c -->
                                <hr>

                            </div>
                            @endif



                            <!-- /tab_1 -->

                            <!-- /tab_2 -->
                        </div>
                        <input class="btn_1 full-width" type="submit" value="Sigiente">
                    </form>
                </div>
                <!-- /step -->


            </div>


            <div class="col-lg-4 col-md-6 {{Session::get('proceso') ==2 ? '' : 'disablebutton'}}">

                <div class="step middle payments ">
                    <h3>2. Método de Envio y Pago</h3>
                    <p>Envio al Distrito Metropolitano de Quito recaargo de $3.50</p>
                </div>

                <h6 class="pb-2">Método de envío</h6>
                <form action="{{route('siguiente')}}" method="POST">
                    @csrf
                    <ul>

                        @if(Session::get('provincia_id')==17)
                        <li>
                            <label class="container_radio">Envío Local<a href="#0" class="info" data-toggle="modal" data-target="#payments_method"></a>
                                <input type="radio" id="local1" name="envio" value="1" required onclick="local();" {{ Session::has('envio') && Session::get('envio')=='1'? 'checked' : ''}}>
                                <span class="checkmark"></span>
                            </label>
                        </li>
                        @else
                        <li>
                            <label class="container_radio">Envío Provincias<a href="#0" class="info" data-toggle="modal" data-target="#payments_method"></a>
                                <input type="radio" id="provincia1" name="envio" value="2" required onclick="provincia();" {{ Session::has('envio') && Session::get('envio')=='2'? 'checked' : ''}}>
                                <span class="checkmark"></span>
                            </label>
                        </li>


                        @endif




                    </ul>


                    <div>
                        <figure><img src="/images/portada/logo1.png" alt="" width="300" height="100">
                        </figure>
                        <ul>
                            <li>
                                <label class="container_radio">Transferencia Bancaria / Depósito<a href="#0" class="info" data-toggle="modal" data-target="#payments_method"></a>
                                    <input type="radio" name="pago" value="2" required {{ Session::has('pago') && Session::get('pago')=='2'? 'checked' : ''}}>
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_radio">Tarjeta de Crédito / Débito<a href="#0" class="info" data-toggle="modal" data-target="#payments_method"></a>
                                    <input type="radio" name="pago" value="1" required {{ Session::has('pago') && Session::get('pago')=='1'? 'checked' : ''}}>
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>

                    </div>
                    <br>
                    <!-- /step -->


                    <input class="btn_1 full-width" type="submit" value="Siguiente">

                </form>

                <form action="{{route('atras')}}" method="POST">
                    @csrf
                    <input class="btn_1 full-width" type="submit" value="Vuelve al paso anterior">
                </form>



            </div>
            <div class="col-lg-4 col-md-6 {{Session::get('proceso') ==3 ? '' : 'disablebutton'}}">
                <div class="step last">
                    <form id="formPago" action="{{route('finalizar.compra')}}" method="POST">
                        @csrf
                        <h3>3. Resumen de la Orden</h3>
                        <div class="box_general summary">
                            <ul>
                                @forelse($cartCollection as $item)
                                <li class="clearfix"><em>{{ $item->quantity }} x {{ $item->name }}</em> <span>${{ $item->price*$item->quantity }}</span></li>
                                @empty
                                @endforelse
                            </ul>
                            <ul>

                                @if (Session::has('envio')==null)
                                <li class="clearfix"><em><strong>Transporte</strong></em> <span id="envio">$0.0</span></li>
                                @endif

                                <li class="clearfix"><em><strong>SubTotal</strong></em> <span>${{ \Cart::getSubTotal() }}</span></li>
                                @if(Session::has('envio') && Session::get('envio')=='1')
                                <li class="clearfix"><em><strong>Transporte</strong></em> <span id="envio">$3.50</span></li>
                                @endif
                                @if(Session::has('envio') && Session::get('envio')=='2')
                                <li class="clearfix"><em><strong>Transporte</strong></em> <span id="envio">$4.50</span></li>
                                @endif

                                <li class="clearfix"><em><strong>IVA</strong></em> <span id="envio">${{Session::get('IVA')}}</span></li>
                            </ul>

                            @if (Session::has('envio')==null)
                            <div class="total clearfix">TOTAL <span id="total"> </span></div>
                            @endif



                            @if (Session::has('envio')&& Session::get('envio')=='1')
                            <div class="total clearfix">TOTAL <span id="total">${{ number_format (Session::get('TOTAL')+3.50+Session::get('IVA'),2) }}</span></div>
                            @endif
                            @if (Session::has('envio')&& Session::get('envio')=='2')
                            <div class="total clearfix">TOTAL <span id="total">${{ number_format (Session::get('TOTAL')+4.50+Session::get('IVA'),2) }} </span></div>
                            @endif
                            <input type="hidden" id="totalhidden" name="totals" value="{{ $total }}">


                            <!--
                    <div class="form-group">
                            <label class="container_check">Register transferencia.
                              <input type="checkbox" checked>
                              <span class="checkmark"></span>
                            </label>
                        </div>
                    -->
                            <br>
                            @if (!Session::has('pago')||(Session::has('pago')&& Session::get('pago')=='2'))
                            <button type="submit" class="btn_1 full-width">Confirmar y pagar</button>
                            @else

                            <div id="paypal-button-container"></div>

                            @endif

                    </form>
                    <form action="{{route('atras2')}}" method="POST">
                        @csrf
                        <input class="btn_1 full-width" type="submit" name="atras" value="Vuelve al paso anterior">
                    </form>

                </div>
                <!-- /box_general -->
            </div>
            <!-- /step -->
        </div>
    </div>
    <!-- /row -->
</div>
<!-- /container -->


</div>


<!-- BASE CSS -->

<link href="{{ asset('css/checkout/css/style.css')}}" rel="stylesheet">

<!-- SPECIFIC CSS -->
<link href="{{ asset('css/checkout/css/checkout.css')}}" rel="stylesheet">
<script src="{{url('js/checkout/envio.js')}}"></script>
<script src="{{url('js/validarRuc/ruc.js')}}"></script>
<!-- YOUR CUSTOM CSS -->
<link href="{{ asset('css/checkout/css/custom.css')}}" rel="stylesheet">

<script src="{{url('js/jscedula/codigo.js')}}"></script>
<script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('js/detalle/product.js') }}"></script>



<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/aos.js') }}"></script>
<script src="{{ asset('js/scrollax.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
<link href="{{ asset('css/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
<!-- Link  factura -->


<script src="{{ asset('js/factura/factura.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/fiddle.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/forge.min.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/moment.min.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/buffer.js') }}"></script>

<script>
    // paypal.Buttons().render('#paypal-button-container');

    // Other address Panel
    $('#other_addr input').on("change", function() {
        if (this.checked)
            $('#other_addr_c').fadeIn('fast');
        else
            $('#other_addr_c').fadeOut('fast');
    });


    paypal.Buttons({

        style: {
            size: 'small'
            , color: 'blue'
            , shape: 'pill'
            , label: 'checkout'
        , }
        , createOrder: function(data, actions) {
            // This function sets up the details of the transaction, including the amount and line item details.
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        @if(Session::get('envio') == '1')
                        value: '{{ Cart::getTotal()+3.50+Session::get('IVA') }}'
                        @else
                        value: '{{ Cart::getTotal()+4.50+Session::get('IVA') }}'
                        @endif

                    }
                }]
            });
        }
        , onApprove: function(data, actions) {
            // This function captures the funds from the transaction.
            return actions.order.capture().then(function(details) {
                //  var path_host = window.location.origin;  //http://localhost:8000/
                //         alert(path_host + "/js/lib_firma_sri/src/leerFactura.php");ruta_certificado

                //  obtenerComprobanteFirmado_sri(correo = null, claveAcceso = null);


                //  alert(path_host + "/Factura/lib_firma_sri/src/leerFactura.php");

                $("#formPago").submit(); // llama controlador

                //  procesar();
            });
        }
    }).render('#paypal-button-container');
    //This function displays Smart Payment Buttons on your web page.

    const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content;
    document.getElementById('provincia').addEventListener('change',(e)=>{
        fetch('cantones',{
            method : 'POST',
            body: JSON.stringify({canton : e.target.value}),

            headers:{
                'Content-Type': 'application/json',
                "X-CSRF-Token": csrfToken
            }
        }).then(response =>{
            return response.json()
        }).then( data =>{
            var opciones ="<option value=''>Elegir</option>";
            for (let i in data.lista) {
               opciones+= '<option value="'+data.lista[i].canton_id+'">'+data.lista[i].canton+'</option>';
            }
            document.getElementById("canton").innerHTML = opciones;
        }).catch(error =>console.error(error));
    })


    document.getElementById('canton').addEventListener('change',(e)=>{
        fetch('parroquias',{
            method : 'POST',
            body: JSON.stringify({parroquia : e.target.value}),

            headers:{
                'Content-Type': 'application/json',
                "X-CSRF-Token": csrfToken
            }
        }).then(response =>{
            return response.json()
        }).then( data =>{
            var opciones ="<option value=''>Elegir</option>";
            for (let i in data.listap) {
               opciones+= '<option value="'+data.listap[i].id+'">'+data.listap[i].parroquia+'</option>';
            }
            document.getElementById("parroquia").innerHTML = opciones;
        }).catch(error =>console.error(error));
    })

</script>






@endsection
