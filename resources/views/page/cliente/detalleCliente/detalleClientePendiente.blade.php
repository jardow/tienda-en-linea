@extends('layouts.master.master')
@section('title', 'Cliente Tienda || Detalle Factura')

@section('content')
<div class="container" style="margin-top: 80px">

    <div class="row justify-content-center">


        <div class="super_container">
            @include('custom.message')
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Cliente</li>
                            <li class="breadcrumb-item active" aria-current="page">Validar Pago</li>
                            <li class="breadcrumb-item active" aria-current="page">Detalle Pre Factura</li>
                        </ol>
                    </nav>
                </div>
            </div>

            @foreach ($facturas as $factura )

        @endforeach


            <div class="panel invoice-list">
                <div class="list-group animate__animated animate__fadeInLeft">

                    <a href="#" class="list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-between">
                            @foreach ($detalle_facturas as $detalle_factura )

                            @endforeach
                            <h5 class="mb-1">Factura N° {{ $detalle_factura->factura_id }}</h5>
                            <small class="text-muted">Fecha: {{ $factura->fecha_emision }} </small>
                        </div>

                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"></h5>
                            <small class="text-muted">Serie: {{ $factura->serie }}</small>
                        </div>
                        <p class="amount mb-1">Nombre Cliente: {{$factura->name}} {{$factura->surname}}</p>
                        <p class="amount mb-1">Ruc / C.I.: {{ $factura->idcard }}</p>
                        <div class="text-muted">Dirección: {{ $factura->city }}</div>
                        <div class="text-muted">Telefono: {{ $factura->phone }}</div>
                        <div class="text-muted">Correo: {{ $factura->email }}</div>

                    </a>
                </div>
            </div>

            <div class="main">
                <div class="container mt-3">
                    <div class="card animate__animated animate__fadeIn">
                        <div class="card-header">
                            Clave Acceso:
                            <strong>{{ $factura->claveacceso }} </strong>
                            <span class="float-right"> <strong>Estado:</strong>PENDIENTE</span>

                        </div>
                        <div class="card-body">
                            <div class="row mb-4">


                                Detalle de compra

                            </div>

                            <div class="table-responsive-sm">
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col" width="2%" class="center">Cod Producto</th>
                                            <th scope="col" width="20%">Cantidad</th>
                                            <th scope="col" class="d-none d-sm-table-cell" width="50%">Descripción</th>

                                            <th scope="col" width="10%" class="text-right">P. Unidad</th>

                                            <th scope="col" width="10%" class="text-right">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($detalle_facturas as $detalle_factura )
                                        <tr>

                                            <td class="text-left">{{ $detalle_factura->codigo_producto }}</td>
                                            <td class="item_name"> {{ $detalle_factura->cantidad }}</td>
                                            <td class="item_desc d-none d-sm-table-cell">{{ $detalle_factura->descripcion }}</td>

                                            <td class="text-right">${{ $detalle_factura->preciou }}</td>

                                            <td class="text-right">${{ number_format($detalle_factura->preciou * $detalle_factura->cantidad ,2)}}</td>
                                        </tr>
                                        @endforeach


                                        @foreach ($metodos_envios as $metodos_envio )


                                        <tr>
                                            <td class="center">{{ $metodos_envio->codigo }}</td>
                                            <td class="item_name">1</td>
                                            <td class="item_desc d-none d-sm-table-cell">{{ $metodos_envio->descripcion }}</td>
                                            <td class="text-right">${{ $metodos_envio->precio }}</td>
                                            <td class="text-right">${{ $metodos_envio->precio }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-sm-5">
                                </div>

                                <div class="col-lg-4 col-sm-5 ml-auto">
                                    <table class="table table-sm table-clear">
                                        <tbody>
                                            @foreach ($facturas as $factura )

                                            @endforeach
                                            <tr>
                                                <td class="left">
                                                    <strong>Subtotal</strong>
                                                </td>
                                                <td class="text-right bg-light">${{number_format($factura->total + $metodos_envio->precio ,2)  }}</td>
                                            </tr>

                                            <tr>
                                                <td class="left">
                                                    <strong>IVA (12%)</strong>
                                                </td>
                                                <td class="text-right bg-light">${{$factura->iva}}</td>
                                            </tr>
                                            <tr>
                                                <td class="left">
                                                    <strong>Total</strong>
                                                </td>
                                                <td class="text-right bg-light">
                                                    <strong>${{ number_format($factura->total +$factura->iva+$metodos_envio->precio,2) }}</strong>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/aos.js') }}"></script>
    <script src="{{ asset('js/scrollax.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
    <script src="{{ asset('js/detalle/product.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
    <!--
-->

    <script>
        $('#colores').DataTable({
            responsive: true
            , autoWidth: false
            , dom: '<"#example_header"fB<"top"<"newline clear"><"newline clear">l>>rtip',




            "language": {
                // "lengthMenu": "Mostrar _MENU_ registros por página",
                "lengthMenu": "Mostrar " +
                    `<select>
          <option value='3'>3</option>
          <option value='25'>25</option>
          <option value='50'>50</option>
          <option value='100'>100</option>
          <option value='-1'>all</option>
          </select> ` +
                    "registros por página"
                , "zeroRecords": "No se encontró nada,  - lo siento"
                , "info": "Mostrando página _PAGE_ de _PAGES_"
                , "infoEmpty": "No hay registros disponibles"
                , "infoFiltered": "(filtrado de _MAX_ registros totales)"
                , 'search': 'Buscar:'
                , 'paginate': {
                    'next': 'Siguiente'
                    , 'previous': 'Anterior'



                }
            }
        });

    </script>
    @endsection
