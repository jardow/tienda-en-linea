@extends('layouts.master.master')
@section('title', 'Cliente Tienda || Validar Pago')

@section('content')
<div class="container" style="margin-top: 80px">

    <div class="row justify-content-center">


        <div class="super_container">
            @include('custom.message')
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Cliente</li>
                            <li class="breadcrumb-item active" aria-current="page">Procesar Pago</li>
                        </ol>
                    </nav>
                </div>
            </div>


            <table  class="table table-striped" id="colores">
                <thead>
                    <tr>
                        <th scope="col">Cod Factura</th>
                        <th scope="col">Fecha Compra</th>
                        <th scope="col">Detalle Compra</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Comprobante</th>
                        <th scope="col">Procesar Pago</th>
                        <th scope="col">Obeservación</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($facturas as $factura )

                    <tr>
                        <form action="{{route('validarPagoCliente.update',$factura->validarPagos_id)}}" method="POST" enctype="multipart/form-data">
                            @method('Put')
                            @csrf
                        <td scope="row">{{ $factura->id }}</td>
                        <td>{{  \Carbon\Carbon::parse(strtotime($factura->fecha_emision))->formatLocalized('%d %B %Y') }}</td>
                        <td> <a class="btn btn-outline-info"  href="{{ url('cliente/compras/factura/detalle/pendiente',Crypt::encrypt($factura->id))}}">DETALLE FACTURA</a></td>

                        @if ($factura->estado ==1)
                        <td>PAGADO</td>

                        @elseif ($factura->estado ==2)
                        <td>Pendiente</td>

                        @elseif ($factura->estado ==3)
                        <td>Proceso de Verificación</td>
                        @endif


                        @if ($factura->estado ==1)
                        <td>/ / / / / /</td>
                        @elseif ($factura->estado ==2)
                        <td><input class="form-control"  accept="image/png,image/jpeg,image/jpg" type="file" name="voucher" id="voucher" ></td>
                        @elseif ($factura->estado ==3)
                        <td>/ / / / / /</td>
                        @endif


                        @if ($factura->estado ==1)
                        <td><input class="btn btn-primary" type="submit" value="Procesar Pago" disabled></td>
                        @elseif($factura->estado ==3)
                        <td><input class="btn btn-primary" type="submit" value="Procesar Pago" disabled></td>
                        @elseif ($factura->estado ==2)
                        <td><input class="btn btn-primary" type="submit" value="Procesar Pago"></td>

                        @endif
                            @if($factura->fecha_pago=="1988-02-29 00:00:00")
                                <td>Comprobante Invalido / No se procede a validar el pago Adjuntar comprobante un valido</td>

                                @else

                            <th>

                                @if ($factura->estado ==1)
                               <h6>Pago realizado con éxito</h6>
                                @elseif($factura->estado ==3)
                                <h6>Esperando validación de pago</h6>
                                @endif

                            </th>

                            @endif



                    </form>
                    </tr>

                    @endforeach

                </tbody>
               </table>


        </div>
    </div>

<!--


    -->

            <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
            <link href="{{ asset('css/owl.carousel.min.css')}}" rel="stylesheet">
            <link href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
            <script src="{{ asset('js/jquery.min.js') }}"></script>
            <script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
            <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
            <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
            <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
            <script src="{{ asset('js/aos.js') }}"></script>
            <script src="{{ asset('js/scrollax.min.js') }}"></script>
            <script src="{{ asset('js/main.js') }}"></script>
            <script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
            <script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
            <script src="{{ asset('js/detalle/product.js') }}"></script>
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
            <!--
-->

<script>
    $('#colores').DataTable({
        responsive:true,
        autoWidth: false,
        dom: '<"#example_header"fB<"top"<"newline clear"><"newline clear">l>>rtip',




        "language": {
          // "lengthMenu": "Mostrar _MENU_ registros por página",
          "lengthMenu": "Mostrar "+
          `<select>
          <option value='3'>3</option>
          <option value='25'>25</option>
          <option value='50'>50</option>
          <option value='100'>100</option>
          <option value='-1'>all</option>
          </select> `  +
          "registros por página",
           "zeroRecords": "No se encontró nada,  - lo siento",
           "info": "Mostrando página _PAGE_ de _PAGES_",
           "infoEmpty": "No hay registros disponibles",
           "infoFiltered": "(filtrado de _MAX_ registros totales)",
           'search':'Buscar:',
           'paginate':{
               'next':'Siguiente',
               'previous':'Anterior'



           }
       }
    });
</script>
@endsection
