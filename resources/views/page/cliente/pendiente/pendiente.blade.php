@extends('layouts.master.master')
@section('title', 'Cliente Tienda ||Entregas Pendientes')

@section('content')
<div class="container" style="margin-top: 80px">

    <div class="row justify-content-center">
        <div class="super_container">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Cliente</li>
                            <li class="breadcrumb-item active" aria-current="page">Pedidos Pendientes</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <table  class="table table-striped" id="colores">
                <thead>
                    <tr>
                        <th scope="col">Cod Envío</th>
                        <th scope="col">Detalle Factura</th>
                        <th scope="col">Modalidad de Pago</th>
                        <th scope="col">Método de Envío</th>
                        <th scope="col">Fecha de Compra</th>
                        <th scope="col">Estado</th>

                    </tr>
                </thead>
                <tbody>



                    @foreach ($pendientes as $item )



                    <tr>
                        <td scope="row">{{ $item->id }}</td>
                        @if($item->estado_compra_id==1)
                        <td> <a class="btn btn-info"  href="{{ url('cliente/compras/factura/detalle',Crypt::encrypt( $item->factura_id))}}">DETALLE FACTURA</a></td>
                        @else
                        <td> <a class="btn btn-outline-info"  href="{{ url('cliente/compras/factura/detalle/pendiente',Crypt::encrypt($item->factura_id))}}">DETALLE FACTURA</a></td>
                        @endif



                        @if($item->modalidad_pago_id ==1)
                        <td>Tarjeta de Crédito / Débito</td>
                        @else
                        <td>Transferencia / Depósito</td>
                        @endif

                        @if($item->metodo_envio_id ==1)
                        <td>Envío Local</td>
                        @else
                        <td>Envío a Provincias</td>
                        @endif

                        <td>{{  \Carbon\Carbon::parse(strtotime($item->fecha_emision))->formatLocalized('%d %B %Y') }}</td>

                        <th>PENDIENTE</th>





                        </tr>
                        @endforeach

                </tbody>
               </table>


        </div>
    </div>

<!--


    -->

            <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
            <link href="{{ asset('css/owl.carousel.min.css')}}" rel="stylesheet">
            <link href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
            <script src="{{ asset('js/jquery.min.js') }}"></script>
            <script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
            <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
            <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
            <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
            <script src="{{ asset('js/aos.js') }}"></script>
            <script src="{{ asset('js/scrollax.min.js') }}"></script>
            <script src="{{ asset('js/main.js') }}"></script>
            <script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
            <script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
            <script src="{{ asset('js/detalle/product.js') }}"></script>
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
            <!--
-->

<script>
    $('#colores').DataTable({
        responsive:true,
        autoWidth: false,
        dom: '<"#example_header"fB<"top"<"newline clear"><"newline clear">l>>rtip',




        "language": {
          // "lengthMenu": "Mostrar _MENU_ registros por página",
          "lengthMenu": "Mostrar "+
          `<select>
          <option value='3'>3</option>
          <option value='25'>25</option>
          <option value='50'>50</option>
          <option value='100'>100</option>
          <option value='-1'>all</option>
          </select> `  +
          "registros por página",
           "zeroRecords": "No se encontró nada,  - lo siento",
           "info": "Mostrando página _PAGE_ de _PAGES_",
           "infoEmpty": "No hay registros disponibles",
           "infoFiltered": "(filtrado de _MAX_ registros totales)",
           'search':'Buscar:',
           'paginate':{
               'next':'Siguiente',
               'previous':'Anterior'



           }
       }
    });
</script>
@endsection
