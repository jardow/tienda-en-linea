@extends('layouts.master.master')
@section('title', 'Cliente Tienda || Contacto')

@section('content')
<div class="container" style="margin-top: 80px">

    <div class="row justify-content-center">

        <div class="super_container">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Envio</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <!-- Termina la definición del menú -->
            <main role="main" class="container">
                @include('custom.message')
                <div class="row">
                    <!-- Aquí pon las col-x necesarias, comienza tu contenido, etcétera -->

                    <div class="widget gva-contact-info ">
                        <center>

                        <h3 class="widget-title">
                            <span>QUITO</span>
                        </h3>
                        <div class="widget-content">
                            <div class="content">
                                <div class="address"><i class="fas fa-map-marker-alt"></i></i>Dirección: Calle J N24-118 y Calle I ().</div>

                                <div class="phone"><i class="fas fa-phone-volume"></i>Atención y servicio al cliente: (09) 939230614 </div>

                                <div class="email"><i class="fas fa-envelope"></i>Email: info@storevalentina.com</div>


                            </div>
                        </div>
                    </center>
                    </div>

<br>
                    <div class="col-12">
                        <center>
                        <h5>Será un placer para nosotros despejar sus dudas</h5>
                        <h5>y atender sus requerimientos.</h5>
                        <a href="//www.storevalentina.com" target="_blank">STORE VALENTINA</a>
                    </center>
                    </div>

                    <div class="col-12">
                        <form action="{{ url('cliente/contactanos/envio')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="fas fa-user" for="nombre">Nombre</label>
                                <input  name="nombre" required type="text" id="nombre" class="form-control" placeholder="Tu nombre">
                            </div>
                            <div class="form-group">
                                <label class="fas fa-envelope" for="correo">Correo electrónico</label>
                                <input name="correo" required type="email" id="correo" class="form-control" placeholder="Tu correo electrónico">
                            </div>

                            <div class="form-group">
                                <label class="fas fa-phone" for="telefono">Telefono</label>
                                <input name="telefono" required type="text" id="telefono" class="form-control" placeholder="Tu numéro Teléfonico">
                            </div>

                            <div class="form-group">
                                <label class="fal fa-envelope-open-text" for="mensaje">Mensaje</label>
                                <textarea required placeholder="Escribe tu mensaje" class="form-control" name="mensaje" id="mensaje" cols="30" rows="5"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn-success btn" type="submit">
                                    Enviar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </main>

        </div>
    </div>

    <!--


    -->

    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/aos.js') }}"></script>
    <script src="{{ asset('js/scrollax.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
    <script src="{{ asset('js/detalle/product.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
    <!--
-->

    <script>
        $('#colores').DataTable({
            responsive: true
            , autoWidth: false
            , dom: '<"#example_header"fB<"top"<"newline clear"><"newline clear">l>>rtip',




            "language": {
                // "lengthMenu": "Mostrar _MENU_ registros por página",
                "lengthMenu": "Mostrar " +
                    `<select>
          <option value='3'>3</option>
          <option value='25'>25</option>
          <option value='50'>50</option>
          <option value='100'>100</option>
          <option value='-1'>all</option>
          </select> ` +
                    "registros por página"
                , "zeroRecords": "No se encontró nada,  - lo siento"
                , "info": "Mostrando página _PAGE_ de _PAGES_"
                , "infoEmpty": "No hay registros disponibles"
                , "infoFiltered": "(filtrado de _MAX_ registros totales)"
                , 'search': 'Buscar:'
                , 'paginate': {
                    'next': 'Siguiente'
                    , 'previous': 'Anterior'



                }
            }
        });

    </script>
    @endsection
