@extends('layouts.master.master')
@section('title', 'Cliente Tienda || Inicio')
@section('content')
<div class="container" style="margin-top: 80px">

    <div class="row justify-content-center">
<div class="super_container">
    <div class="row">
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Catalogo</li>
                    <li class="breadcrumb-item active" aria-current="page">Detalle</li>
                </ol>
            </nav>
        </div>
    </div>
	<div class="super_container_inner">
		<div class="super_overlay"></div>
		<div class="product">
			<div class="container">
				<div class="row">
					<!-- Product Image -->
					<div class="col-lg-6">
						<div class="product_image_slider_container">
                            <!--
							<span class="badge-new"><b> Nuevo</b></span>
							<span class="badge-offer"><b> - 50%</b></span>
                            -->




							<div id="slider" class="flexslider">
								<ul class="slides">
                                    @foreach ($imagenes as $img )

                                    @if ($img->url!="null")
                                        <li>
                                            <img src="{{$img->url}}" />
                                        </li>
                                    @endif


                                    @endforeach

								</ul>
							</div>

							<div class="carousel_container">
								<div id="carousel" class="flexslider">
									<ul class="slides">
                                        @foreach ($imagenes as $img )
                                        @if ($img->url!="null")
										<li>
											<div><img src="{{$img->url}}" /></div>
										</li>
                                        @endif
                                        @endforeach

									</ul>
								</div>
								<div class="fs_prev fs_nav disabled"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>
								<div class="fs_next fs_nav"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
							</div>

						</div>
					</div>

					<!-- Product Info -->

                    @foreach ($producto as $pro )


					<div class="col-lg-6 product_col">
						<div class="product_info">
							<div class="product_name">{{$pro->nombre}}</div>
                            <hr>
                            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><p> COD : {{$pro->codigo_producto}}</p></div>
							<div class="product_category"><a href="category.html">Categoria:{{$pro->categoria}}</a></div>
                            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><p> Sección:{{$pro->seccion}}</p></div>
							<div class="product_price">{{$pro->precio}}<span>.99</span>
                                <!--
                            <del class="price-old"> $1980.00</del>
                            -->
                            </div>

							<div class="product_text">
								<p>{{$pro->descripcion}}</p>
							</div>

                            <hr>

                            @if ($pro->color!='No definido')
                            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><p> Color: {{$pro->color}}</p></div>
                            <hr>
                            @endif
                            @if ($pro->talla!='0')
                            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item"><p>Talla : {{$pro->talla}}</p></div>
                            <hr>
                            @endif



                            <form action="{{ route('cart.store') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" value="{{$pro->id}}" id="id" name="id">
                                <input type="hidden" value="{{$pro->nombre}}" id="name" name="name">
                                <input type="hidden" value="{{$pro->precio}}" id="price" name="price">
                                <input type="hidden" value="{{$pro->imagen}}" id="img" name="img">
                                <input type="hidden" value="{{$pro->stock}}" id="stock" name="stock">


                                <div class="product_text">
                                    <span style="color: black; font-weight: bold;">Cantidad</span>
                                    <input class="form-number form-control" type="number" id="quantity" name="quantity" value="1" step="1" min="0" placeholder="">
                                </div>

                                <!--
                                <input type="hidden" value="1" id="quantity" name="quantity">
                                -->
                                <div class="card-footer" style="background-color: white;">
                                      <div class="row">
                                        <button class="btn btn-secondary btn-sm" class="tooltip-test" title="añadir">
                                            <i class="fa fa-shopping-cart"></i> añadir al carrito
                                        </button>
                                    </div>
                                </div>
                            </form>
                            @endforeach

						</div>
					</div>
				</div>
			</div>
		</div>


<!--
	tab info
-->
<div class="tabinfo">
<div class="container">


	<div class="tab-content">
		<div class="tab-pane fade show active" id="descripcion">

						<h3>Descripción</h3>
						<p>
							</p>
                            <div class="field field--name-body field--type-text-with-summary field--label-hidden field--item">
                <p>varios colores</p>
				<p>En stock.<br><br>

                    {{$pro->descripcion}}
                </p></div>
				<p></p>
		</div>
	</div>
</div>
</div>
</div>
</div>

    </div>
</div>

<link  href="{{ asset('css/animate.css')}}" rel="stylesheet">
<link  href="{{ asset('css/owl.carousel.min.css')}}"  rel="stylesheet">
<link  href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/aos.js') }}"></script>
<script src="{{ asset('js/scrollax.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('js/detalle/product.js') }}"></script>
<!--
-->
@endsection
