@extends('layouts.master.master')
@section('title', 'Perfil Cliente || Inicio')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Perfil Cliente</div>
                @include('custom.message')

                <div class="container rounded bg-white mt-5 mb-5">
                    <div class="row">
                        <div class="col-md-5 border-right">
                            @foreach ($users as $user  )
                            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                @if ($user->avatar=="null")
                                <img class="rounded-circle mt-5" width="150px"
                                src="\images\perfil\no.jpg">
                                @else
                                <img class="rounded-circle mt-5" width="150px"
                                src="{{$user->avatar}}">
                                @endif
                                <span class="font-weight-bold">{{$user->name}}</span>
                                <span class="text-black-50">{{$user->email}}</span>
                                <div class="mt-5 text-center">
                                    <center>
                                        <a class="btn btn-success"  href="{{ route('cliente-perfil.edit',Crypt::encrypt($user->id))}}">Editar</a>
                                        </center>
                                </div>
                                <span> </span></div>

                        </div>
                        <div class="col-md-7 border-right">
                            <div class="p-3 py-5">
                                <div class="d-flex justify-content-between align-items-center mb-3">
                                    <h4 class="text-right">Configuración de perfil</h4>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-6"><label class="labels">Nombres</label><input type="text" class="form-control" placeholder="Nombres" value="{{$user->name}}" readonly></div>
                                    <div class="col-md-6"><label class="labels">Apellidos</label><input type="text" class="form-control" value="" placeholder="{{$user->surname}}" readonly></div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12"><label class="labels">Correo Electrónico</label><input type="text" class="form-control" placeholder="" value="{{$user->email}}" readonly></div>
                                    <div class="col-md-12"><label class="labels">Cédula de Identidad</label><input type="text" class="form-control" placeholder="" value="{{$user->idcard}}" readonly></div>
                                    <div class="col-md-12"><label class="labels">Telefono</label><input type="text" class="form-control" placeholder="Telefono" value="{{$user->phone}}" readonly></div>

                                    <div class="col-md-12"><label class="labels">Dirección</label><input type="text" class="form-control" placeholder="Dirección" value="{{$user->city}}" readonly></div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                </div>
                </div>


                @endforeach
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/aos.js') }}"></script>
<script src="{{ asset('js/scrollax.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<link  href="{{ asset('css/animate.css')}}" rel="stylesheet">
<link  href="{{ asset('css/owl.carousel.min.css')}}"  rel="stylesheet">
<link  href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
@endsection
