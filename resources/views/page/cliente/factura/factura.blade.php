@extends('layouts.master.master')
@section('title', 'Cliente Tienda || Facturas')

@section('content')
<div class="container" style="margin-top: 80px">

    <div class="row justify-content-center">
        <div class="super_container">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Cliente</li>
                            <li class="breadcrumb-item active" aria-current="page">Facturas</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <table  class="table table-striped" id="colores">
                <thead>
                    <tr>
                        <th scope="col">N° Factura</th>
                        <th scope="col">Fecha Compra</th>
                        <th scope="col">PDF</th>
                        <th scope="col">XML</th>


                    </tr>
                </thead>
                <tbody>
                    @foreach ($rutas as $ruta )


                    <tr>
                        <td scope="row">{{ $ruta->serie }}</td>
                        <td>{{   \Carbon\Carbon::parse(strtotime( $ruta->fecha_emision))->formatLocalized('%d %B %Y') }}</td>
                        <td><a class="btn btn-outline-danger"  href="{{ url('cliente/compras/factura/facturapdf',Crypt::encrypt($ruta->factura_id))}}">PDF</a></td>
                        <td><a class="btn btn-outline-primary" href="{{ url('cliente/compras/factura/facturaxml',Crypt::encrypt($ruta->factura_id))}}">XML</a></td>

                        </tr>
                        @endforeach
                </tbody>
               </table>


        </div>
    </div>

<!--


    -->

    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">


            <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
            <link href="{{ asset('css/owl.carousel.min.css')}}" rel="stylesheet">
            <link href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
            <script src="{{ asset('js/jquery.min.js') }}"></script>
            <script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
            <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
            <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
            <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
            <script src="{{ asset('js/aos.js') }}"></script>
            <script src="{{ asset('js/scrollax.min.js') }}"></script>
            <script src="{{ asset('js/main.js') }}"></script>
            <script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
            <script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
            <script src="{{ asset('js/detalle/product.js') }}"></script>
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
            <!--
-->

<script>
    $('#colores').DataTable({
        responsive:true,
        autoWidth: false,
        dom: '<"#example_header"fB<"top"<"newline clear"><"newline clear">l>>rtip',




        "language": {
          // "lengthMenu": "Mostrar _MENU_ registros por página",
          "lengthMenu": "Mostrar "+
          `<select>
          <option value='3'>3</option>
          <option value='25'>25</option>
          <option value='50'>50</option>
          <option value='100'>100</option>
          <option value='-1'>all</option>
          </select> `  +
          "registros por página",
           "zeroRecords": "No se encontró nada,  - lo siento",
           "info": "Mostrando página _PAGE_ de _PAGES_",
           "infoEmpty": "No hay registros disponibles",
           "infoFiltered": "(filtrado de _MAX_ registros totales)",
           'search':'Buscar:',
           'paginate':{
               'next':'Siguiente',
               'previous':'Anterior'



           }
       }
    });
</script>
@endsection
