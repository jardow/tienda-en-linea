@extends('layouts.master.master')
@section('title', 'Cliente Tienda || Inicio')
@section('content')

<div class="container" style="margin-top: 80px">
    <!--
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Shop</li>
        </ol>
    </nav>
-->
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Tienda</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Catálogo</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
                <h4>    {{ $products->links('pagination::bootstrap-4') }}</h4>
                <form action="{{ url('cliente/buscar')}}" method="POST">
                    @csrf
                    {{csrf_field()}}
                <span>Filtrar Categoría</span>


                <div class="col-6 form-group pr-1">

                        <select   class="browser-default custom-select"  name="categoria" id="categoria" onchange="combo();">
                            <option value='0'>Selecione un opción</option>
                            @foreach($categorias as $categoria)
                                <option value="{{$categoria->id}}"  {{old('categoria')==$categoria->id?'selected':''}}>{{$categoria->descripcion}}</option>
                            @endforeach
                          </select>

                    <div id="boton" style="display: none">
                        <input class="btn btn-primary" type="submit" value="Buscar"\>
                     </div>
                </div>


             </form>
                          <br></br>

                          <!--
                <div id="hide-me" style="display: none">
                <span>Filtrar   Sección </span>
                <select name="seccion" id="seccion" onchange="combo();">
                    <option value='0'>Selecione un opción</option>
                    @foreach($seccions as $seccion)
                        <option value="{{$seccion->id}}"  {{old('seccion')==$seccion->id?'selected':''}}>{{$seccion->descripcion}}</option>
                    @endforeach
                  </select>
                </div>
            -->


            <hr>
            <div class="row">
                @foreach($products as $pro)
                    <div class="col-lg-3">
                        <div class="card" style="margin-bottom: 20px; height: auto;">

                            <img src="{{$pro->url}}"
                                 class="card-img-top mx-auto"
                                 style="height: 150px; width: 150px;display: block;"
                                 alt="{{ $pro->url }}"
                            >
                            <div class="card-body">

                                    <a href="{{ route('detalle.edit',Crypt::encrypt($pro->id))}}"><h6 class="card-title">{{ $pro->articulo }}</h6></a>

                                    <p align="left"> </p>
                                    <p align="left">${{$pro->precio}}</p>

                                <form action="{{ route('cart.store') }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $pro->id }}" id="id" name="id">
                                    <input type="hidden" value="{{ $pro->articulo }}" id="name" name="name">
                                    <input type="hidden" value="{{ $pro->precio }}" id="price" name="price">
                                    <input type="hidden" value="{{ $pro->url }}" id="img" name="img">
                                    <input type="hidden" value="{{ $pro->stock }}" id="stock" name="stock">
                                    <input type="hidden" value="1" id="quantity" name="quantity">
                                    <div class="card-footer" style="background-color: white;">
                                          <div class="row">
                                            <button class="btn btn-secondary btn-sm" class="tooltip-test" title="añadir">
                                                <i class="fa fa-shopping-cart"></i> añadir al carrito
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>

        </div>
    </div>
</div>
<script src="{{url('js/catalogo/catalogo.js')}}"></script>
<script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('js/detalle/product.js') }}"></script>



<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/aos.js') }}"></script>
<script src="{{ asset('js/scrollax.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<link  href="{{ asset('css/animate.css')}}" rel="stylesheet">
<link  href="{{ asset('css/owl.carousel.min.css')}}"  rel="stylesheet">
<link  href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">

@endsection






