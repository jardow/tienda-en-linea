@extends('layouts.master.master')
@section('title', 'Cliente Tienda || Finalizar')
@section('content')
<div class="container" style="margin-top: 80px">
    <div class="row">
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Cliente</li>
                    <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                    <li class="breadcrumb-item active" aria-current="page">Finalizar Compra</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container margin_30">
        @include('custom.message')
        <!-- /page_header -->

        <div class="col-lg-7">


            @php

            $datos_factura=DB::table('datos_facturacion_electronicas')
            ->select('datos_facturacion_electronicas.ubicacionarchivop12',
            'datos_facturacion_electronicas.contrasena')
            ->get();

            foreach ($datos_factura as $item) {
            }
            //Consulta Base
            $ruta_certificado_p12=public_path($item->ubicacionarchivop12);
            $contrasena=$item->contrasena;

            //Host mail
            $host_email="mail.storevalentina.com";
            $email="info@storevalentina.com";
            $passEmail="jardow8829";
            $portEmail="465";
            //Parametros de Conexion BD
            $host_bd=config('database.connections.mysql.host');
            $pass_bd=config('database.connections.mysql.password');
            $user_bd=config('database.connections.mysql.username');
            $database_bd=config('database.connections.mysql.database');
            $port_bd=config('database.connections.mysql.port');

            $estadoCompra=DB::table('facturas')->where('id', Session::get('factura_id'))->get();

            foreach($estadoCompra as $estado){
            $est=$estado->estado_compra_id;
            }

        //    return $est;

            if($est==1){
            $rutas=DB::table('rutas')
            ->join('facturas', 'rutas.factura_id', '=', 'facturas.id')
            ->where('rutas.factura_id', Session::get('factura_id'))
            ->select('facturas.claveacceso',
            'rutas.rutagenerado')
            ->get();
            //C:\xampp\htdocs\Test\tesis-tienda-virtual\ProyectoTesis\public\Factura\No_Firmado\.xml

            foreach($rutas as $ruta){

            }
            $claveA=$ruta->claveacceso;
            $ruta_xml=Storage::disk('local')->get($ruta->rutagenerado);

            // $ruta_xml=$ruta->rutagenerado;

            $factura_id=Session::get('factura_id');
            }



/*

            $output=['success'=>1,
            'claveAcceso'=>$claveA, ///
            'factura'=>$ruta_xml, ///
            'ruta_certificado'=>$ruta_certificado_p12,
            'pass_certificado'=>$contrasena,
            'ruta_respuesta'=>'h',
            'host_email'=>$host_email,
            'email'=>$email,
            'passEmail'=>$passEmail,
            'portEmail'=>$portEmail,
            'host_bd'=> $host_bd,
            'pass_bd'=>$pass_bd,
            'user_bd'=>$user_bd,
            'database'=>$database_bd,
            'port_bd'=>$port_bd,
            'factura_id'=>Session::get('factura_id'),
            'email_cliente'=>Auth::user()->email,
            'correo'=>'bacoth@gmail.com',
            ];
            */
            @endphp



            @php
            $factura_id=Session::get('factura_id');
            $relacion_ids=DB::table('detalle_facturas')
            ->where('factura_id', $factura_id)
            ->select('detalle_facturas.cantidad',
            'articulos.descripcion',
            'detalle_facturas.total',
            'detalle_facturas.preciou',
            'productos.codigo_producto')
            ->join('productos', 'detalle_facturas.producto_id', '=', 'productos.id')
            ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
            ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
            ->get();

            $factura_id=Session::get('factura_id');
        $relacions=DB::table('facturas')
        ->where('id', $factura_id)
        ->get();

        foreach($relacions as $ses){

        }
            @endphp



            @if ($ses->modalidad_pago_id==2)
            <center>
                <h3>Depósito o Transferencia</h3>
            </center>
            <center>
                <spam>Banco Pichincha</spam><br>
            </center>
            <center>
                <spam>Cuenta de Ahhoros </spam><br>
            </center>
            <center>
                <spam>Store Valentina</spam><br>
            </center>
            <center>
                <spam>N°: 2202995864</spam>
            </center>
            <center>
                <h3>¡Compra Exitosa!</h3>
            </center>
            @endif



           <center><h3>Gracias por su Compra</h3></center>

            @if (!$ses->modalidad_pago_id||($ses->modalidad_pago_id&& $ses->modalidad_pago_id=='2'))
            <a class="btn_1 full-width"  href="{{ url('/')}}">Finalizar</a>

            @else

            @php
                $rutas=DB::table('rutas')->where('factura_id', Session::get('factura_id'))->get();
                foreach ($rutas as $r) {
                    # code...
                }
            @endphp
            @if($r->estado=='AUTORIZADO')
            <div class="text-center">
                <img src="https://srienlinea.sri.gob.ec/recursos-sri-en-linea/imagenes/consultas/facturacion-electronica-carrusel.png" class="rounded" alt="..." width="250">
              </div>
            <center> <button autofocus class="btn btn-danger" id="fin" style="width: 450px" disabled>COMPROBANTE ENVIADO AL CORREO ELECTRONICO</button></center>
            @else
            <center> <button autofocus class="btn btn-outline-primary" id="fin" style="width: 350px" onclick="prueba();">GENERAR FACTURACIÖN ELECTRONICA </button></center>
            @endif


            @endif



            @if($est==1)
            <input type="hidden" value="{{ $ruta_xml }}" id="xml"></input>
            @endif

            <table class="table table-striped" align="center">
                <thead>
                    <tr>
                        <th>Cod Producto</th>
                        <th>Articulo</th>
                        <th>Cantidad</th>
                        <th>Precio Unitario</th>
                        <th>Sub Total</th>
                    </tr>
                </thead>


                <tbody>
                    @foreach ($relacion_ids as $item )
                    <tr>
                        <td>{{ $item->codigo_producto }}</td>
                        <td>{{ $item->descripcion }}</td>
                        <td>{{ $item->cantidad }}</td>
                        <td>{{ $item->preciou }}</td>
                        <td>{{ $item->total }}</td>
                        @endforeach
                    </tr>

                    <tr>
                        @php
                        $metodos_envios=DB::table('metodo_envios')
                        ->where('id',$ses->metodo_envio_id)
                        ->get();;
                        @endphp
                        @foreach ($metodos_envios as $envio )


                        <td>{{ $envio->codigo }}</td>
                        <td>{{ $envio->descripcion }}</td>
                        <td>1</td>
                        <td>{{ $envio->precio }}</td>
                        <td>{{ $envio->precio }}</td>
                        @endforeach
                    </tr>
                    <td>Método de Pago</td>
                    @if ($ses->modalidad_pago_id==2)
                    <td>Transferencia / Depósito</td>
                    @endif
                    @if ($ses->modalidad_pago_id==1)
                    <td>Tarjeta de Crédito / Débito</td>
                    @endif
                    <tr>
                     <th>IVA</th>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td>{{ $ses->iva }}</td>
                    </tr>
                    <tr>

                    </tr>

                    <tr>

                        <th colspan="4">Total</th>
                        @if ($ses->metodo_envio_id==1)
                        <td colspan="4">$ {{ number_format($ses->total+3.50+$ses->iva ,2)}}</td>
                        @endif
                        @if ($ses->metodo_envio_id==2)
                        <td colspan="4">$ {{ number_format($ses->total+4.50+$ses->iva ,2)}}</td>
                        @endif

                    </tr>

                    <tr>
                        @if($est==1)
                        <th><input id="claveA" type="hidden" value="{{ $claveA }}"></th>
                        @endif

                    </tr>
                </tbody>
            </table>

            <!--
            <center><a style="width: 300px" class="btn_1 full-width" href="{{ url('/') }}">GENERAR FACTURACIÖN ELECTRONICA </a></center>
-->

        </div>


    </div>
</div>
<!-- /container -->
</div>

<!-- BASE CSS -->

<link href="{{ asset('css/checkout/css/style.css')}}" rel="stylesheet">

<!-- SPECIFIC CSS -->
<link href="{{ asset('css/checkout/css/checkout.css')}}" rel="stylesheet">
<script src="{{url('js/checkout/envio.js')}}"></script>
<script src="{{url('js/validarRuc/ruc.js')}}"></script>
<!-- YOUR CUSTOM CSS -->
<link href="{{ asset('css/checkout/css/custom.css')}}" rel="stylesheet">
<script src="{{url('js/jscedula/codigo.js')}}"></script>
<script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('js/detalle/product.js') }}"></script>



<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/aos.js') }}"></script>
<script src="{{ asset('js/scrollax.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
<link href="{{ asset('css/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">


<script src="{{ asset('js/factura/factura.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/fiddle.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/forge.min.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/moment.min.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/buffer.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/uft8.js') }}"></script>




<script>
    function prueba() {

   const btncompra = document.getElementById('fin');
   btncompra.disabled = true;

        var xml = $("#xml").val()
        // var ruta_certificado = 'https://storevalentina.com/Factura/lib_firma_sri/firma/nelly_maria_cuenca_macas.p12';
        var ruta_certificado = 'http://localhost:8000/Factura/lib_firma_sri/firma/nelly_maria_cuenca_macas.p12';
        var ruta_factura = xml;
        var pwd_p12 = '{{$contrasena}}';
//  var ruta_respuesta = 'https://storevalentina.com/Factura/lib_firma_sri/example.php';
        var ruta_respuesta = 'http://localhost:8000/Factura/lib_firma_sri/example.php';

        var host_email = '{{$host_email}}';
        var email = '{{$email}}';
        var passEmail = '{{$passEmail}}';
        var port = '{{$portEmail}}';
        var host_bd = '{{$host_bd}}';
        var pass_bd = '{{$pass_bd}}';
        var user_bd = '{{$user_bd}}';
        var database = '{{$database_bd}}';
        var port_bd = '{{ $port_bd}}';
        var id_factura = '{{$factura_id}}';
        var email_cliente = '';
        var server = '';








       obtenerComprobanteFirmado_sri(ruta_certificado, pwd_p12, ruta_respuesta, ruta_factura, host_bd, pass_bd, user_bd, database, port_bd, id_factura);



    }

</script>



@endsection
