@extends('layouts.master.master')
@section('title', 'Cliente Tienda || Acerca de')

@section('content')
<div class="container" style="margin-top: 80px">

    <div class="row justify-content-center">

        <div class="super_container">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Acerca de</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="text-center">
                <img src="https://www.valentina-store.com/wp-content/uploads/2021/01/Logo-Valentina-Store.jpg" class="rounded" alt="..." width="250">
              </div>

          <center><h1>Historia</h1></center>
          <center><p>STORE VALENTINA se constituyó en el año 2018
            como una micro  orgullosamente ecuatoriana,
 </p></center>

 <center><h1>Misión</h1></center>
 <center><p>Superar las expectativas de nuestros clientes en
     la coordinación de servicios a través de nuestro centro de
     atención especializado; mediante el mejoramiento continuo del
      Sistema de Gestión de Calidad cumpliendo con todos los requisitos
      aplicables. Innovando nuestros servicios; respaldados en talento
      humano calificado y la optimización de los recursos técnicos,
       tecnológicos y financieros.
</p></center>

<center><h1>Visión</h1></center>
<center><p>Liderar el mercado de soluciones empresariales,
     con innovación y automatización; generando efecto !WOW! en los clientes.
</p></center>


        </div>
    </div>

<!--


    -->

            <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
            <link href="{{ asset('css/owl.carousel.min.css')}}" rel="stylesheet">
            <link href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
            <script src="{{ asset('js/jquery.min.js') }}"></script>
            <script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
            <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
            <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
            <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
            <script src="{{ asset('js/aos.js') }}"></script>
            <script src="{{ asset('js/scrollax.min.js') }}"></script>
            <script src="{{ asset('js/main.js') }}"></script>
            <script src="{{ asset('js/detalle/jquery-3.2.1.min.js') }}"></script>
            <script src="{{ asset('js/detalle/jquery.flexslider-min.js') }}"></script>
            <script src="{{ asset('js/detalle/product.js') }}"></script>
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
            <!--
-->

<script>
    $('#colores').DataTable({
        responsive:true,
        autoWidth: false,
        dom: '<"#example_header"fB<"top"<"newline clear"><"newline clear">l>>rtip',




        "language": {
          // "lengthMenu": "Mostrar _MENU_ registros por página",
          "lengthMenu": "Mostrar "+
          `<select>
          <option value='3'>3</option>
          <option value='25'>25</option>
          <option value='50'>50</option>
          <option value='100'>100</option>
          <option value='-1'>all</option>
          </select> `  +
          "registros por página",
           "zeroRecords": "No se encontró nada,  - lo siento",
           "info": "Mostrando página _PAGE_ de _PAGES_",
           "infoEmpty": "No hay registros disponibles",
           "infoFiltered": "(filtrado de _MAX_ registros totales)",
           'search':'Buscar:',
           'paginate':{
               'next':'Siguiente',
               'previous':'Anterior'



           }
       }
    });
</script>
@endsection
