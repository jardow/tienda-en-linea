@extends('layouts.master.master')
@section('content')
<div class="container" style="margin-top: 80px">
    <div class="row justify-content-center">
        <div class="col-lg-12">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('catalogo.index') }}">Tienda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Cliente</li>
            <li class="breadcrumb-item active" aria-current="page">Carrito de Compras</li>
        </ol>
    </nav>
    @if(session()->has('success_msg'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session()->get('success_msg') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif
    @if(session()->has('alert_msg'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ session()->get('alert_msg') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif
    @if(count($errors) > 0)
        @foreach($errors0>all() as $error)
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ $error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        @endforeach
    @endif
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <br>
            @if(\Cart::getTotalQuantity()>0)
                <h4>{{ \Cart::getTotalQuantity()}} Producto (s) en su carrito</h4><br>
            @else
                <h4>
                    No hay producto (s) en su carrito</h4><br>
                <a href="{{ route('catalogo.index')}}" class="btn btn-dark">Seguir comprando</a>
            @endif

            @foreach($cartCollection as $item)
                <div class="row">
                    <div class="col-lg-3">
                        <img src="{{ $item->attributes->image }}" class="img-thumbnail" width="200" height="200">
                    </div>
                    <div class="col-lg-5">
                        <p>
                            <b>

                                <a href="#">{{ $item->name }}</a>
                                <!--   <a href="{{ route('detalle.edit',Crypt::encrypt($item->id))}}">{{ $item->name }}</a> -->

                            </b><br>
                            <b>Price: </b>${{ $item->price }}<br>
                            <b>Sub Total: </b>${{ \Cart::get($item->id)->getPriceSum() }}<br>
                            {{--                                <b>With Discount: </b>${{ \Cart::get($item->id)->getPriceSumWithConditions() }}--}}
                        </p>
                    </div>
                    <div class="col-lg-4">
                        <div class="row">
                            <form action="{{ route('cart.update') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <input type="hidden" value="{{ $item->id}}" id="id" name="id">
                                          <input type="number" class="form-control form-control-sm"  min="1" max="50" pattern="^[0-9]+" value="{{ $item->quantity }}"
                                           id="quantity" name="quantity" style="width: 70px; margin-right: 10px;">
                                    <button class="btn btn-secondary btn-sm" style="margin-right: 25px;"><i class="fa fa-edit"></i></button>
                                </div>
                            </form>
                            <form action="{{ route('cart.remove') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" value="{{ $item->id }}" id="id" name="id">
                                <button class="btn btn-dark btn-sm" style="margin-right: 10px;"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            @endforeach
            @if(count($cartCollection)>0)
                <form action="{{ route('cart.clear') }}" method="POST">
                    {{ csrf_field() }}
                    <button class="btn btn-outline-danger">Vaciar carrito</button>
                </form>
            @endif
        </div>
        @if(count($cartCollection)>0)
            <div class="col-lg-5">
                <div class="card">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b>Total: </b>${{ \Cart::getTotal() }}</li>
                    </ul>
                </div>
                <br><a href="{{ route('catalogo.index')}}" class="btn btn-outline-primary">Seguir Comprando</a>
                <a href="{{ route('checkout.index') }}" class="btn btn-outline-success">Procesar Compra</a>
            </div>
        @endif
    </div>
    <br><br>
</div>
</div>
</div>


<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/aos.js') }}"></script>
<script src="{{ asset('js/scrollax.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<link  href="{{ asset('css/animate.css')}}" rel="stylesheet">
<link  href="{{ asset('css/owl.carousel.min.css')}}"  rel="stylesheet">
<link  href="{{ asset('css/owl.theme.default.min.css')}}" rel="stylesheet">
@endsection
