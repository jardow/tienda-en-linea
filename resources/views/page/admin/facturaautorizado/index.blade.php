@extends('adminlte::page')
@section('title', 'Administrador || Facturación Autorizado')

@section('plugins.Sweetalert2', true)


@section('css')


<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">



@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Administrador</li>
                        <li class="breadcrumb-item">Facturación</li>
                        <li class="breadcrumb-item active" aria-current="page">Autorizado</li>
                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
        <hr>
        <table class="table table-striped" id="colores">
            <thead>
                <tr>
                    <th scope="col">Cod Factura</th>
                    <th scope="col">Modalidad Pago</th>
                    <th scope="col">Método Envío</th>
                    <th scope="col">Fecha Emisión</th>
                    <th scope="col">Estado</th>
                    <th scope="col">RIDE PDF</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($facturas as $factura )


                <tr>
                    <td>{{ $factura->id }}</td>
                    @if($factura->modalidad_pago_id==1)
                    <td>Tarjeta de Crédito / Débito</td>
                    @else
                    <td>Transferencia / Depósito</td>
                    @endif
                    @if($factura->metodo_envio_id==1)
                    <td>Envío Local</td>
                    @else
                    <td>Envío a Provincias</td>
                    @endif
                    <td>{{ \Carbon\Carbon::parse(strtotime($factura->fecha_emision))->formatLocalized('%d  %B %Y')  }}</td>
                    <td>{{ $factura->estado }}</td>
                    <td><a class="btn btn-outline-danger" href="{{ url('administrador/facturacion/factura/facturaPDF',Crypt::encrypt($factura->factura_id))}}">PDF</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection

@section('js')



<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    $('#colores').DataTable({
        responsive: true
        , autoWidth: false,

        "language": {
            // "lengthMenu": "Mostrar _MENU_ registros por página",
            "lengthMenu": "Mostrar " +
                `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> ` +
                "registros por página"
            , "zeroRecords": "No se encontró nada,  - lo siento"
            , "info": "Mostrando página _PAGE_ de _PAGES_"
            , "infoEmpty": "No hay registros disponibles"
            , "infoFiltered": "(filtrado de _MAX_ registros totales)"
            , 'search': 'Buscar:'
            , 'paginate': {
                'next': 'Siguiente'
                , 'previous': 'Anterior'
            }
        }
    });




    $(function() {
        $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');
        });
    });

</script>
@endsection
