@extends('adminlte::page')
@section('title', 'Gestión Sección || Crear')
@section('plugins.Sweetalert2', true)

@section('content')

<div class="card">
    <div class="card-header">
    <h1 class="card-title">Gestión Sección</h1>
    </div>
    <div class="card-body">
        @include('custom.message')

             <form action="{{ route('seccion.store')}}" method="POST">
             @csrf
             <div class="container">
                 <div class="mb-3">
                     <input type="text" class="form-control" id="descripcion" placeholder="Sección" name="descripcion"   value={{old('descripcion')}}>
                   </div>

             </div>
             <hr>
             <h3>Estado</h3>
             <div class="form-check form-check-inline">
                 <input class="form-check-input" type="radio" name="estado" id="activo" value="activo"
                 @if (old('estado')=="activo")
                 checked
               @endif

                 >
                 <label class="form-check-label" for="fullaccessyes">Activo</label>
               </div>
               <div class="form-check form-check-inline">
                 <input class="form-check-input" type="radio" name="estado" id="deshabilitado" value="deshabilitado"
                 @if (old('estado')=="deshabilitado")
                 checked
               @endif
               @if (old('estado')===null)
                 checked
               @endif

               >

                 <label class="form-check-label" for="fullaccessno">Deshabilitado</label>
               </div>
               <hr>


               <hr>
               <input class="btn btn-primary" type="submit" value="Guardar"\>
             </form>



     </div>
</div>

@stop

@section('css')

    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('js')

@stop
