@extends('adminlte::page')
@section('title', 'Administrador || Facturación Emitir Factura Validar Pago')
@section('plugins.Sweetalert2', true)
@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Administrador</li>
                        <li class="breadcrumb-item">Facturación</li>
                        <li class="breadcrumb-item">Emitir Factura</li>
                        <li class="breadcrumb-item active" aria-current="page">Validar Pago</li>
                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
        <hr>

        <table class="table table-striped" id="colores">
            <thead>
                <tr>
                    <th scope="col">Cod Factura</th>
                    <th scope="col">Fecha Pago</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Total</th>
                    <th scope="col">Detalle Compra</th>
                    <th scope="col">Comprobante</th>
                    <th scope="col">Autorizar</th>
                    <th scope="col">Rechazar</th>
                </tr>
            </thead>
            @foreach ($facturas as $factura )
            @php
            $rutas=DB::table('rutas')
            ->where('rutas.factura_id',$factura->id )
            ->get();
            @endphp
            <tbody>

            @if(!$rutas->isempty())

                @foreach ($rutas as $ruta )

                @endforeach

                @if($ruta->estado=='AUTORIZADO')
                @else
                <tr>
                    <td>{{ $factura->id }}</td>

                    <td>{{ \Carbon\Carbon::parse(strtotime($factura->fecha_pago))->formatLocalized('%d  %B %Y')  }}</td>
                        @if($factura->estado ==3)
                            <td>Proceso de Verificación</td>
                        @else
                            <td>Proceso de Verificación</td>
                        @endif

                        @if($factura->metodo_envio_id==1)
                            <td>{{number_format(($factura->total+$factura->iva)+3.50,2) }}</td>
                        @else
                            <td>{{ number_format (($factura->total +$factura->iva)+4.50,2) }}</td>
                        @endif
                    <td> <a class="btn btn-outline-primary" href="{{ url('administrador/pago/factura/detalle',Crypt::encrypt($factura->id)) }}">PRE. FACTURA</a></td>
                    <td><a class="btn btn-outline-primary" href="{{ url('administrador/facturacion/factura/detalle/comprobante',Crypt::encrypt($factura->id)) }}"><img src="{{ $factura->voucher }}" width="35"></img></a></td>

                   @php
                    $rutas=DB::table('rutas')->
                    where('rutas.factura_id', $factura->id)
                    ->get();
                    @endphp

                        @if(!$rutas->isEmpty())
                            <td><a class="btn btn-outline-success" href="{{url('administrador/facturacion/emitir/factura',Crypt::encrypt($factura->id)) }}">Generar Factura</a></td>
                        @else
                            <td><a class="btn btn-outline-success" href="{{url('administrador/facturacion/generar/factura',Crypt::encrypt($factura->id)) }}">Validar Pago</a></td>
                        @endif

                        @if(!$rutas->isEmpty())
                            <th></th>
                        @else
                            <td><a class="btn btn-outline-danger" href="{{ url('administrador/pago/rechazado',$factura->validarPagos_id)}}">Rechazado</a></td>
                        @endif
                </tr>
                @endif

            @else
            <tr>
                <td>{{ $factura->id }}</td>
                <td> {{ \Carbon\Carbon::parse(strtotime($factura->fecha_pago))->formatLocalized('%d  %B %Y')  }}</td>
                    @if($factura->estado ==3)
                        <td>Proceso de Verificación</td>
                    @else
                        <td>Proceso de Verificación</td>
                    @endif

                    @if($factura->metodo_envio_id==1)
                        <td>{{number_format(($factura->total+$factura->iva)+3.50,2) }}</td>
                    @else
                        <td>{{ number_format (($factura->total +$factura->iva)+4.50,2) }}</td>
                    @endif
                <td> <a class="btn btn-outline-primary" href="{{ url('administrador/pago/factura/detalle',Crypt::encrypt($factura->id)) }}">PRE. FACTURA</a></td>
                <td><a class="btn btn-outline-primary" href="{{ url('administrador/facturacion/factura/detalle/comprobante',Crypt::encrypt($factura->id)) }}"><img src="{{ $factura->voucher }}" width="35"></img></a></td>

               @php
                $rutas=DB::table('rutas')->
                where('rutas.factura_id', $factura->id)
                ->get();
                @endphp

                    @if(!$rutas->isEmpty())
                        <td><a class="btn btn-outline-success" href="{{url('administrador/facturacion/emitir/factura',Crypt::encrypt($factura->id)) }}">Generar Factura</a></td>
                    @else
                        <td><a class="btn btn-outline-success" href="{{url('administrador/facturacion/generar/factura',Crypt::encrypt($factura->id)) }}">Validar Pago</a></td>
                    @endif

                    @if(!$rutas->isEmpty())
                        <th></th>
                    @else
                        <td><a class="btn btn-outline-danger" href="{{ url('administrador/pago/rechazado',$factura->validarPagos_id)}}">Rechazar</a></td>
                    @endif
            </tr>

            @endif
            </tbody>
            @endforeach
        </table>
    </div>
</div>
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    $('#colores').DataTable({
        responsive: true
        , autoWidth: false,

        "language": {
            // "lengthMenu": "Mostrar _MENU_ registros por página",
            "lengthMenu": "Mostrar " +
                `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> ` +
                "registros por página"
            , "zeroRecords": "No se encontró nada,  - lo siento"
            , "info": "Mostrando página _PAGE_ de _PAGES_"
            , "infoEmpty": "No hay registros disponibles"
            , "infoFiltered": "(filtrado de _MAX_ registros totales)"
            , 'search': 'Buscar:'
            , 'paginate': {
                'next': 'Siguiente'
                , 'previous': 'Anterior'
            }
        }
    });
    $(function() {
        $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');
        });
    });

</script>
@endsection
