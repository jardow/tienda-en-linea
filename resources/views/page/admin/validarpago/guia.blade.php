@extends('adminlte::page')
@section('title', 'Facturación || Comprobante de Pago')

@section('plugins.Sweetalert2', true)


@section('css')


<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">



@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <h1 class="card-title">Comprobante de Pago</h1>
    </div>
    <div class="card-body">
        @include('custom.message')
        <hr>



        <table align="center">
            @foreach ($guia as $item )

            <img src="{{ $item->voucher }}" class="img-fluid" alt="Responsive image">
            @endforeach
        </table>





    </div>
</div>
@endsection

@section('js')



<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    $('#colores').DataTable({
        responsive: true
        , autoWidth: false,

        "language": {
            // "lengthMenu": "Mostrar _MENU_ registros por página",
            "lengthMenu": "Mostrar " +
                `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> ` +
                "registros por página"
            , "zeroRecords": "No se encontró nada,  - lo siento"
            , "info": "Mostrando página _PAGE_ de _PAGES_"
            , "infoEmpty": "No hay registros disponibles"
            , "infoFiltered": "(filtrado de _MAX_ registros totales)"
            , 'search': 'Buscar:'
            , 'paginate': {
                'next': 'Siguiente'
                , 'previous': 'Anterior'
            }
        }
    });




    $(function() {
        $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');
        });
    });

</script>
@endsection



