
@extends('adminlte::page')
@section('title', 'Administrador || Productos Crear Articulo')

@section('plugins.Sweetalert2', true)


@section('css')

    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Administrador</li>
                        <li class="breadcrumb-item">Productos</li>
                        <li class="breadcrumb-item">Crear Articulo</li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="{{ url()->previous() }}">Back</a></li>
                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
        <form action="{{ route('producto.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <br/>
    <div class="container">
        <div class="row row-cols-3">

          <div class="col">CodProducto <input type="text"  name="cod" id="cod" value="{{$codProducto}}" readonly></div>
          <div class="col">Nombre         <input type="text" name="nombre" id="nombre" value={{old('nombre')}}></div>
          <div class="col">Precio <input type="number" placeholder="0.00"   step="0.01" name="precio" id="precio" value={{old('precio')}}></div>
        </div>
        <br/><br/>
        <div class="row row-cols-3">
            <div class="col">Stock             <input type="number" name="stock" id="stock" value={{old('stock')}}></div>
            <div class="col">Estado
            <div class="card-body">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="estado" id="activo" value="activo"
                    @if (old('estado')=="activo")
                    checked
                  @endif
                    >
                    <label class="form-check-label" for="fullaccessyes">Activo</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="estado" id="deshabilitado" value="deshabilitado"
                    @if (old('estado')=="deshabilitado")
                    checked
                  @endif
                  @if (old('estado')===null)
                    checked
                  @endif
                  >
                    <label class="form-check-label" for="fullaccessno">Deshabilitado</label>
                  </div>
            </div>
                </div>
          </div>
          <br/><br/>

          <div class="row row-cols-3">
            <div class="col">Categoria
                <select class="form-select" aria-label="Default select example" name="categoria" id="categoria" onchange="categoriaf();">
                    <option value="0" selected>Selecione un opción</option>
                    @foreach($categorias as $categoria)
                        <option value="{{$categoria->id}}"  {{old('categoria')==$categoria->id?'selected':''}}>{{$categoria->descripcion}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col">Seccion
                    <select class="form-select" aria-label="Default select example" name="seccion" id="seccion" onchange="categoriaf();">
                        <option value="0" selected>Selecione un opción</option>
                        @foreach($seccions as $seccion)
                            <option value="{{$seccion->id}}"  {{old('seccion')==$seccion->id?'selected':''}}>{{$seccion->descripcion}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col">Origen
                        <select class="form-select" aria-label="Default select example" name="origen" id="origen">
                            <option value="0" selected>Selecione un opción</option>
                            @foreach($origens as $origen)
                            <option value="{{$origen->id}}"  {{old('origen')==$origen->id?'selected':''}}>{{$origen->descripcion}}</option>
                            @endforeach
                          </select>
                        </div>
        </div>

        <br/> <br/> <br/>

          <div class="row row-cols-3">
            <div id="talla1"  class="col" style="display: none">
                Talla       <select class="form-select" aria-label="Default select example" id='talla' name='talla' style="display: none">
                    <option value='0'>Selecione un opción</option>
                 </select>
                </div>
            <div class="col">Color  
                <select class="form-select" aria-label="Default select example" name="color" id="color" >
                    <option value="0" selected>Selecione un opción</option>
                    @foreach($colors as $color)
                    <option value="{{$color->id}}"  {{old('color')==$color->id?'selected':''}}>{{$color->descripcion}}</option>
                    @endforeach
                  </select>
            </div>
            <br/><br/>
          </div>
          <div class="row row-cols-2">
            <div class="col">Imagen 1 <input class="form-control"  accept="image/png,image/jpeg" type="file" name="imagen1" id="imagen1" ></div>
            <div class="col">Imagen 2 <input class="form-control"  accept="image/png,image/jpeg" type="file" name="imagen2" id="imagen2" ></div>
            <br/><br/>
            <div class="col">Imagen 3 <input class="form-control"  accept="image/png,image/jpeg" type="file" name="imagen3" id="imagen3" ></div>
            <div class="col">Imagen 4 <input class="form-control"  accept="image/png,image/jpeg" type="file" name="imagen4" id="imagen4" ></div>
            <br/><br/>
            <div class="col">Imagen 5 <input class="form-control" accept="image/png,image/jpeg" type="file" name="imagen5" id="imagen5" ></div>
          </div>
          <br/><br/>
          <div class="col-12 col-sm-12">
            <div class="col">Descripcion <textarea class="form-control rounded-0" placeholder="Descripción" id="descripcion" name="descripcion" style="height: 100px">{{ old('descripcion') }}</textarea></div>
          </div>
          <br/>
          <div class="col-12 col-sm-12">
            <div  class="col"><center>      <input class="btn btn-primary" type="submit" value="Guardar"\></center>
                <hr></div>
          </div>
          <br/>
      </div>
  </div>
</div>
</form>
@endsection
@section('js')
<script src="{{url('js/categoria/categoria.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
