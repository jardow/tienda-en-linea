
@extends('adminlte::page')
@section('title', 'Administrador || Productos Editar Articulo')

@section('plugins.Sweetalert2', true)


@section('css')

    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Administrador</li>
                        <li class="breadcrumb-item">Productos</li>
                        <li class="breadcrumb-item">Editar Articulo</li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="{{ url()->previous() }}">Back</a></li>

                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
    <form action="{{ route('producto.update', $producto->id)}}" method="POST" enctype="multipart/form-data">
                    @method('Put')
                    @csrf
                <br/>
            <div class="container">
                <div class="row row-cols-3">
                  <div class="col">CodProducto <input type="text"  name="cod" id="cod" value="{{$producto->codigo_producto}}" readonly></div>
                  @foreach ($relaccions as $relaccion)
                  <div class="col">Nombre  
                    <textarea class="form-control"
                    placeholder="Descripción" id="nombre" name="nombre" readonly
                    style="height: 100px">{{$relaccion->articulo->descripcion}}  </textarea>

                </div>
                 <div class="col">Precio <input type="number" placeholder="0.00"   step="0.01" name="precio" id="precio" value={{$producto->precio}} autofocus></div>
                </div>
                <br/><br/>
                <div class="row row-cols-3">
                    <div class="col">Stock             <input type="number" name="stock" id="stock" value={{$producto->stock}}></div>
                    <div class="col">Estado
                        <div class="card-body">
                            <div class="form-check form-check-inline">
                                <input type="hidden" name="estado" id="estado" value="{{ $producto->estado }}">
                            <input class="form-check-input" type="radio" name="estado" id="activo" value="1" disabled="disabled"
                             @if ( $producto['estado']=="1")
                             checked
                             @elseif (old('estado')=="1")
                            checked
                            @endif
                            >
                            <label class="form-check-label" for="fullaccessyes">Activo</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="estado" id="deshabilitado" value="2" disabled="disabled"
                                @if ( $producto['estado']=="2")
                                checked
                                @elseif (old('estado')=="2")
                                checked
                                @endif
                                >
                                <label class="form-check-label" for="fullaccessno">Deshabilitado</label>
                           </div>
                        </div>
                            </div>
                      </div>
                      <br/><br/>
                      <div class="row row-cols-3">
                        <div class="col">Categoria    
                                @foreach($categorias as $categoria)

                                @if ($categoria->id==$relaccion->categoria->id)

                                <input type="text"  name="categoria" id="categoria" value="{{$categoria->descripcion}}" readonly>
                                @endif
                                @endforeach
                            </div>
                            <div class="col">Seccion
                                    @foreach($seccions as $seccion)
                                    @if ($seccion->id==$relaccion->seccion->id)
                                    <input type="text"  name="seccion" id="seccion" value="{{$seccion->descripcion}}" readonly>
                                    @endif
                                    @endforeach
                                </div>
                                <div class="col">Origen
                                        @foreach($origens as $origen)
                                        @if ($origen->id==$idOrigen)
                                        <input type="text"  name="origen" id="origen" value="{{$origen->descripcion}}" readonly>
                                        @endif
                                        @endforeach
                                </div>
                    </div>
                <br/> <br/> <br/>
                  <div class="row row-cols-3">
                    <div class="col">Color           
                            @foreach($colors as $color)
                            @if ($color->id==$relaccion->color->id)
                            <input type="text"  name="color" id="color" value="{{$color->descripcion}}" readonly>
                            @endif
                            @endforeach
                    </div>
                    <div id="talla1"  class="col">
                            
                            @foreach($tallas as $talla)

                            @if ($talla->id==$relaccion->talla->id)

                            @if($relaccion->talla->id==0)
                               @else
                               Talla 
                               <input type="text"  name="talla" id="talla" value="{{$talla->talla}}" readonly>
                            @endif

                            @endif
                            @endforeach
                        </div>
                    <br/><br/>
                  </div>
                  @endforeach
                  <br/>
                  <input type="text" name="idrelacion" id="idrelacion" value="{{$relaccion->id}}" hidden>
                  <div class="row row-cols-3">
                        <div class="col">
                            <table border="1">
                                <tr>
                                  @foreach ($imagenes as $imagen )
                                  <th>   </th>
                                  <th>
                                      @if ($imagen->url=="null")
                                             <img src="\images\producto\no.jpg"
                                      style="border: 1px solid #cccccc;
                                      border-radius: 15px; width: 129px;
                                      height: auto;float:left; margin-right: 7px;">
                                     @else
                                      <img src="{{$imagen->url}}"
                                      style="border: 1px solid #cccccc;
                                      border-radius: 15px; width: 129px;
                                      height: auto;float:left; margin-right: 7px;">
                                    </th>
                                    @endif
                                  <th>       
                                    <a class="btn btn-success"  href="{{ route('imagen.edit', $imagen->id)}}">Cambiar</a>                                                             
                                  </th>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                  <br/><br/>
                  <div class="col-12 col-sm-12">
                    <div class="col">Descripcion <textarea class="form-control"
                         placeholder="Descripción" id="descripcion" name="descripcion"
                         style="height: 100px">{{$producto->descripcion}}</textarea></div>
                        </div>
                  <br/>
                  <div class="col-12 col-sm-12">
                    <div  class="col"><center>      <input class="btn btn-primary" type="submit" value="Guardar"\></center>
                        <hr></div>
                  </div>
                  <br/>
              </div>
            </form>

          </div>
        </div>
</div>
@endsection
@section('js')
<script src="{{url('js/categoria/categoria.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
