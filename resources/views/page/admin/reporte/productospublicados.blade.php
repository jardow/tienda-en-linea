
@extends('adminlte::page')
@section('title', 'Administrador || Reportes Publicado')

@section('plugins.Sweetalert2', true)


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Administrador</li>
                        <li class="breadcrumb-item">Reportes</li>
                        <li class="breadcrumb-item">Publicado</li>
                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
    <hr>


    <table  class="table table-striped" id="colores">
        <thead>
            <tr>
                <th scope="col">Cod</th>
                <th scope="col">Imagen</th>
                <th scope="col">Artículo</th>
                <th scope="col">Fecha Publicado</th>
                <th scope="col">Stock</th>
            </tr>
        </thead>
        <tbody>
           @foreach ($ProductosPublicados as $producto )
            <tr>
                <td>{{$producto->id}}</td>
                <td><img src="{{ $producto->imagen}}" width="35"></img></a></td>
                <td>{{$producto->articulo}}</td>
                <td>{{ \Carbon\Carbon::parse(strtotime($producto->fecha_publicado))->formatLocalized('%d  %B %Y')  }}</td>
                <td>{{$producto->stock}}</td>

            </tr>
            @endforeach

        </tbody>


       </table>

    </div>
  </div>
@endsection

@section('js')

<script src="{{url('js/estado/estado.js')}}"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
