
@extends('adminlte::page')
@section('title', 'Gestión Imagen || Editar')

@section('plugins.Sweetalert2', true)


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Administrador</li>
                        <li class="breadcrumb-item">Productos</li>
                        <li class="breadcrumb-item">Editar Articulo</li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="{{ url()->previous() }}">Back</a></li>

                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')

    <hr>

    <form action="{{ route('imagen.update', $imagen->id)}}" method="POST" enctype="multipart/form-data">
        @method('Put')
         @csrf
         <table border="1">
            <table border="1">
                <tr>
                    @foreach ($imagens as $img )

                  <th> </th>
                  <th>
                    @if ($img->url=="null")
                           <img src="\images\producto\no.jpg"
                    style="border: 1px solid #cccccc;
                    border-radius: 15px; width: 79px;
                    height: auto;float:left; margin-right: 7px;">
                   @else
                    <img src="{{$img->url}}"
                    style="border: 1px solid #cccccc;
                    border-radius: 15px; width:199px;
                    height: auto;float:left; margin-right: 7px;">
                  </th>
                  @endif
                  <th>
                    <input accept="image/png,image/jpeg" type="file" name="imagen" id="imagen" >
                  </th>
                </tr>
                @endforeach
            </table>
<tr>
    <th>
        <input class="btn btn-primary" type="submit" value="Guardar"\>
    </th>

</tr>
       </form>
    </div>
  </div>
@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
