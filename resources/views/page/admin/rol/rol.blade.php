
@extends('adminlte::page')
@section('title', 'Módulo Asignar Rol|| Asignar Rol')

@section('plugins.Sweetalert2', true)


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
    <h1 class="card-title">Habilitar o Deshabilitar</h1>
    </div>
    <div class="card-body">
        @include('custom.message')

    <hr>


    <form action="{{ route('asignarRol.update', $id)}}" method="POST">
        @method('Put')
         @csrf
       <div class="container">

@foreach ($user as $us )


        <div class="row row-cols-4">
            <div class="col">
                Nombres:  <input class="form-control" type="text"  name="nombre" id="nombre" value="{{$us->name}}" readonly>
            </div>
        </div>

        <div class="row row-cols-4">
            <div class="col">
                Apellidos:  <input class="form-control" type="text"  name="nombre" id="nombre" value="{{$us->surname}}"readonly >
            </div>
        </div>

        <div class="row row-cols-4">
            <div class="col">
                Correo:  <input class="form-control" type="text"  name="nombre" id="nombre" value="{{$us->email}}" readonly>
            </div>
        </div>

        <div class="row row-cols-4">
            <div class="col">
                Ciudad:  <input class="form-control" type="text"  name="nombre" id="nombre" value="{{$us->city}}" readonly>
            </div>
        </div>




        @endforeach

        @foreach ($role as $rol )



       </div>
       <hr>
       <h3>Rol</h3>
    <div class="form-check form-check-inline">
           <input class="form-check-input" type="radio" name="rol" id="cliente" value="cliente" disabled
           @if ( $rol->slug =="cliente")
           checked
         @elseif (old('slug')=="cliente")
           checked
         @endif

           >
           <label class="form-check-label" for="fullaccessyes">Cliente</label>
         </div>
         <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="rol" id="repartidor" value="repartidor" disabled
           @if (  $rol->slug =="repartidor")
             checked
           @elseif (old('slug')=="repartidor")
             checked
           @endif


         >

           <label class="form-check-label" for="fullaccessno">Repartidor</label>
         </div>

         <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="rol" id="logistica" value="logistica" disabled
             @if (  $rol->slug =="logistica")
               checked
             @elseif (old('slug')=="logistica")
               checked
             @endif


           >

             <label class="form-check-label" for="fullaccessno">Logistica</label>
           </div>




     </div>


     <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="rol" id="cliente" value="cliente" disabled
        @if ( $us->state =="cliente")
        checked
      @elseif (old('slug')=="cliente")
        checked
      @endif

        >
        <label class="form-check-label" for="fullaccessyes">Cliente</label>
      </div>
      <div class="form-check form-check-inline">
       <input class="form-check-input" type="radio" name="rol" id="repartidor" value="repartidor" disabled
        @if (  $us->state =="repartidor")
          checked
        @elseif (old('slug')=="repartidor")
          checked
        @endif


      >

        <label class="form-check-label" for="fullaccessno">Repartidor</label>
      </div>






    </div>


    </div>


<!--Hola  -->





</div>



    <input class="btn btn-primary" type="submit" value="Guardar"\>
</form>



  </div>
  @endforeach
@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
