@extends('adminlte::page')
@section('title', 'Facturación || Generar Factura')

@section('plugins.Sweetalert2', true)


@section('css')


<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <h1 class="card-title">GENERAR FACTURA ELECTRÓNICA</h1>
    </div>
    <div class="card-body">
        @include('custom.message')
        <hr>
        @php
        $datos_factura=DB::table('datos_facturacion_electronicas')
        ->select('datos_facturacion_electronicas.ubicacionarchivop12',
        'datos_facturacion_electronicas.contrasena')
        ->get();
        foreach ($datos_factura as $item) {
        }
        //Consulta Base
        $ruta_certificado_p12=public_path($item->ubicacionarchivop12);
        $contrasena=$item->contrasena;
        //Host mail
        $host_email="smtp.gmail.com";
        $email="facturacion.store.valentina@gmail.com";
        $passEmail="8829store";
        $portEmail="465";
        //Parametros de Conexion BD
        $host_bd=config('database.connections.mysql.host');
        $pass_bd=config('database.connections.mysql.password');
        $user_bd=config('database.connections.mysql.username');
        $database_bd=config('database.connections.mysql.database');
        $port_bd=config('database.connections.mysql.port');
        $estadoCompra=DB::table('facturas')->where('id', $factura_id)->get();
        foreach($estadoCompra as $estado){
        $est=$estado->estado_compra_id;
        }
        // return $est;
        if($est==1){
        $rutas=DB::table('rutas')
        ->join('facturas', 'rutas.factura_id', '=', 'facturas.id')
        ->where('rutas.factura_id', $factura_id)
        ->select('facturas.claveacceso',
        'rutas.rutagenerado')
        ->get();
        //C:\xampp\htdocs\Test\tesis-tienda-virtual\ProyectoTesis\public\Factura\No_Firmado\.xml
        foreach($rutas as $ruta){
        }
        $claveA=$ruta->claveacceso;
        $ruta_xml=Storage::disk('local')->get($ruta->rutagenerado);
        // $ruta_xml=$ruta->rutagenerado;
        $factura_id=$factura_id;
        }
        @endphp
        @php
        $factura_id=$factura_id;
        $relacion_ids=DB::table('detalle_facturas')
        ->where('factura_id', $factura_id)
        ->select('detalle_facturas.cantidad',
        'articulos.descripcion',
        'detalle_facturas.total',
        'detalle_facturas.preciou',
        'productos.codigo_producto')
        ->join('productos', 'detalle_facturas.producto_id', '=', 'productos.id')
        ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
        ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
        ->get();
        $factura_id=$factura_id;
        $relacions=DB::table('facturas')
        ->where('id', $factura_id)
        ->get();
        foreach($relacions as $ses){
        }
        @endphp
        <center>
            <h3>Detalle de Compra</h3>
        </center>
        @php
        $rutas=DB::table('rutas')->where('factura_id', $factura_id)->get();
        foreach ($rutas as $r) {
        # code...
        }
        @endphp
        @if ($rutas->isEmpty())
        <p>Hola</p>


        <center> <button autofocus class="btn btn-outline-primary" id="fin" style="width: 350px" onclick="prueba();">GENERAR FACTURACIÖN ELECTRONICA </button></center>

        @if($est==1)
        <input type="hidden" value="{{ $ruta_xml }}" id="xml"></input>
        @endif
        <table class="table table-striped" align="center">
            <thead>
                <tr>
                    <th>Cod Producto</th>
                    <th>Articulo</th>
                    <th>Cantidad</th>
                    <th>Precio Unitario</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($relacion_ids as $item )
                <tr>
                    <td>{{ $item->codigo_producto }}</td>
                    <td>{{ $item->descripcion }}</td>
                    <td>{{ $item->cantidad }}</td>
                    <td>{{ $item->preciou }}</td>
                    <td>{{ $item->total }}</td>
                    @endforeach
                </tr>
                <tr>
                    @php
                    $metodos_envios=DB::table('metodo_envios')
                    ->where('id',$ses->metodo_envio_id)
                    ->get();;
                    @endphp
                    @foreach ($metodos_envios as $envio )
                    <td>{{ $envio->codigo }}</td>
                    <td>{{ $envio->descripcion }}</td>
                    <td>1</td>
                    <td>{{ $envio->precio }}</td>
                    <td>{{ $envio->precio }}</td>
                    @endforeach
                </tr>
                <td>Método de Pago</td>
                @if ($ses->modalidad_pago_id==2)
                <td>Transferencia / Depósito</td>
                @endif
                @if ($ses->modalidad_pago_id==1)
                <td>Tarjeta de Crédito / Débito</td>
                @endif
                <tr>
                    <th>IVA</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ $ses->iva }}</td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <th colspan="4">Total</th>
                    @if ($ses->metodo_envio_id==1)
                    <td colspan="4">$ {{ number_format($ses->total+3.50+$ses->iva ,2)}}</td>
                    @endif
                    @if ($ses->metodo_envio_id==2)
                    <td colspan="4">$ {{ number_format($ses->total+4.50+$ses->iva ,2)}}</td>
                    @endif
                </tr>
                <tr>
                    @if($est==1)
                    <th><input id="claveA" type="hidden" value="{{ $claveA }}"></th>
                    @endif

        Fin
        @else


        @if($r->estado=='AUTORIZADO')
        <div class="text-center">
            <img src="https://srienlinea.sri.gob.ec/recursos-sri-en-linea/imagenes/consultas/facturacion-electronica-carrusel.png" class="rounded" alt="..." width="250">
        </div>
        <center> <button autofocus class="btn btn-danger" id="fin" style="width: 450px" disabled>COMPROBANTE GENERADO CORRECTAMENTE Y ENVIADO AL CORREO ELECTRONICO</button></center>
        @else
        <center> <button autofocus class="btn btn-outline-primary" id="fin" style="width: 350px" onclick="prueba();">GENERAR FACTURACIÖN ELECTRONICA </button></center>
        @endif
        @if($est==1)
        <input type="hidden" value="{{ $ruta_xml }}" id="xml"></input>
        @endif
        <table class="table table-striped" align="center">
            <thead>
                <tr>
                    <th>Cod Producto</th>
                    <th>Articulo</th>
                    <th>Cantidad</th>
                    <th>Precio Unitario</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($relacion_ids as $item )
                <tr>
                    <td>{{ $item->codigo_producto }}</td>
                    <td>{{ $item->descripcion }}</td>
                    <td>{{ $item->cantidad }}</td>
                    <td>{{ $item->preciou }}</td>
                    <td>{{ $item->total }}</td>
                    @endforeach
                </tr>
                <tr>
                    @php
                    $metodos_envios=DB::table('metodo_envios')
                    ->where('id',$ses->metodo_envio_id)
                    ->get();;
                    @endphp
                    @foreach ($metodos_envios as $envio )
                    <td>{{ $envio->codigo }}</td>
                    <td>{{ $envio->descripcion }}</td>
                    <td>1</td>
                    <td>{{ $envio->precio }}</td>
                    <td>{{ $envio->precio }}</td>
                    @endforeach
                </tr>
                <td>Método de Pago</td>
                @if ($ses->modalidad_pago_id==2)
                <td>Transferencia / Depósito</td>
                @endif
                @if ($ses->modalidad_pago_id==1)
                <td>Tarjeta de Crédito / Débito</td>
                @endif
                <tr>
                    <th>IVA</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ $ses->iva }}</td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <th colspan="4">Total</th>
                    @if ($ses->metodo_envio_id==1)
                    <td colspan="4">$ {{ number_format($ses->total+3.50+$ses->iva ,2)}}</td>
                    @endif
                    @if ($ses->metodo_envio_id==2)
                    <td colspan="4">$ {{ number_format($ses->total+4.50+$ses->iva ,2)}}</td>
                    @endif
                </tr>
                <tr>
                    @if($est==1)
                    <th><input id="claveA" type="hidden" value="{{ $claveA }}"></th>
                    @endif
                    @endif
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
//Factura
<script src="{{ asset('js/factura/factura.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/fiddle.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/forge.min.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/moment.min.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/buffer.js') }}"></script>
<script src="{{ asset('Factura/lib_firma_sri/js/uft8.js') }}"></script>
<script>
function prueba() {
    const btncompra = document.getElementById('fin');
   btncompra.disabled = true;
     var xml = $("#xml").val()
      // var ruta_certificado = 'https://storevalentina.com/Factura/lib_firma_sri/firma/nelly_maria_cuenca_macas.p12';
     var ruta_certificado = 'http://localhost:8000/Factura/lib_firma_sri/firma/nelly_maria_cuenca_macas.p12';
     var ruta_factura = xml;
     var pwd_p12 = '{{$contrasena}}';
     //  var ruta_respuesta = 'https://storevalentina.com/Factura/lib_firma_sri/example.php';
     var ruta_respuesta = 'http://localhost:8000/Factura/lib_firma_sri/example.php';
     var host_email = '{{$host_email}}';
     var email = '{{$email}}';
     var passEmail = '{{$passEmail}}';
     var port = '{{$portEmail}}';
     var host_bd = '{{$host_bd}}';
     var pass_bd = '{{$pass_bd}}';
     var user_bd = '{{$user_bd}}';
     var database = '{{$database_bd}}';
     var port_bd = '{{ $port_bd}}';
     var id_factura = '{{$factura_id}}';
     var email_cliente = '';
     var server = '';
     obtenerComprobanteFirmado_sri(ruta_certificado, pwd_p12, ruta_respuesta, ruta_factura, host_bd, pass_bd, user_bd, database, port_bd, id_factura);
 }
    $('#colores').DataTable({
        responsive: true
        , autoWidth: false,

        "language": {
            // "lengthMenu": "Mostrar _MENU_ registros por página",
            "lengthMenu": "Mostrar " +
                `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> ` +
                "registros por página"
            , "zeroRecords": "No se encontró nada,  - lo siento"
            , "info": "Mostrando página _PAGE_ de _PAGES_"
            , "infoEmpty": "No hay registros disponibles"
            , "infoFiltered": "(filtrado de _MAX_ registros totales)"
            , 'search': 'Buscar:'
            , 'paginate': {
                'next': 'Siguiente'
                , 'previous': 'Anterior'
            }
        }
    });
</script>
@endsection
