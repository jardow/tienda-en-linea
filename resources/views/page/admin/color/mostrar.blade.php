@extends('adminlte::page')
@section('title', 'Color')

@section('plugins.Sweetalert2', true)
@section('content_header')
<h1>Tablero</>
@stop

@section('content')

<div class="card">
    <div class="card-header">
    <h1 class="card-title">Hola mundo</h1>
    </div>

    <div class="card-body">
        @include('custom.message')

         <form action="{{ route('color.update', $color->id)}}" method="POST">
          @csrf
          @method('PUT')

          <div class="container">

             <h3>Required data</h3>

              <div class="form-group">
                 <input type="text" class="form-control"
                 id="name"
                 placeholder="Name"
                 name="name"
                 value="{{ old('name', $color->descripcion)}}"
                 readonly>
               </div>
               <div class="form-group">
                 <input type="text"
                 class="form-control"
                 id="slug"
                 placeholder="Slug"
                 name="slug"
                 value="{{ old('slug' , $color->estado)}}"
                 readonly>
               </div>
               <div class="form-group">
                <input type="text"
                class="form-control"
                id="created_at"
                placeholder="Fecha"
                name="created_at"
                value="{{ old('created_at' , $color->created_at)}}"
                readonly>
              </div>

              <div class="form-group">
                <input type="text"
                class="form-control"
                id="updated_at"
                placeholder="Fecha"
                name="updated_at"
                value="{{ old('updated_at' , $color->updated_at)}}"
                readonly>
              </div>


               <hr>




                <a class="btn btn-success" href="{{route('color.edit',$color->id)}}">Edit</a>
               <a class="btn btn-danger" href="{{route('color.index')}}">Back</a>
          </div>
         </form>
     </div>

</div>

@stop

@section('css')
	<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop
