@extends('adminlte::page')
@section('title', 'Administrador || Productos Gestión Productos Cateogoría')

@section('plugins.Sweetalert2', true)


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Administrador</li>
                        <li class="breadcrumb-item">Productos</li>
                        <li class="breadcrumb-item">Gestión Producto</li>
                        <li class="breadcrumb-item">Categoría</li>


                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
       <a href=" {{route('categoria.create')}}" class="btn btn-primary">CREAR</a>
    <hr>


       <table  class="table table-striped" id="colores">
        <thead>
            <tr>
                <th scope="col">Cod</th>
                <th scope="col">Nombre</th>
                <th scope="col">Creado</th>
                <th scope="col">Actualizado</th>
                <th scope="col">Estado</th>
                <!--
                <th>ELIMINAR</th>
                -->


            </tr>
        </thead>
        <tbody>
            @foreach ($categorias as $categoria )
            <tr>
                <td scope="row">{{$categoria->id}}</td>
                <td>{{$categoria->descripcion}}</td>
                <td>{{$categoria->created_at->diffForHumans()}}</td>
                <td>{{$categoria->updated_at->diffForHumans()}}</td>

                @if($categoria->id==1 ||$categoria->id==2 )
                <td>
                    <a class="btn btn-primary"   href="#" disabled="disabled">Activo</a>
                </td>


                @else
                @if ($categoria->estado =="1")
                <td>
                    <a class="btn btn-outline-success"  href="{{ url('/estadocategoria', ['id' => $categoria->id, 'estado' => $categoria->estado , 'descripcion' => $categoria->descripcion])}}">Activo</a>
                </td>
                @else
                <td>
                    <a class="btn btn-outline-danger"  href="{{ url('/estadocategoria', ['id' => $categoria->id, 'estado' => $categoria->estado , 'descripcion' => $categoria->descripcion])}}">Inactivo</a>
                </td>
                @endif

                @endif




                <!--
                <td>
                    <form action="{{ route('categoria.destroy',$categoria->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button onclick="return confirm('Esta seguro que desea eliminar Categoría :  {{$categoria->descripcion}}')" class="btn btn-danger">Eliminar</button>
                    </form>
                </td>
            -->

                                </tr>

            @endforeach
        </tbody>
       </table>

    </div>
  </div>
@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
