@extends('adminlte::page')
@section('title', 'Perfil')

@section('plugins.Sweetalert2', false)
@section('content_header')

@stop

@section('content')

<div class="card">
    <div class="card-header">
    <h1 class="card-title">Edital Perfil</h1>
    </div>
    <div class="card-body">
        @include('custom.message')
        <form action="{{ route('perfil.update', $iduser)}}" method="POST" enctype="multipart/form-data">
            @method('Put')
            @csrf
            <div class="container rounded bg-white mt-5 mb-5">
                <div class="row">
                    <div class="col-md-5 border-right">
                        @foreach ($users as $user  )
                        <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                            @if ($user->avatar=="null")
                            <img class="rounded-circle mt-5" width="150px"
                            src="\images\perfil\no.jpg">
                            <input accept="image/png,image/jpeg" type="file" name="imagen" id="imagen" >
                            @else
                            <img class="rounded-circle mt-5" width="150px"
                            src="{{$user->avatar}}">
                            <span>
                                <input accept="image/png,image/jpeg" type="file" name="imagen" id="imagen" >
                            </span>
                            @endif
                            <span class="font-weight-bold">{{$user->name}}</span>
                            <span class="text-black-50">{{$user->email}}</span>
                            <div class="mt-5 text-center">
                                <center>      <input class="btn btn-primary" type="submit" value="Guardar"\></center>

                            </div>
                            <span> </span></div>

                    </div>
                    <div class="col-md-7 border-right">
                        <div class="p-3 py-5">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4 class="text-right">Configuración de perfil</h4>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-6"><label class="labels">Nombres</label><input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombres" value="{{$user->name}}" ></div>
                                <div class="col-md-6"><label class="labels">Apellidos</label><input type="text" class="form-control" name="apellido" id="apellido" value="{{$user->surname}}" placeholder="Apellidos" ></div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12"><label class="labels">Correo Electrónico</label><input type="text" class="form-control" name="correo" id="correo" placeholder="Correo" value="{{$user->email}}" readonly></div>
                                <div class="col-md-12"><label class="labels">Cédula de Identidad</label><input type="text" class="form-control" name="cedula" id="cedula" placeholder="Cédula" value="{{$user->idcard}}" readonly></div>
                                <div class="col-md-12"><label class="labels">Telefono</label><input type="text" class="form-control" placeholder="Telefono" name="celular" id="celular" value="{{$user->phone}}"></div>
                                <div class="col-md-12"><label class="labels">Dirección</label><input type="text" class="form-control" placeholder="Dirección" name="direccion" id="direccion" value="{{$user->city}}"></div>
                                <div class="col-md-12"><label class="labels"></label>
                                 <br>
                                <a class="btn btn-outline-primary" href="{{ url('password/reset')}}">Cambiar Contraseña:</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            @endforeach


  </div>
</form>

@stop

@section('css')
	<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
Swal.fire(
  'Good job!',
  'You clicked the button!',
  'success'
)
</script>
@stop
