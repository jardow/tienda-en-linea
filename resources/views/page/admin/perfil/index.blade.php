@extends('adminlte::page')
@section('title', 'Perfil')

@section('plugins.Sweetalert2', false)
@section('content_header')

@stop

@section('content')
@foreach ($users as $user  )
<div class="card">
        <div class="card-header">Perfil {{$user->name}}</div>


        @include('custom.message')
        <div class="container rounded bg-white mt-5 mb-5">
            <div class="row">
                <div class="col-md-5 border-right">

                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                        @if ($user->avatar=="null")
                        <img class="rounded-circle mt-5" width="150px"
                        src="\images\perfil\no.jpg">
                        @else
                        <img class="rounded-circle mt-5" width="150px"
                        src="{{$user->avatar}}">
                        @endif
                        <span class="font-weight-bold">{{$user->name}}</span>
                        <span class="text-black-50">{{$user->email}}</span>
                        <div class="mt-5 text-center">
                            <center>
                                <a class="btn btn-success"  href="{{ route('perfil.edit',Crypt::encrypt($user->id))}}">Editar</a>
                                </center>
                        </div>
                        <span> </span></div>

                </div>
                <div class="col-md-7 border-right">
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Configuración de perfil</h4>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-6"><label class="labels">Nombres</label>  <input type="text" class="form-control" placeholder="Nombres" value="{{$user->name}}" readonly></div>
                            <div class="col-md-6"><label class="labels">Apellidos</label><input type="text" class="form-control" placeholder="Apellidos" value="{{$user->surname}}" readonly></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels">Correo Electrónico</label><input type="text" class="form-control" placeholder="" value="{{$user->email}}" readonly></div>
                            <div class="col-md-12"><label class="labels">Cédula de Identidad</label><input type="text" class="form-control" placeholder="" value="{{$user->idcard}}" readonly></div>
                            <div class="col-md-12"><label class="labels">Telefono</label><input type="text" class="form-control" placeholder="Telefono" value="{{$user->phone}}" readonly></div>

                            <div class="col-md-12"><label class="labels">Dirección</label><input type="text" class="form-control" placeholder="Dirección" value="{{$user->city}}" readonly></div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        @endforeach
    </form>
  </div>


@stop

@section('css')
	<link rel="stylesheet" href="/css/admin_cusertom.css">
@stop

@section('js')
<script>
Swal.fire(
  'Good job!',
  'You clicked the button!',
  'success'
)
</script>
@stop
