@extends('adminlte::page')
@section('title', 'Gestión Talla || Editar')
@section('plugins.Sweetalert2', true)
@section('content')
<div class="card">
    <div class="card-header">
    <h1 class="card-title">Gestión Talla</h1>
    </div>
    <div class="card-body">
        @include('custom.message')
             <form action="{{ route('talla.update', $talla->id)}}" method="POST">
              @method('Put')
               @csrf
             <div class="container">
                 <div class="mb-3">
                     <input type="text" class="form-control" id="descripcion" placeholder="Talla" name="descripcion"   value={{old('descripcion', $talla->talla)}}>
                   </div>

             </div>
             <h5>Categoria</h5>
             <select name="categoria" id="categoria">
                @foreach($categorias as $categoria)
                @if ($categoria->id==$categoria_id)
                <option value="{{$categoria->id}}" selected>{{$categoria->descripcion}}</option>
                @else
                <option value="{{$categoria->id}}">{{$categoria->descripcion}}</option>
                @endif
                @endforeach
              </select>

              <hr>
              <h5>Sección</h5>
              <select name="seccion" id="seccion">
                <option value="0" selected>Selecione un opción</option>
                @foreach($seccions as $seccion)
                @if ($seccion->id==$seccion_id)
                <option value="{{$seccion->id}}" selected>{{$seccion->descripcion}}</option>
                @else
                <option value="{{$seccion->id}}">{{$seccion->descripcion}}</option>
                @endif
                @endforeach
               </select>


             <hr>
             <h3>Estado</h3>
             <div class="form-check form-check-inline">
                 <input class="form-check-input" type="radio" name="estado" id="activo" value="activo"
                 @if ( $talla['estado']=="activo")
                 checked
               @elseif (old('estado')=="activo")
                 checked
               @endif

                 >
                 <label class="form-check-label" for="fullaccessyes">Activo</label>
               </div>
               <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="estado" id="deshabilitado" value="deshabilitado"
                 @if ( $talla['estado']=="deshabilitado")
                   checked
                 @elseif (old('estado')=="deshabilitado")
                   checked
                 @endif


               >

                                <label class="form-check-label" for="fullaccessno">Deshabilitado</label>
               </div>
               <hr>
               <hr>
               <input class="btn btn-primary" type="submit" value="Guardar"\>
             </form>
     </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('js')

@stop
