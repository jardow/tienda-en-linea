
@extends('adminlte::page')
@section('title', 'Administrador || Usuarios Crear Usuario')

@section('plugins.Sweetalert2', true)


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Administrador</li>
                        <li class="breadcrumb-item">Usuarios</li>
                        <li class="breadcrumb-item active" aria-current="page">Crear Usuario</li>
                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
    <hr>


    <form action="{{ route('crearusuario.store')}}" method="POST">
        @csrf
        <div class="container">





            <div class="form-group row">

                <label for="avatar" class="col-md-4 col-form-label text-md-right">Avatar</label>
                <div class="col-md-6">
                    <img src="/images/avatar/1.png" alt="1"
                    style="border: 1px solid #cccccc; border-radius: 5px; width: 39px; height: auto;float:left; margin-right: 7px;">
                    <input id="avatar" type="radio" required="required"  @error('avatar') is-invalid @enderror" name="avatar" value="1.png"><br>
                    <br>
                    <img src="/images/avatar/2.jpg" alt="2"
                    style="border: 1px solid #cccccc; border-radius: 5px; width: 39px; height: auto;float:left; margin-right: 7px;">
                    <input  id="avatar" type="radio" required="required" name="avatar" @error('avatar') is-invalid @enderror" value="2.jpg">
                    <hr>
                    @error('avatar')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>


                <div class="form-group row">
                <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre</label>
                <div class="col-md-6">
                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" min="3" max="20">
                @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
         </div>
                </div>

        <div class="form-group row">
         <label for="apellido" class="col-md-4 col-form-label text-md-right">Apellido</label>
         <div class="col-md-6">
         <input id="apellido" type="text" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{ old('apellido') }}" required autocomplete="apellido" min="3" max="20">
         @error('apellido')
             <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
             </span>
         @enderror
        </div>
        </div>

        <div class="form-group row">
            <label for="idcard" class="col-md-4 col-form-label text-md-right">Cédula</label>
            <div class="col-md-6">
                <input id="cedula" type="text" class="form-control @error('idcard') is-invalid @enderror"
                name="idcard" value="{{ old('idcard') }}" required autocomplete="idcard" onblur="return check_cedula(this.form);"  max="10">
            @error('idcard')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
           </div>
           </div>

           <div class="form-group row">
            <label for="telefono" class="col-md-4 col-form-label text-md-right">Teléfono</label>
            <div class="col-md-6">
            <input id="telefono" type="number" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono" min="7" >
            @error('telefono')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
           </div>
           </div>

           <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">Correo</label>
            <div class="col-md-6">
            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
           </div>
           </div>

           <div class="form-group row">
            <label for="nombre" class="col-md-4 col-form-label text-md-right">Rol</label>
            <div class="col-md-6">

                <select  class="form-select" aria-label="Default select example" name="rol" id="rol">
                    <option value="0" selected>Seleccione una opción</option>
                    <option value="2">Logística</option>
                    <option value="4">Repartidor</option>
                  </select>

            @error('rol')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
           </div>
           </div>

            </div>

        </div>

          <hr>
         <center> <input class="btn btn-primary" type="submit" value="Guardar"\></center>

        </form>

    </div>
  </div>
@endsection

@section('js')
<script src="{{url('js/jscedula/codigo.js')}}"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
