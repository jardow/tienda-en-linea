
@extends('adminlte::page')
@section('title', 'Gestión Articulo || Inicio')

@section('plugins.Sweetalert2', true)


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
    <h1 class="card-title">Gestión Articulo</h1>
    </div>
    <div class="card-body">
       <a href=" {{route('articulo.create')}}" class="btn btn-primary">CREAR</a>
    <hr>


       <table  class="table table-striped" id="colores">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">NOMBRE</th>
                <th scope="col">ESTADO</th>
                <th scope="col">CREADO</th>
                <th scope="col">ACTUALIZADO</th>
                <th >ACCIONES</th>
                <th ></th>

            </tr>
        </thead>
        <tbody>
            @foreach ($articulos as $articulo )
            <tr>
                <td scope="row">{{$articulo->id}}</td>
                <td>{{$articulo->descripcion}}</td>
                <td>{{$articulo->estado}}</td>
                <td>{{$articulo->created_at->diffForHumans()}}</td>
                <td>{{$articulo->updated_at->diffForHumans()}}</td>
                <td> <a class="btn btn-success"  href="{{ route('categoria.edit', $articulo->id)}}">Editar</a></td>
                <td>
                    <form action="{{ route('categoria.destroy',$articulo->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button onclick="return confirm('Esta seguro que desea eliminar Color :  {{$articulo->descripcion}}')" class="btn btn-danger">Eliminar</button>
                    </form>
                </td>
                                </tr>

            @endforeach
        </tbody>
       </table>

    </div>
  </div>
@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
