
@extends('adminlte::page')
@section('title', 'Administrador || Productos Publicados Publicado')

@section('plugins.Sweetalert2', true)


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
    <link  rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

 <div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Administrador</li>
                        <li class="breadcrumb-item">Productos</li>
                        <li class="breadcrumb-item">Publicados</li>
                        <li class="breadcrumb-item">Publicado</li>

                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
    <hr>


       <table  class="table table-striped" id="colores">

        <thead>
            <tr>
                <th scope="col">Cod</th>
                <th scope="col">Imagen</th>
                <th scope="col">Artículo</th>
                <th scope="col">Fecha Publicado</th>
                <th>Estado</th>
                <th >Poner en Oferta</th>


            </tr>
        </thead>

        <tbody>
            @foreach ($productos as $producto )
          <tr>

            <td>{{$producto->id}}</td>
            <td><img src="{{$producto->imagen}}"
                style="border: 1px solid #cccccc; border-radius: 5px; width: 39px; height: auto;float:left; margin-right: 7px;"></td>
            <td>{{$producto->articulo}}</td>
            <td>{{ \Carbon\Carbon::parse(strtotime($producto->fecha_publicado))->formatLocalized('%d  %B %Y')  }}</td>

            @if ($producto->estado==1)
                <td> <a class="btn btn-outline-primary"  href="{{url('/publicarestado',['id' => $producto->id,'estado'=>$producto->estado] )}}">Publicado</a></td>
            @endif

            @if ($producto->estado==2)
            <td> <a class="btn btn-secondary"  href="{{url('/publicarestado',['id' => $producto->id,'estado'=>$producto->estado] )}}">Inactivo</a></td>
        @endif

        @if ($producto->estado==3)
        <td> <a class="btn disabled"  href="#">Oferta</a></td>
    @endif

        @if ($producto->estado==1)
            <td>
                <a class="btn btn-outline-danger"  href="{{url('/ofertar',['producto_id' => $producto->producto_id,'publicado_id'=>$producto->id] )}}">Oferta</a>
            </td>
            @endif

            @if ($producto->estado==2)
            <td>
                <a  class="btn disabled"  href="#">Inactivo</a>
            </td>
            @endif

            @if ($producto->estado==3)
            <td>
                <a  class="btn disabled"  href="#">Activo</a>
            </td>
            @endif

          </tr>
          @endforeach
        </tbody>
       </table>

    </div>
  </div>
@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
     $('#colores').DataTable({
         responsive:true,
         autoWidth: false,

         "language": {
           // "lengthMenu": "Mostrar _MENU_ registros por página",
           "lengthMenu": "Mostrar "+
           `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> `  +
           "registros por página",
            "zeroRecords": "No se encontró nada,  - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            'search':'Buscar:',
            'paginate':{
                'next':'Siguiente',
                'previous':'Anterior'
            }
        }
     });
</script>
@endsection
