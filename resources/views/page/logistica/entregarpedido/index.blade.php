@extends('adminlte::page')
@section('title', 'Logística || Entregar Pedido')

@section('plugins.Sweetalert2', true)


@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

<style>
    .selectAltura {
        display: block;
        height: 50px;
        width: 200px;
    }
    .selectAltura2 {
        padding: 20px;
    }
</style>
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Logística</li>
                        <li class="breadcrumb-item active" aria-current="page">Entregar Pedido</li>
                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
        <hr>
        <table class="table table-striped" id="colores">
            <thead>
                <tr>
                    <th scope="col">Cod</th>
                    <th scope="col">Detalle Factura</th>
                    <th scope="col">Tipo Envío</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Ciudad</th>
                    <th scope="col">Cantón</th>
                    <th scope="col">Parroquia</th>

                    <th scope="col">Repartidor</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($envios as $envio )
                <tr>
                    <td scope="row">{{ $envio->id }}</td>
                    <td> <a class="btn btn-outline-primary" href="{{ url('logistica/factura/detalle',Crypt::encrypt($envio->id)) }}">FACTURA</a></td>
                    <td>{{ $envio->metodo_envio_id }}</td>
                    <td>{{ $envio->nombres }} {{ $envio->apellidos }}</td>
                    <td>{{ $envio->ciudad }}</td>
                    <td>{{ $envio->canton }}</td>
                    <td>{{ $envio->parroquia }}</td>
                    <form action="{{ url('logistica/asignar/pedido/')}}" method="POST">
                        @csrf
                        <td><select id='mensajero' name='mensajero'>
                                <option value="0" selected>Asignar R</option>
                                @foreach ($repartidores as $repartidor )
                                <option value="{{ $repartidor->id }}">{{ $repartidor->surname }}</option>
                                @endforeach
                            </select>
                        </td>

                        <input type="hidden" name="envio_id" value="{{ $envio->id }}">

                        <td>
                            <input type="submit" class="btn btn-primary" value="Asignar P" \>

                        </td>
                    </form>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>
</div>
@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    $('#colores').DataTable({
        responsive: true
        , autoWidth: false,

        "language": {
            // "lengthMenu": "Mostrar _MENU_ registros por página",
            "lengthMenu": "Mostrar " +
                `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> ` +
                "registros por página"
            , "zeroRecords": "No se encontró nada,  - lo siento"
            , "info": "Mostrando página _PAGE_ de _PAGES_"
            , "infoEmpty": "No hay registros disponibles"
            , "infoFiltered": "(filtrado de _MAX_ registros totales)"
            , 'search': 'Buscar:'
            , 'paginate': {
                'next': 'Siguiente'
                , 'previous': 'Anterior'
            }
        }
    });

</script>
@endsection
