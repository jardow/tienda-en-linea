@extends('adminlte::page')
@section('title', 'Logística || Reporte Entrega')

@section('plugins.Sweetalert2', true)


@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Logística</li>
                        <li class="breadcrumb-item active" aria-current="page">Reporte Entrega</li>
                    </ol>
                </nav>

        </div>
    </div>
    <div class="card-body">
        @include('custom.message')
        <hr>


        <table class="table table-striped" id="colores">
            <thead>
                <tr>
                    <th scope="col">Cod Envío</th>
                    <th scope="col">Asignado Por</th>
                    <th scope="col">Fecha Asignado </th>
                    <th scope="col">Entregado Por</th>
                    <th scope="col">Fecha Entregado </th>
                    <th scope="col">Guía</th>
                    <th scope="col">Estado</th>

                    <!--
                <th>ELIMINAR</th>
                -->

                </tr>
            </thead>
            <tbody>
                @foreach ($detalle_envios as $detalle_envio )
                <tr>
                    <td scope="row">{{ $detalle_envio->id }}</td>
                    @php
                        $logisticas=DB::table('users')->where('id',$detalle_envio->user_id)->get();
                    @endphp
                    @foreach ($logisticas as $logistica )
                    <td>{{ $logistica->surname}}</td>
                    @endforeach
                    @php
                        $envios=DB::table('envios')->where('factura_id',$detalle_envio->envio_id)->get();
                    @endphp
                    @foreach ($envios as $envio )
                    <td>{{ $envio->updated_at }}</td>
                    @endforeach
                    @php
                        $repartidor=DB::table('users')->where('id',$detalle_envio->repartidor_id)->get();
                    @endphp
                     @foreach ($repartidor as $repartido )
                     <td>{{ $repartido->surname}}</td>
                     @endforeach
                    <td>{{ $detalle_envio->created_at}}</td>

                    <td><a class="btn btn-outline-success"  href="{{ url('logistica/pedidos/entregados/guia' ,Crypt::encrypt( $detalle_envio->envio_id))}}"><img src="{{ $detalle_envio->guia }}" width="35"></img></a></td>
                    @if($detalle_envio->estado==1)
                    <td>Entregado</td>
                    @else
                    <td>Pendiente</td>
                    @endif

                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection

@section('js')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script>
    $('#colores').DataTable({
        responsive: true
        , autoWidth: false,

        "language": {
            // "lengthMenu": "Mostrar _MENU_ registros por página",
            "lengthMenu": "Mostrar " +
                `<select>
           <option value='3'>3</option>
           <option value='25'>25</option>
           <option value='50'>50</option>
           <option value='100'>100</option>
           <option value='-1'>all</option>
           </select> ` +
                "registros por página"
            , "zeroRecords": "No se encontró nada,  - lo siento"
            , "info": "Mostrando página _PAGE_ de _PAGES_"
            , "infoEmpty": "No hay registros disponibles"
            , "infoFiltered": "(filtrado de _MAX_ registros totales)"
            , 'search': 'Buscar:'
            , 'paginate': {
                'next': 'Siguiente'
                , 'previous': 'Anterior'
            }
        }
    });

</script>
@endsection
