@extends('layouts.master.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create Roles') }}</div>

                <div class="card-body">
                   @include('custom.message')

                        <form action="{{ route('role.store')}}" method="POST">
                        @csrf
                        <div class="container">
                            <div class="mb-3">
                                <input type="text" class="form-control" id="name" placeholder="Name" name="name"   value={{old('name')}}>
                              </div>
                              <div class="mb-3">
                                <input type="text" class="form-control" id="slug" placeholder="Slug" name="slug" value={{old('slug')}}>
                              </div>
                              <div class="form-floating">
                                <textarea class="form-control" placeholder="Description" id="description" name="description" style="height: 100px" >{{ old('description')}}</textarea>
                              </div>
                        </div>
                        <hr>
                        <h3>Full Access</h3>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="full-access" id="fullaccessyes" value="yes"
                            @if (old('full-access')=="yes")
                            checked
                          @endif

                            >
                            <label class="form-check-label" for="fullaccessyes">Yes</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="full-access" id="fullaccessno" value="no"
                            @if (old('full-access')=="no")
                            checked
                          @endif
                          @if (old('full-access')===null)
                            checked
                          @endif

                          >

                            <label class="form-check-label" for="fullaccessno">No</label>
                          </div>
                          <hr>
                          <h3>Permission List</h3>
                          @foreach ($permissions as $permission )
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox"
                            class="custom-control-input"
                            id="permission_{{$permission->id}}"
                            value="{{$permission->id}}"
                            name="permission[]"
                            @if( is_array(old('permission')) && in_array("$permission->id", old('permission'))    )
                            checked
                            @endif
                            >

                            <label class="custom-control-label"
                                for="permission_{{$permission->id}}">
                                {{ $permission->id }}
                                -
                                {{ $permission->name }}
                                <em>( {{ $permission->description }} )</em>
                        </label>
                          </div>
                          @endforeach
                          <hr>
                          <input class="btn btn-primary" type="submit" value="Guardar"\>
                        </form>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
