<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Store Valentina - @yield('title')</title>


<script src="{{ asset('js/app.js') }}" defer></script>

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">



<!-- Styles
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
-->

<link rel="stylesheet" type="text/css" href="{{ asset('css/detalle/product.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/detalle/product_responsive.css')}}">


