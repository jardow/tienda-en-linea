<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @section('title', 'Page Title')
    @include('layouts.user.layouts.user_head')


</head>
<body>
    @include('layouts.user.layouts.user_header')


    @include('layouts.user.layouts.user_footer')
</body>
</html>
