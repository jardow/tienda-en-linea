<div class="py-1 bg-secondary">
    <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
            <div class="col-lg-12 d-block">
                <div class="row d-flex">
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
                      <a class="text-white bg-secondary" href="{{ URL::to('https://wa.link/6fw6yt') }}">+593 939230614</a>

                    </div>
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
                        <span class="text">info@storevalentina.com</span>
                    </div>
                    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
                        <span class="text">Quito &amp; Ecuador</span>
                    </div>
                </div>
            </div>
        </div>
      </div>
</div>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
                <div class="container">
                  <a class="navbar-brand" href="{{ url('/') }}">STORE VALENTINA</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                  <!--  <span class="oi oi-menu"></span>   Menu-->
                   <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <a class="dropdown-item" href="{{ url('/') }}">Inicio</a>
                          <a class="dropdown-item" href="{{ url('/cliente/catalogo') }}">Catálogo</a>
                          <a class="dropdown-item"      href="{{ '/cliente/acercade' }}">Acerca de</a>
                          <a class="dropdown-item" href="{{ '/cliente/contactanos' }}">Contactanos</a>
            <!--<li class="nav-item dropdown">  -->
                            <a id="navbarDropdown" class="nav-link dropdown-toggle"
                               href="#" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false"
                            >
                                <span class="badge badge-pill badge-dark">
                                    <i class="fa fa-shopping-cart"></i> {{ \Cart::getTotalQuantity()}}
                                </span>
                            </a>
                            <a class="dropdown-item" href="{{ route('cart.index') }}">Carrito</a>

                        @guest
                        @if (Route::has('login'))

                                <a href="{{ route('login') }}">{{ __('Login') }}</a>

                        @endif

                        @if (Route::has('register'))

                                <a  href="{{ route('register') }}">{{ __('Register') }}</a>

                        @endif
                      @else


                        <p></p>
                        <img src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}"
                        style="border: 1px solid #cccccc; border-radius: 5px; width: 39px; height: auto;float:left; margin-right: 7px;">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <div" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('cliente-perfil.index') }}">Perfil</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>


                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>

                    @endguest
                        </div>



                      </li>

                  </button>

                  <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav ml-auto">

                      <li class="nav-item active"><a href="{{ url('/') }}" class="nav-link">Inicio</a></li>
                      <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Catálogo</a>
                      <div class="dropdown-menu" aria-labelledby="dropdown04">
                     <a class="dropdown-item" href="{{ url('/cliente/catalogo') }}">Catálogo</a>

                        @if(count(\Cart::getContent()) > 0)
                        <a class="dropdown-item" href="{{ route('cart.index') }}">Carrito</a>
                        <a class="dropdown-item" href="{{ route('checkout.index') }}">Checkout</a>
                        @else

                        @endif
                      </div>
                    </li>
                      <li class="nav-item"><a href="{{ '/cliente/acercade' }}" class="nav-link">Acerca de</a></li>
                      <li class="nav-item"><a href="{{ '/cliente/contactanos' }}" class="nav-link">Contactanos</a></li>
                      <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle"
                           href="#" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"
                        >
                            <span class="badge badge-pill badge-dark">
                                <i class="fa fa-shopping-cart"></i> {{ \Cart::getTotalQuantity()}}
                            </span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="width: 450px; padding: 0px; border-color: #9DA0A2">
                            <ul class="list-group" style="margin: 20px;">
                                @include('partials.cart-drop')
                            </ul>

                        </div>
                    </li>

                    </ul>
                  </div>
                </div>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                      @else
                        <li class="nav-item dropdown">
                            <img src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->name }}"
                            style="border: 1px solid #cccccc; border-radius: 5px; width: 39px; height: auto;float:left; margin-right: 7px;">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('cliente-perfil.index') }}">Perfil</a>
                                <a class="dropdown-item" href="{{ route('validarPagoCliente.index') }}">Validar Pago</a>
                                <a class="dropdown-item" href="{{ route('factura.cliente') }}">Facturas</a>
                                <a class="dropdown-item" href="{{ url('cliente/compras/pedidos/pendientes') }}">Pendientes</a>
                                <a class="dropdown-item" href="{{ url('cliente/envios/') }}">Entregas</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>


                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
</div>

        @yield('content')


