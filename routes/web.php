<?php

use App\Models\Role;


use App\Models\User;  //libre
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Mail\ContactanosMailable;   //libre
use Illuminate\Support\Facades\Gate; //libre
use App\Http\Controllers\Admin\RolController;
use Illuminate\Support\Facades\Mail;  //libre
use Illuminate\Support\Facades\Auth;// liberia
use App\Http\Controllers\Admin\ColorController;
use App\Http\Controllers\Admin\TallaController;
use App\Http\Controllers\Admin\ImagenController;
use App\Http\Controllers\Admin\OfertaController;
use App\Http\Controllers\Admin\OrigenController;
use App\Http\Controllers\Cliente\CartController;
use App\Http\Controllers\Admin\SeccionController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Admin\ArticuloController;
use App\Http\Controllers\Admin\ProductoController;
use App\Http\Controllers\Cliente\BuscarController;
use App\Http\Controllers\Cliente\PerfilController;
use App\Http\Controllers\Admin\CategoriaController;
use App\Http\Controllers\Admin\PublicadoController;
use App\Http\Controllers\Cliente\DetalleController;
use App\Http\Controllers\Admin\TallaComboController;
use App\Http\Controllers\Cliente\AccionesController;
use App\Http\Controllers\Cliente\CatalogoController;
use App\Http\Controllers\Cliente\CheckoutController;
use App\Http\Controllers\Admin\MetodoAdminController;
use App\Http\Controllers\Admin\CrearUsuarioController;
use App\Http\Controllers\Admin\UserController; //libre
use App\Http\Controllers\HomeUserController;   //libre
use App\Http\Controllers\Admin\HomeController;  //libre
use App\Http\Controllers\Admin\Reporte_UsuatioController;
use App\Http\Controllers\Cliente\CorreoFacturaController;
use App\Http\Controllers\Cliente\MetodoClienteController;
use App\Http\Controllers\Cliente\OfertaDetalleController;
use App\Http\Controllers\Admin\Relaccion_ImagenController;
use App\Http\Controllers\Admin\ValidarPagoAdminController;
use App\Http\Controllers\Cliente\FacturaClienteController;
use App\Http\Controllers\Cliente\SinAuthController;
use App\Http\Controllers\Logistica\MetodoLogisticaController;
use App\Http\Controllers\Cliente\ValidarPagoClienteController;
use App\Http\Controllers\Repartidor\MetodoRepartidorController;
use App\Http\Controllers\Logistica\EntregarPedidoLogisticaController;
use App\Http\Controllers\Repartidor\EntregarPedidosMensajeroController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    //Ofertas


    $products=DB::table('productos')
    ->where('imagens.perfil','si')
    ->where('ofertas.estado','1')
    ->select(
        'productos.codigo_producto',
        'productos.id',
        'relaccion__imagens.relaccion_id',
        'productos.stock',
        'productos.estado',
        'ofertas.precio_oferta',
        'productos.descripcion',
        'imagens.url as imagen',
        'articulos.descripcion as articulo',
        'categorias.descripcion as categoria',
        'productos.created_at',
        'imagens.perfil',
        'imagens.url'
    )
    ->join('ofertas', 'productos.id', '=', 'ofertas.producto_id')
    ->join('publicados', 'productos.id', '=', 'publicados.producto_id')
    ->join('relaccions', 'productos.relaccion_id', '=', 'relaccions.id')
    ->join('articulos', 'relaccions.articulo_id', '=', 'articulos.id')
    ->join('categorias', 'relaccions.categoria_id', '=', 'categorias.id')
    ->join('relaccion__imagens', 'relaccions.id', '=', 'relaccion__imagens.relaccion_id')
    ->join('imagens', 'relaccion__imagens.imagen_id', '=', 'imagens.id')
    ->get();



  //  return $products;

    return view('welcome')->with(['products' =>$products]);
});


Route::get('/cart', [CartController::class, 'cart'])->name('cart.index');
Route::post('/add', [CartController::class, 'add'])->name('cart.store');
Route::post('/update', [CartController::class, 'update'])->name('cart.update');
Route::post('/remove', [CartController::class, 'remove'])->name('cart.remove');
Route::post('/clear', [CartController::class, 'clear'])->name('cart.clear');

Auth::routes(['verify' =>true]);



Route::post('/combo', [TallaComboController::class, 'consultaTallas']);
Route::get('/estadouser/{id}/{state}/{name}', [TallaComboController::class, 'usuarioEstado']);
Route::get('/estadocategoria/{id}/{estado}/{descripcion}', [TallaComboController::class, 'categoriaEstado']);
Route::get('/estadocolor/{id}/{estado}/{descripcion}', [TallaComboController::class, 'colorEstado']);
Route::get('/estadoseccion/{id}/{estado}/{descripcion}', [TallaComboController::class, 'seccionEstado']);
Route::get('/estadotalla/{id}/{estado}/{talla}', [TallaComboController::class, 'tallaEstado']);
Route::get('/estadoorigen/{id}/{estado}/{descripcion}', [TallaComboController::class, 'origenEstado']);
Route::get('/publicar/{producto_id}/{relaccion_id}', [TallaComboController::class, 'publicarProducto']);
Route::get('/publicarestado/{id}/{estado}', [TallaComboController::class, 'estadoPublicado']);
Route::get('/ofertar/{producto_id}/{publicado_id}', [TallaComboController::class, 'ofertarProducto']);
Route::get('/ofertaestado/{id}/{estado}', [TallaComboController::class, 'estadoOferta']);
Route::get('/eliminaroferta/{id}/{idpublicado}', [TallaComboController::class, 'eliminarOferta']);




Route::get('/tienda', [HomeUserController::class, 'getUser'])->name('tienda');
Route::post('/combocatalogo', [HomeUserController::class, 'catalogo'])->name('combocatalogo');

/*
Route::get('/contactanos', function () {
    $correo = new ContactanosMailable;
    Mail::to('bacoth@gmail.com')->send($correo);

    return "Mensaje enviado";
});
*/

//Route::get('/enviar/factura',[CorreoFacturaController::class, 'envioFactura'])->name('envio.factura');


Route::resource('/role', RoleController::class)->names('role');


//Route::get('/cliente/perfil',[ RegisterController::class, 'actualizar'])->name('cliente-perfil');


Route::get('/test', function () {

	return Role::create([
        'rol' => 'Logistica',
        'slug' => 'logistica',
        'description' =>'Entregar productos',
        'full-access' =>'no'
        ]);

    });


// Google login
Route::get('login/google', [App\Http\Controllers\Auth\LoginController::class, 'redirectToGoogle'])->name('login.google');
Route::get('login/google/callback', [App\Http\Controllers\Auth\LoginController::class, 'handleGoogleCallback']);

// FaceBook login
Route::get('login/facebook', [App\Http\Controllers\Auth\LoginController::class, 'redirectToFacebook'])->name('login.facebook');
Route::get('login/facebook/callback', [App\Http\Controllers\Auth\LoginController::class, 'handleFacebookCallback']);

// GitHub login
Route::get('login/github', [App\Http\Controllers\Auth\LoginController::class, 'redirectToGithub'])->name('login.github');
Route::get('login/github/callback', [App\Http\Controllers\Auth\LoginController::class, 'handleGithubCallback']);


//Controlladores para Admin
Route::prefix('/administrador')->group(function (){
    Route::resource('/perfil', UserController::class)->names('perfil');
    Route::resource('/modulo_producto/producto', ProductoController::class)->names('producto');
    Route::resource('/modulo_producto/gestion/color', ColorController::class)->names('color');
    Route::resource('/modulo_producto/gestion/categoria', CategoriaController::class)->names('categoria');
    Route::resource('/modulo_producto/gestion/seccion', SeccionController::class)->names('seccion');
    Route::resource('/modulo_producto/gestion/talla', TallaController::class)->names('talla');
    Route::resource('/modulo_producto/gestion/origen', OrigenController::class)->names('origen');
    Route::resource('/modulo_producto/gestion/articulo', ArticuloController::class)->names('articulo');
    Route::resource('/modulo_producto/publicado/publicado', PublicadoController::class)->names('publicado');
    Route::resource('/modulo_producto/publicado/oferta', OfertaController::class)->names('oferta');
    Route::resource('/modulo_producto/producto/imagen', ImagenController::class)->names('imagen');
    Route::resource('/modulo_producto/producto/relaccion_imagen', Relaccion_ImagenController::class)->names('relaccion_imagen');
    Route::get('modulo_usuario/reporte_usuario',[Reporte_UsuatioController::class, 'reporteUsuario'])->name('reporte_usuario');
    Route::get('modulo_usuario/reporte_cliente',[Reporte_UsuatioController::class, 'reporteCliente'])->name('reporte_cliente');
    Route::resource('/modulo_usuario/rol', RolController::class)->names('asignarRol');
    Route::resource('/modulo_usuario/crear', CrearUsuarioController::class)->names('crearusuario');
    Route::get('/panel',[HomeController::class,'index'])->name('home');
    Route::resource('/facturacion/validar/pago', ValidarPagoAdminController::class)->names('validar.pago.admin');
    Route::get('/pago/rechazado/{id}', [MetodoAdminController::class, 'rechazadoPago']);
    Route::get('/pago/factura/detalle/{id}', [MetodoAdminController::class, 'detalleFacturaAdmin']);
    Route::get('/facturacion/generar/factura', [MetodoAdminController::class, 'facturaPendiente']);
    Route::get('/facturacion/factura/autorizado', [MetodoAdminController::class, 'facturaAutorizado']);
    Route::get('/facturacion/factura/rechazado', [MetodoAdminController::class, 'facturaRechazado']);
    Route::get('/facturacion/factura/facturaPDF/{id}', [MetodoAdminController::class, 'descargarPDF']);
    Route::get('/reportes/entregas', [MetodoAdminController::class, 'reporteEntregas']);
    Route::get('/facturacion/generar/factura/{id}', [MetodoAdminController::class, 'generarNumerador']);
    Route::get('/facturacion/emitir/factura/{id}', [MetodoAdminController::class, 'idGenerarFactura']);
    //
    Route::get('/facturacion/factura/detalle/comprobante/{id}', [MetodoAdminController::class, 'comprobante']);
    Route::get('/reportes/entregas/guia/{id}', [MetodoAdminController::class, 'ReporteEntregasGuia']);
    Route::get('/reportes/productos/vendidos', [MetodoAdminController::class, 'productosVendidos']);
    Route::get('/reportes/productos/publicados', [MetodoAdminController::class, 'productosPublicados']);
//reportes/productos/publicados
});

///

//Cliente

Route::prefix('/cliente')->group(function (){
   Route::resource('/perfil', PerfilController::class)->names('cliente-perfil');
   Route::resource('/detalle', DetalleController::class)->names('detalle');
   Route::resource('/oferta', OfertaDetalleController::class)->names('detalleOferta');
   Route::resource('/catalogo',CatalogoController::class,)->names('catalogo');
   Route::resource('/checkout',CheckoutController::class,)->names('checkout');
   Route::match(['get', 'post'], '/buscar', [BuscarController::class, 'buscar'])->name('buscar');
   Route::post('/informacioncheckout1',[AccionesController::class, 'info'])->name('info_envio');
   Route::post('/atras',[AccionesController::class, 'atras'])->name('atras');
   Route::post('/atras2',[AccionesController::class, 'atras2'])->name('atras2');
   Route::post('/siguiente',[AccionesController::class, 'siguiente'])->name('siguiente');
   Route::post('/finalizar',[AccionesController::class, 'finalizar'])->name('finalizar.compra');
   Route::get('/finalizar/compra',[AccionesController::class, 'finalizarcompra'])->name('finalizar.compra.deposito');
   Route::resource('/validar/pago',ValidarPagoClienteController::class,)->names('validarPagoCliente');
   Route::get('/compras/facturas',[FacturaClienteController::class, 'factura'])->name('factura.cliente');
   Route::get('/compras/factura/detalle/{id}', [MetodoClienteController::class, 'detalleFaturaCliente']);
   Route::get('/compras/factura/detalle/pendiente/{id}', [MetodoClienteController::class, 'detalleFaturaClientePendiente']);
   Route::get('/envios/', [MetodoClienteController::class, 'envio']);
   Route::get('/compras/factura/facturapdf/{id}', [MetodoClienteController::class, 'descargarPDF']);
   Route::get('/compras/factura/facturaxml/{id}', [MetodoClienteController::class, 'descargarXML']);
   Route::get('/compras/factura/guia/{id}', [MetodoClienteController::class, 'guia']);
   Route::get('/contactanos', [SinAuthController::class, 'contactanos']);
   Route::post('/contactanos/envio',[SinAuthController::class, 'enviarContacto'])->name('enviar.contactanos');
   Route::get('/acercade', [SinAuthController::class, 'acercaDe']);
   Route::get('/compras/pedidos/pendientes', [MetodoClienteController::class, 'pedidosPendientes']);
   Route::post('cantones/', [MetodoClienteController::class, 'cantones']);
   Route::post('parroquias/', [MetodoClienteController::class, 'parroquias']);



   //  });

 // Route::get('/perfil',[ RegisterController::class, 'actualizar'])->name('cliente-perfil');


});


Route::prefix('/logistica')->group(function (){
    Route::resource('/entregar/pedido', EntregarPedidoLogisticaController::class)->names('entregar-pedido-logistica');
    Route::get('/pedido/pendiente', [MetodoLogisticaController::class, 'pedidoPendiente']);
    Route::get('/pedido/entregado', [MetodoLogisticaController::class, 'pedidoEntregado']);
    Route::get('/factura/detalle/{id}', [MetodoLogisticaController::class, 'detalleFactura']);
    Route::post('/asignar/pedido/', [MetodoLogisticaController::class, 'asignarPedido']);
    Route::get('/pedidos/entregados/guia/{id}', [MetodoLogisticaController::class, 'guia']);
});


Route::prefix('/repartidor')->group(function (){
    Route::resource('/pedidos/entregados', EntregarPedidosMensajeroController::class)->names('entregar-pedido-repartidor');
    Route::get('/pedidos/pendientes', [MetodoRepartidorController::class, 'pedidoPendiente']);
    Route::get('/pedidos/entregados/guia/{id}', [MetodoRepartidorController::class, 'guia']);
});


